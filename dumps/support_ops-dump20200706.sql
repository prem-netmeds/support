-- MySQL dump 10.13  Distrib 8.0.18, for Win64 (x86_64)
--
-- Host: localhost    Database: support_ops
-- ------------------------------------------------------
-- Server version	5.7.28-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `mstar_customer_block_reason`
--

DROP TABLE IF EXISTS `mstar_customer_block_reason`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `mstar_customer_block_reason` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) NOT NULL,
  `reason` mediumtext NOT NULL,
  `date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mstar_customer_block_reason`
--

LOCK TABLES `mstar_customer_block_reason` WRITE;
/*!40000 ALTER TABLE `mstar_customer_block_reason` DISABLE KEYS */;
/*!40000 ALTER TABLE `mstar_customer_block_reason` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `spt_ops_user`
--

DROP TABLE IF EXISTS `spt_ops_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `spt_ops_user` (
  `user_name` varchar(150) NOT NULL,
  `user_id` varchar(150) NOT NULL,
  `pass_hash_sha1` mediumtext NOT NULL,
  `pass_reset_code` int(11) DEFAULT NULL,
  `reset_code_valid_till` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `user_email` varchar(150) DEFAULT NULL,
  `role` varchar(150) NOT NULL,
  `team` varchar(50) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `spt_ops_user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `spt_ops_user`
--

LOCK TABLES `spt_ops_user` WRITE;
/*!40000 ALTER TABLE `spt_ops_user` DISABLE KEYS */;
INSERT INTO `spt_ops_user` VALUES ('Anandhan','anandhan','40bd001563085fc35165329ea1ff5c5ecbdbbeef',NULL,'2020-01-06 02:10:06','anandan.selvaraj@netmeds.com','admin','Software Development',0,'2020-01-06 02:10:06','2020-02-04 03:50:52'),('Muthukumar','muthukumar','40bd001563085fc35165329ea1ff5c5ecbdbbeef',636222,'2020-01-28 08:21:47','muthukumar.a@netmeds.com','admin','Software Development',1,'2018-06-29 04:54:12','2020-04-26 15:06:39'),('Prem Kumar','premkumar','40bd001563085fc35165329ea1ff5c5ecbdbbeef',NULL,'2019-12-31 08:24:45','prem.kumar@netmeds.com','admin','Software Development',1,'2018-06-29 04:54:12','2020-01-06 04:20:56');
/*!40000 ALTER TABLE `spt_ops_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `spt_smart_recon_config`
--

DROP TABLE IF EXISTS `spt_smart_recon_config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `spt_smart_recon_config` (
  `config_key` varchar(150) NOT NULL,
  `config_value` int(3) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`config_key`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `spt_smart_recon_config`
--

LOCK TABLES `spt_smart_recon_config` WRITE;
/*!40000 ALTER TABLE `spt_smart_recon_config` DISABLE KEYS */;
INSERT INTO `spt_smart_recon_config` VALUES ('frequency_allowed_count',3,'2020-07-01 16:25:39','2020-07-01 16:25:39'),('frequency_check_duration_in_minutes',60,'2020-07-01 16:25:39','2020-07-01 16:25:39'),('max_duration_in_hours',25,'2020-07-01 16:25:39','2020-07-01 16:25:39'),('max_time_to_go_back_in_days',7,'2020-07-01 16:25:39','2020-07-02 14:07:47');
/*!40000 ALTER TABLE `spt_smart_recon_config` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `spt_smart_recon_report_request`
--

DROP TABLE IF EXISTS `spt_smart_recon_report_request`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `spt_smart_recon_report_request` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `client_ip` varchar(15) NOT NULL,
  `request_parameters` text,
  `request_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `CLIENT_INDEX` (`client_ip`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `spt_smart_recon_report_request`
--

LOCK TABLES `spt_smart_recon_report_request` WRITE;
/*!40000 ALTER TABLE `spt_smart_recon_report_request` DISABLE KEYS */;
INSERT INTO `spt_smart_recon_report_request` VALUES (1,'127.0.0.1','start_time: 2020-07-04 00:00:00, end_time: 2020-07-04 01:00:00','2020-07-05 05:49:08'),(2,'127.0.0.1','start_time: 2020-07-04 00:00:00, end_time: 2020-07-04 01:00:00','2020-07-05 06:58:01');
/*!40000 ALTER TABLE `spt_smart_recon_report_request` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-07-06 10:27:00
