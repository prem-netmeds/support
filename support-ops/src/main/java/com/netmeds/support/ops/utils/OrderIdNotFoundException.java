package com.netmeds.support.ops.utils;

public class OrderIdNotFoundException extends RuntimeException{
  public OrderIdNotFoundException(String msg){
    super(msg);
  }
  
}
