
package com.netmeds.support.ops.utils;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.activation.DataHandler;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

public class Mailer {

    private static final Logger logger=Logger.getLogger(Mailer.class.getName());

    private static final String SKIP_PROPNAME="mailer.donotsend";
    private static final String PREFIX_PROPNAME="mailer.subjectprefix";
    private static final String SUFFIX_PROPNAME="mailer.subjectsuffix";

    private Properties props;
    private Authenticator authenticator;
    
    public Mailer(Properties props) {
        this.props=props;
        authenticator=null;
    }

    public Mailer(Properties props, Authenticator authenticator) {
        this.props = props;
        this.authenticator = authenticator;
    }

    public Mailer(Properties props, String username, String passwd) {
        this.props = props;
        this.authenticator = new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(username, passwd);
            }
        };
    }

    public Mailer(String host) {
        this(host, "25");
    }

    public Mailer(String host, String port) {
        props=new Properties();
        props.setProperty("mail.smtp.host", host);
        props.setProperty("mail.smtp.port", port);
        //props.setProperty("mail.smtp.localhost", AppParamsManager.getParam("mail.smtp.localhost"));
    }

    public boolean sendEmail(   String   fromAddr,
                                String   fromName,
                                String[] receipientsTo,
                                String   subject,
                                String   content)
                    throws MessagingException {
        return sendEmail(fromAddr, fromName, receipientsTo, null, null, subject, content, null, null);
    }

    public boolean sendEmail(   String   fromAddr,
                                String   fromName,
                                String[] receipientsTo,
                                String   subject,
                                String   content,
                                File[]   standardAttachments)
                    throws MessagingException {
        try {
            return sendEmail(fromAddr, fromName, receipientsTo, null, null, subject, content, convertFileArrayToURLArray(standardAttachments), null);
        }
        catch(MalformedURLException e) {
            logger.log(Level.SEVERE, "thrown MalformedURLException:", e);
            return false;
        }
    }

    public boolean sendEmail(   String   fromAddr,
                                String[] receipientsTo,
                                String[] receipientsCC,
                                String[] receipientsBCC,
                                String   subject,
                                File     contentFile,
                                File[]   standardAttachments,
                                File[]   inlineAttachments)
                    throws MessagingException {
        String content;

        content=returnFileAsString(contentFile.getPath());
        return sendEmail(fromAddr, receipientsTo, receipientsCC, receipientsBCC, subject, content, standardAttachments, inlineAttachments);
    }

    public boolean sendEmail(   String   fromAddr,
                                String[] receipientsTo,
                                String[] receipientsCC,
                                String[] receipientsBCC,
                                String   subject,
                                String   content,
                                File[]   standardAttachments,
                                File[]   inlineAttachments)
                    throws MessagingException {
        URL[] stdAttachmentsURLArray;
        URL[] inlAttachmentsURLArray;
        int i, len;

        try {
            len=(standardAttachments!=null)?standardAttachments.length:0;
            stdAttachmentsURLArray=new URL[len];
            for(i=0;  i<len;  i++) {
                stdAttachmentsURLArray[i]=standardAttachments[i].toURI().toURL();
            }

            len=(inlineAttachments!=null)?inlineAttachments.length:0;
            inlAttachmentsURLArray=new URL[len];
            for(i=0;  i<len;  i++) {
                inlAttachmentsURLArray[i]=inlineAttachments[i].toURI().toURL();
            }
        }
        catch(MalformedURLException e) {
            logger.log(Level.SEVERE, "thrown MalformedURLException:", e);
            return false;
        }

        return sendEmail(fromAddr, receipientsTo, receipientsCC, receipientsBCC, subject, content, stdAttachmentsURLArray, inlAttachmentsURLArray);
    }

    public boolean sendEmail(   String   fromAddr,
                                String[] receipientsTo,
                                String[] receipientsCC,
                                String[] receipientsBCC,
                                String   subject,
                                String   content,
                                URL[]    standardAttachments,
                                URL[]    inlineAttachments)
                    throws MessagingException {

        return sendEmail(fromAddr, null, receipientsTo, receipientsCC, receipientsBCC, subject, content, standardAttachments, inlineAttachments);
    }

    public boolean sendEmail(   String   fromAddr,
                                String   fromName,
                                String[] receipientsTo,
                                String[] receipientsCC,
                                String[] receipientsBCC,
                                String   subject,
                                String   content,
                                URL[]    standardAttachments,
                                URL[]    inlineAttachments
                                )
                    throws MessagingException {
        Session session;
        Message message;
        MimeMultipart mainMultipart, multipart;
        BodyPart bodyPart;
        String header;
        URL file;
        int i, len;

        logger.log(Level.INFO, "sending mail. subject:[[[{0}]]] receipients:[[[{1}]]]", new String[]{subject, concatNames(receipientsTo)});
        if(receipientsBCC!=null && receipientsBCC.length>0) {
            logger.log(Level.INFO, "bcc-ing mail. subject:[[[{0}]]] receipients:[[[{1}]]]", new String[]{subject, concatNames(receipientsTo)});
        }

        skipcheck:
        { // check for skip
            String value=System.getProperty(SKIP_PROPNAME);

            if(value==null) break skipcheck;
            if(value.equals("true")) {
                logger.log(Level.INFO, "mail sending skipped.");
                return true;
            }
        }

        session=(authenticator!=null)?Session.getInstance(props, authenticator):Session.getInstance(props);

        message = new MimeMessage(session);
        message.setReplyTo(new Address[]{new InternetAddress(fromAddr)});

        if(fromName!=null) {
            try {
                message.setFrom(new InternetAddress(fromAddr, fromName));
            }
            catch (UnsupportedEncodingException ex) {
                logger.log(Level.WARNING, "UnsupportedEncodingException: Setting fromName '{'{0}'}' throws exception.", fromName);
                message.setFrom(new InternetAddress(fromAddr));
            }
        } else {
            message.setFrom(new InternetAddress(fromAddr));
        }

        message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(concatNames(receipientsTo)));
        if(receipientsCC!=null) {
            message.setRecipients(Message.RecipientType.CC, InternetAddress.parse(concatNames(receipientsCC)));
        }
        if(receipientsBCC!=null) {
            message.setRecipients(Message.RecipientType.BCC, InternetAddress.parse(concatNames(receipientsBCC)));
        }
        message.setSubject(getDecoratedSubject(subject));

        mainMultipart = new MimeMultipart("mixed");

        multipart=new MimeMultipart("related");

        // body content
        bodyPart = new MimeBodyPart();
        bodyPart.setContent(content, "text/html");
        multipart.addBodyPart(bodyPart);

        // inline attachments
        if(inlineAttachments!=null) {
            len=inlineAttachments.length;
            for(i=0;  i<len;  i++) {
                file=inlineAttachments[i];
                //if(!file.exists()) {
                //    return false;
                //}

                bodyPart = new MimeBodyPart();
                bodyPart.setDataHandler(new DataHandler(file));
                if((header=getContentTypeHeader(file))!=null) {
                    bodyPart.setHeader("Content-Type", header);
                }
                bodyPart.setDisposition(Part.INLINE);
                bodyPart.setHeader("Content-Disposition", "inline; filename=\""+getName(file)+"\"");
                bodyPart.setHeader("Content-ID", "<"+getName(file)+">");

                multipart.addBodyPart(bodyPart);
            }
        }

        // add related parts to mainpart
        bodyPart=new MimeBodyPart();
        bodyPart.setContent(multipart);
        mainMultipart.addBodyPart(bodyPart);

        // standard attachments
        if(standardAttachments!=null) {
            len=standardAttachments.length;
            for(i=0;  i<len;  i++) {
                file=standardAttachments[i];
                //if(!file.exists()) {
                //    return false;
                //}

                bodyPart = new MimeBodyPart();
                bodyPart.setDataHandler(new DataHandler(file));
                if((header=getContentTypeHeader(file))!=null) {
                    bodyPart.setHeader("Content-Type", header);
                }
                bodyPart.setDisposition(Part.ATTACHMENT);
                bodyPart.setHeader("Content-Disposition", "attachment; filename=\""+getName(file)+"\"");
                bodyPart.setHeader("Content-ID", "<"+getName(file)+">");
                bodyPart.setFileName(getName(file));

                mainMultipart.addBodyPart(bodyPart);
            }
        }

        // Add multipart content to message.
        message.setContent(mainMultipart);

        Transport.send(message);

        return true;
    }

    private String getName(URL url) {
        String filepart;
        String[] toks;

        filepart=url.getFile();
        toks=filepart.split("\\/");

        return toks[toks.length-1];
    }

    private String getContentTypeHeader(URL file) {
        String mimeType=null;
        String filename;

        //filename=file.getName().toLowerCase();
        filename=getName(file).toLowerCase();

        if(filename.endsWith(".jpg")) {
            mimeType="image/jpeg";
        }
        else
        if(filename.endsWith(".png")) {
            mimeType="image/png";
        }
        else
        if(filename.endsWith(".pdf")) {
            mimeType="application/pdf";
        }
        else
        if(filename.endsWith(".txt")) {
            mimeType="text/plain";
        }
        else
        if(filename.endsWith(".xls")) {
            mimeType="application/vnd.ms-excel";
        }
        else {
            return null;
        }

        return mimeType+";\n        name=\""+getName(file)+"\" ";
    }

    public String searchAndReplace(Map<String, String> replaceMap, String original) {
        Set<String> keySet;
        String workingStr=original;

        keySet=replaceMap.keySet();

        for(String key : keySet) {
            workingStr=workingStr.replaceAll("#\\@\\{"+key+"}", replaceMap.get(key));
        }

        return workingStr;
    }

    public String returnFileAsString(String filename) {
        try {
            return returnStreamAsString(new FileInputStream(filename));
        }
        catch(Exception e) {
            logger.log(Level.SEVERE, "thrown exception:", e);
            return "";
        }
    }

    public String returnResourceAsString(URL url) {
        try {
            return returnStreamAsString(url.openStream());
        }
        catch(Exception e) {
            logger.log(Level.SEVERE, "thrown exception:", e);
            return "";
        }
    }

    private String returnStreamAsString(InputStream fis) {
        ByteArrayOutputStream baos;
        byte buf[]=new byte[2048];
        int red;
        String ret;

        try {
            baos=new ByteArrayOutputStream();

            while((red=fis.read(buf))!=-1) {
                baos.write(buf, 0, red);
            }

            ret=new String(baos.toByteArray());
            baos.close();

            return ret;
        }
        catch(IOException ioe) {
            logger.log(Level.SEVERE, "thrown IOException:", ioe);
            return "";
        }
        finally {
            if(fis!=null) {
                try { fis.close(); }
                catch(Exception e){ logger.log(Level.SEVERE, "thrown exception:", e); }
            }
        }
    }

    private String concatNames(String[] names) {
        StringBuilder sb=new StringBuilder();
        int i, len;

        for(len=names.length, i=0;   i<len;   i++) {
            if(i!=0) sb.append(",");
            sb.append(names[i]);
        }

        return sb.toString();
    }

    private static URL[] convertFileArrayToURLArray(File[] fa) throws MalformedURLException {
        URL[] ua;
        int i, len;

        len=(fa!=null)?fa.length:0;
        ua=new URL[len];
        for(i=0;  i<len;  i++) {
            ua[i]=fa[i].toURI().toURL();
        }

        return ua;
    }

    public static void turnOffSendingEmails() {
        System.setProperty(SKIP_PROPNAME, "true");
    }

    private static String getDecoratedSubject(String subj) {
        String prefix, suffix;

        prefix=System.getProperty(PREFIX_PROPNAME, "");
        suffix=System.getProperty(SUFFIX_PROPNAME, "");

        return prefix+subj+suffix;
    }
}
