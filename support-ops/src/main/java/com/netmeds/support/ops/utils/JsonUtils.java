/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.netmeds.support.ops.utils;

import java.io.StringReader;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonNumber;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonReader;
import javax.json.JsonString;
import javax.json.JsonValue;

/**
 *
 * @author prem
 */
public class JsonUtils {
    private static final Logger LOGGER = Logger.getLogger(JsonUtils.class.getName());

    private JsonUtils(){}

    public static JsonString createJsonString(final String str) {
        return new JsonString() {

            @Override
            public String getString() {
                return str;
            }

            @Override
            public CharSequence getChars() {
                return str;
            }

            @Override
            public JsonValue.ValueType getValueType() {
                return JsonValue.ValueType.STRING;
            }
        };
    }

    public static JsonNumber createJsonNumber(final int i) {
        return new JsonNumber() {

            @Override
            public boolean isIntegral() {
                return true;
            }

            @Override
            public int intValue() {
                return i;
            }

            @Override
            public int intValueExact() {
                return i;
            }

            @Override
            public long longValue() {
                return i;
            }

            @Override
            public long longValueExact() {
                return i;
            }

            @Override
            public BigInteger bigIntegerValue() {
                return new BigInteger(""+i);
            }

            @Override
            public BigInteger bigIntegerValueExact() {
                return new BigInteger(""+i);
            }

            @Override
            public double doubleValue() {
                return i;
            }

            @Override
            public BigDecimal bigDecimalValue() {
                return new BigDecimal(i);
            }

            @Override
            public JsonValue.ValueType getValueType() {
                return JsonValue.ValueType.NUMBER;
            }

            @Override
            public String toString() {
                return ""+i;
            }
        };
    }

    public static JsonObject appendValueToJsonObject(JsonObject jsonObject, String key, JsonValue value) {
        Set<String> keySet;
        JsonObjectBuilder jBuilder = Json.createObjectBuilder();
        try{
            keySet = jsonObject.keySet();
            jBuilder.add(key, value);
            for(String keyStr : keySet){
                if(!key.equalsIgnoreCase(keyStr)) jBuilder.add(keyStr, jsonObject.get(keyStr));
            }
        }catch(Exception e){
            LOGGER.log(Level.INFO,"Exception thrown",e);
            return null;
        }
        return jBuilder.build();
    }

    public static JsonObject mergeJsonObjects(JsonObject baseObject, JsonObject superImposeObject) {
        Set<String> keySetSourse;
        Set<String> keySetTargetted;
        JsonObjectBuilder jBuilder = Json.createObjectBuilder();
        try{
            keySetSourse = baseObject.keySet();
            for(String keyStr : keySetSourse){
                jBuilder.add(keyStr, baseObject.get(keyStr));
            }

            keySetTargetted = superImposeObject.keySet();
            for(String keyStr : keySetTargetted){
                jBuilder.add(keyStr, superImposeObject.get(keyStr));
            }

        }catch(Exception e){
            LOGGER.log(Level.INFO,"Exception thrown",e);
            return null;
        }
        return jBuilder.build();
    }
    
    public static JsonObject getJsonObject(JsonObject parentJsonObj,String key){
        if(parentJsonObj == null)throw new RuntimeException(String.format("null value passed for the key '%1$s'. expected: jsonobject.",key));
        JsonObject childJsonObj;
        childJsonObj = parentJsonObj.getJsonObject(key);
        if(childJsonObj == null)throw new RuntimeException(String.format("Jsonobject '%1$s' not found.",key));
        return childJsonObj;
    }
    
    public static JsonArray getJsonArray(JsonObject parentJsonObj,String key){
        if(parentJsonObj == null)throw new RuntimeException(String.format("null value passed for the key '%1$s'. expected: jsonobject.",key));
        if(!parentJsonObj.containsKey(key))throw new RuntimeException(String.format("tartget key '%1$s' not found.",key));
        JsonArray childJsonArr;
        childJsonArr = parentJsonObj.getJsonArray(key);
        return childJsonArr;
    }
    
    public static Object convertJsonValueToObject(JsonObject jsonObj,String key,List<JsonValue.ValueType> expectedType){
        if(jsonObj == null)throw new RuntimeException(String.format("null value passed for the key '%1$s'. expected: jsonobject.",key));
        if(!jsonObj.containsKey(key))throw new RuntimeException(String.format("key value pair not available for the key '%1$s'.",key));
        JsonValue jsonVal = jsonObj.get(key);
        boolean validTypeSt = false;
        for (JsonValue.ValueType valueType : expectedType) {
            if(valueType.equals(jsonVal.getValueType())){
                validTypeSt = true;
            }
        }
        if(validTypeSt){
            return convertJsonValueToObject(jsonObj,key);
        }else{
            throw new RuntimeException(String.format("invalid json value type for key '%1$s' found '%3$s' expected '%2$s'.",key,expectedType.toString(),jsonVal.getValueType()));
        }
    }
    
    public static Object convertJsonValueToObject(JsonObject jsonObj,String key){
        if(jsonObj == null)throw new RuntimeException(String.format("null value passed for the key '%1$s'. expected: jsonobject.",key));
        Object outObj = null;
        if(!jsonObj.containsKey(key))throw new RuntimeException(String.format("key value pair not available for the key '%1$s'.",key));
        JsonValue jsonVal = jsonObj.get(key);
        switch(jsonVal.getValueType()) {
            case NULL:
                outObj = null;break;
            case STRING:
                outObj = jsonObj.getString(key);break;
            case NUMBER:
                outObj = jsonObj.getJsonNumber(key);
                double d = Double.parseDouble(outObj+"");
                int i = (int)d;
                if(i == 0 || d == 0.0){
                    outObj = 0;
                }else{
                    if(d % i == 0){
                        outObj = i;
                    }else{
                        outObj = d;                    
                    }
                }                                
                break;
            case FALSE:
                outObj = jsonObj.getBoolean(key);break;
            case TRUE:
                outObj = jsonObj.getBoolean(key);break;
            case ARRAY:
                outObj = jsonObj.getJsonArray(key);break;
            case OBJECT:
                outObj = jsonObj.getJsonObject(key);break;
            default:break;
        }
        return outObj;
    }
    
    public static JsonObject convertStringToJsonObject(String jsonStr){
        JsonObject jsonObj = null;
        try {
            JsonReader jr = Json.createReader(new StringReader(jsonStr));
            jsonObj = jr.readObject();
        } catch (Exception e) {
            Throwable t = ExceptionUtils.getRootCause(e);
            throw new RuntimeException("json parse error. message: "+t.getMessage());
        }
        return jsonObj;
    }
    
    public static JsonArray readJAFromString(String str) {
        JsonReader jsonReader;
        JsonArray jsonArray;

        if (str == null || str.trim().isEmpty()) {
            return null;
        }
        try{
            jsonReader = Json.createReader(new StringReader(str));
            jsonArray = jsonReader.readArray();
            jsonReader.close();
        }catch(Exception e){
            LOGGER.log(Level.INFO,"",e);
            jsonArray=null;
        }
            
        return jsonArray;
    }
}
