/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.netmeds.support.ops.utils;


import java.io.StringReader;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonReader;
import javax.json.JsonValue;

/**
 *
 * @author NET00266
 */
public class ExceptionUtils {
    
    private static final Logger LOG = Logger.getLogger(ExceptionUtils.class.getName());
    
    @FunctionalInterface
    public interface Consumer_WithExceptions<T, E extends Exception> {
        void accept(T t) throws E;
    }

    @FunctionalInterface
    public interface BiConsumer_WithExceptions<T, U, E extends Exception> {
        void accept(T t, U u) throws E;
    }

    @FunctionalInterface
    public interface Function_WithExceptions<T, R, E extends Exception> {
        R apply(T t) throws E;
    }

    @FunctionalInterface
    public interface Supplier_WithExceptions<T, E extends Exception> {
        T get() throws E;
    }

    @FunctionalInterface
    public interface Runnable_WithExceptions<E extends Exception> {
        void run() throws E;
    }

    /** .forEach(rethrowConsumer(name -> System.out.println(Class.forName(name)))); or .forEach(rethrowConsumer(ClassNameUtil::println)); */
    public static <T, E extends Exception> Consumer<T> rethrowConsumer(Consumer_WithExceptions<T, E> consumer) throws E {
        return t -> {
            try { consumer.accept(t); }
            catch (Exception exception) { throwAsUnchecked(exception); }
            };
        }

    public static <T, U, E extends Exception> BiConsumer<T, U> rethrowBiConsumer(BiConsumer_WithExceptions<T, U, E> biConsumer) throws E {
        return (t, u) -> {
            try { biConsumer.accept(t, u); }
            catch (Exception exception) { throwAsUnchecked(exception); }
            };
        }
    /** .map(rethrowFunction(name -> Class.forName(name))) or .map(rethrowFunction(Class::forName)) */
    public static <T, R, E extends Exception> Function<T, R> rethrowFunction(Function_WithExceptions<T, R, E> function) throws E {
        return t -> {
            try { return function.apply(t); }
            catch (Exception exception) { throwAsUnchecked(exception); return null; }
            };
        }

    /** rethrowSupplier(() -> new StringJoiner(new String(new byte[]{77, 97, 114, 107}, "UTF-8"))), */
    public static <T, E extends Exception> Supplier<T> rethrowSupplier(Supplier_WithExceptions<T, E> function) throws E {
        return () -> {
            try { return function.get(); }
            catch (Exception exception) { throwAsUnchecked(exception); return null; }
            };
        }

    /** uncheck(() -> Class.forName("xxx")); */
    public static void uncheck(Runnable_WithExceptions t)
        {
        try { t.run(); }
        catch (Exception exception) { throwAsUnchecked(exception); }
        }

    /** uncheck(() -> Class.forName("xxx")); */
    public static <R, E extends Exception> R uncheck(Supplier_WithExceptions<R, E> supplier)
        {
        try { return supplier.get(); }
        catch (Exception exception) { throwAsUnchecked(exception); return null; }
        }

    /** uncheck(Class::forName, "xxx"); */
    public static <T, R, E extends Exception> R uncheck(Function_WithExceptions<T, R, E> function, T t) {
        try { return function.apply(t); }
        catch (Exception exception) { throwAsUnchecked(exception); return null; }
        }

    @SuppressWarnings ("unchecked")
    private static <E extends Throwable> void throwAsUnchecked(Exception exception) throws E { throw (E)exception; }
    
    public static boolean isCausedBy(Throwable caught, Class<? extends Throwable> isOfOrCausedBy) {
        if (caught == null) return false;
        else if (isOfOrCausedBy.isAssignableFrom(caught.getClass())) return true;
        else return isCausedBy(caught.getCause(), isOfOrCausedBy);
    }
    
    public static void logExceptionStackTrace(Throwable throwable){
        String className,stackTrace;
        try {
            for (int i = 0; i < throwable.getStackTrace().length; i++) {
                className = throwable.getStackTrace()[i].getClassName();
                stackTrace = throwable.getStackTrace()[i].toString();
                if(className.startsWith("com.netmeds.support.ops")){
                    LOG.log(Level.SEVERE,String.format("Stack Trace(%1$s): ",stackTrace));
                }
            }
        } catch (Exception e) {}
    }
    
    public static JsonObject createExceptionJsonObject(String addMsg,String rootCause,String causedBy,JsonArray stackTraceArr,Throwable throwable){
        rootCause = (rootCause!=null)?rootCause:"null";
        String className,stackTrace;
        JsonArrayBuilder jsonArrB = Json.createArrayBuilder();
        if(stackTraceArr != null){
            for (int i = 0; i < stackTraceArr.size(); i++) {
                jsonArrB.add(stackTraceArr.get(i));
            }
        }else{
            for (int i = 0; i < throwable.getStackTrace().length; i++) {
                className = throwable.getStackTrace()[i].getClassName();
                stackTrace = throwable.getStackTrace()[i].toString();
                if(className.startsWith("com.netmeds.support.ops")){
                    stackTrace = stackTrace.replace(".", " -> ");
                    jsonArrB.add(stackTrace);
                }
            }
        }
        JsonObjectBuilder jsonObjB = Json.createObjectBuilder();
        if(addMsg != null){
            jsonObjB.add("additional_message",addMsg);
        }else{
            jsonObjB.add("additional_message",JsonValue.NULL);
        }
        jsonObjB.add("root_cause",rootCause);
        jsonObjB.add("caused_by",causedBy);
        jsonObjB.add("stack_trace",jsonArrB.build());
        return jsonObjB.build();
    }
    
    public static String getExceptionMessage(Throwable throwable,String addMsg){
        String exceptionJsonObjStr = null;
        try {
            String errorType, exceptionMsg;
            exceptionMsg = throwable.getMessage();
            errorType = "internal error";
            JsonObject exJsonObj = null;
            try {
                JsonReader jr = Json.createReader(new StringReader(exceptionMsg));
                exJsonObj = jr.readObject();
                boolean isValidExJsonObj = true;
                for(String key : new String[]{"additional_message","root_cause","caused_by","stack_trace"}){
                    if(!exJsonObj.containsKey(key))isValidExJsonObj = false;
                }
                if(isValidExJsonObj){
                    exceptionMsg = exJsonObj.getString("root_cause");
                    errorType = exJsonObj.getString("caused_by");
                    JsonArray stackTrace = exJsonObj.getJsonArray("stack_trace");
                    exJsonObj = createExceptionJsonObject(addMsg,exceptionMsg,errorType,stackTrace,throwable);
                }else{
                    exJsonObj = createExceptionJsonObject(addMsg,exceptionMsg,errorType,null,throwable);
                }
            } catch (Exception e) {}
            if(exJsonObj == null){                
                exJsonObj = createExceptionJsonObject(addMsg,exceptionMsg,errorType,null,throwable);
            }
            exceptionJsonObjStr = exJsonObj.toString();
        } catch (Exception e) {}
        return exceptionJsonObjStr;
    }
    
    public static Throwable getRootCause(Throwable exception){
        Throwable rootException = exception;
        while(rootException.getCause() != null){
            rootException = rootException.getCause();
        }
        return rootException;
    }
    
}
