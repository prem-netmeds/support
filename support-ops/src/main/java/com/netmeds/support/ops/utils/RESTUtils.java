/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.netmeds.support.ops.utils;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonValue;
import javax.ws.rs.core.Response;

/**
 *
 * @author prem
 */
@SuppressWarnings("unchecked") // this is shotgun approach. spray and pray
public class RESTUtils {

    private static final String MSG_UNAUTHORIZED  = "not authorized to perform this operation";
    private static final String STATUS_SUCCESS    = "success";
    private static final String STATUS_FAILED     = "failed";
    private static final String PROPNAME_STATUS   = "status";
    private static final String PROPNAME_REASON   = "reason";
    private static final String PROPNAME_RESULT   = "result";

    public static Response sendUnAuthorizedResponse(String reason) {
        return Response.status(Response.Status.UNAUTHORIZED).entity(createFailedResponseJson(reason)).build();
    }

    public static Response sendUnAuthorizedResponse() {
        return Response.status(Response.Status.UNAUTHORIZED).entity(createFailedResponseJson(MSG_UNAUTHORIZED)).build();
    }

    public static Response sendBadRequestResponse(List<String> reasonList) {
        return Response.status(Response.Status.BAD_REQUEST).entity(createFailedResponseJson(convertListToJson(reasonList))).build();
    }

    public static JsonObject createSuccessResponseJson(JsonValue jsonValue) {
        return createResponseJson(STATUS_SUCCESS, (JsonValue)null, jsonValue);
    }

    public static JsonObject createSuccessResponseJson(JsonValue reasonJson, JsonValue resultJson) {
        return createResponseJson(STATUS_SUCCESS, reasonJson, resultJson);
    }

    public static JsonObject createSuccessResponseJsonWithAdditionalValues(JsonValue jsonValue, Map<String, Object> addlValuesMap) {
        return createResponseJson(STATUS_SUCCESS, (JsonValue)null, jsonValue, addlValuesMap);
    }

    public static JsonObject createSuccessResponseJsonWithReason(String reason) {
        return createSuccessResponseJson(reason, (JsonValue)null);
    }

    public static JsonObject createFailedResponseJson(String reason, JsonValue jsonValue) {
        return createResponseJson(STATUS_FAILED, JsonUtils.createJsonString(reason), jsonValue);
    }

    public static JsonObject createFailedResponseJson(JsonValue reason, JsonValue jsonValue) {
        return createResponseJson(STATUS_FAILED, reason, jsonValue);
    }

    public static JsonObject createFailedResponseJson(JsonValue reasonJson) {
        return createResponseJson(STATUS_FAILED, reasonJson, null);
    }

    public static JsonObject createSuccessResponseJson(String resultStr) {
        return createResponseJson(STATUS_SUCCESS, resultStr);
    }

    public static JsonObject createSuccessResponseJson(String detailString,JsonValue jsonValue) {
        return createResponseJson(STATUS_SUCCESS, detailString,jsonValue);
    }

    public static JsonObject createFailedResponseJson(String reason) {
        return createResponseJson(STATUS_FAILED, reason, null);
    }

    public static JsonObject createResponseJson(String status, final String resultStr) {
        return createResponseJson(status, (JsonValue)null, JsonUtils.createJsonString(resultStr));
    }

    public static JsonObject createResponseJson(String status, String reason, JsonValue resultObject) {
        JsonValue reasonJsonValue=null;

        if(reason!=null && !reason.trim().isEmpty()) {
            reasonJsonValue=JsonUtils.createJsonString(reason);
        }

        return createResponseJson(status, reasonJsonValue, resultObject);
    }

    public static JsonObject createResponseJson(String status, JsonValue reason, JsonValue resultObject) {
        return createResponseJson(status, reason, resultObject, null);
    }

    public static JsonObject createResponseJson(String status, JsonValue reason, JsonValue resultObject, Map<String, Object> additionalValuesMap) {
        if(additionalValuesMap==null) {
            additionalValuesMap=new HashMap<>();
        }

        additionalValuesMap.put(PROPNAME_STATUS, status);

        if(reason!=null) {
            additionalValuesMap.put(PROPNAME_REASON, reason);
        }

        if(resultObject!=null) {
            additionalValuesMap.put(PROPNAME_RESULT, resultObject);
        }

        return convertMapToJson(additionalValuesMap);
    }

    public static final String KEY_RETURNVALUES="returnValues";
    public static final String PATHSEPARATOR=".";
    public static final SimpleDateFormat SDF=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    public static JsonObject convertMapToJson(Map<String, Object> responseMap) {
        return convertMapToJson(responseMap, "root");
    }

    private static JsonObject convertMapToJson(Map<String, Object> responseMap, String path) {
        JsonObjectBuilder jsonObj=Json.createObjectBuilder();
        Set<String> keySet;
        Object val;
        Class type;

        keySet=responseMap.keySet();

        for(String key : keySet) {
            val=responseMap.get(key);
            if(val==null) {
                jsonObj.addNull(key);
                continue;
            }
            type=val.getClass();

            ///////////////////////
            // basic types
            if(type.isAssignableFrom(String.class)) {
                jsonObj.add(key, (String)val);
                continue;
            }
            if(type.isAssignableFrom(Integer.class)) {
                jsonObj.add(key, (Integer)val);
                continue;
            }
            if(type.isAssignableFrom(Date.class)) {
                synchronized(SDF) { jsonObj.add(key, SDF.format((Date)val)); }
                continue;
            }
            if(type.isAssignableFrom(BigDecimal.class)) {
                jsonObj.add(key, (BigDecimal)val);
                continue;
            }
            if(type.isAssignableFrom(Long.class)) {
                jsonObj.add(key, (Long)val);
                continue;
            }
            if(type.isAssignableFrom(long.class)) {
                jsonObj.add(key, (long)val);
                continue;
            }
            if(type.isAssignableFrom(boolean.class)) {
                jsonObj.add(key, (boolean)val);
                continue;
            }
            if(type.isAssignableFrom(Boolean.class)) {
                jsonObj.add(key, (boolean)val);
                continue;
            }
            if(type.isAssignableFrom(Double.class)) {
                jsonObj.add(key, (double)val);
                continue;
            }

            ///////////////////////
            // json object
            if(JsonObject.class.isAssignableFrom(type)) {
                jsonObj.add(key, (JsonObject)val);
                continue;
            }

            ///////////////////////
            // json array
            if(JsonArray.class.isAssignableFrom(type)) {
                jsonObj.add(key, (JsonArray)val);
                continue;
            }

            ///////////////////////
            // json value
            if(JsonValue.class.isAssignableFrom(type)) {
                jsonObj.add(key, (JsonValue)val);
                continue;
            }

            ///////////////////////
            // list
            if(List.class.isAssignableFrom(type)) {
                jsonObj.add(key, convertListToJson((List)val, path+PATHSEPARATOR+key));
                continue;
            }

            ///////////////////////
            // map
            if(Map.class.isAssignableFrom(type)) {
                jsonObj.add(key, convertMapToJson((Map)val, path+PATHSEPARATOR+key));
                continue;
            }

            throw new IllegalArgumentException(String.format("at '%1$s%2$s%3$s' found type:%4$s that is not handled", path, PATHSEPARATOR, key, type.getName()));
        }

        return jsonObj.build();
    }

    public static JsonArray convertListToJson(List valList) {
        return convertListToJson(valList, "root");
    }

    private static JsonArray convertListToJson(List valList, String path) {
        JsonArrayBuilder jsonArr=Json.createArrayBuilder();
        Class type;
        int i, len;
        Object val;

        len=valList.size();
        for(i=0;  i<len;  i++) {
            val=valList.get(i);
            if(val==null) {
                jsonArr.addNull();
                continue;
            }
            type=val.getClass();

            if(type.isAssignableFrom(String.class)) {
                jsonArr.add((String)val);
                continue;
            }
            if(type.isAssignableFrom(Integer.class)) {
                jsonArr.add((Integer)val);
                continue;
            }
            if(type.isAssignableFrom(Date.class)) {
                synchronized(SDF) { jsonArr.add(SDF.format((Date)val)); }
                continue;
            }
            if(type.isAssignableFrom(BigDecimal.class)) {
                jsonArr.add((BigDecimal)val);
                continue;
            }
            if(type.isAssignableFrom(Long.class)) {
                jsonArr.add((Long)val);
                continue;
            }
            if(type.isAssignableFrom(long.class)) {
                jsonArr.add((long)val);
                continue;
            }
            if(type.isAssignableFrom(boolean.class)) {
                jsonArr.add((boolean)val);
                continue;
            }

            if(List.class.isAssignableFrom(type)) {
                jsonArr.add(convertListToJson((List)val, String.format("%1$s[%2$d]", path, i)));
                continue;
            }

            if(Map.class.isAssignableFrom(type)) {
                jsonArr.add(convertMapToJson((Map)val, String.format("%1$s[%2$d]", path, i)));
                continue;
            }

            throw new IllegalArgumentException(String.format("at '%1$s[%2$d]' found type:%3$s that is not handled", path, i, type.getName()));
        }

        return jsonArr.build();
    }

    public static boolean isSuccessResponse(JsonObject jobj) {
        return jobj.getString(PROPNAME_STATUS).equals(STATUS_SUCCESS);
    }
}
