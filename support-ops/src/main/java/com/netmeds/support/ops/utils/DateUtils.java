/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.netmeds.support.ops.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 *
 * @author prem
 */
public class DateUtils {

    private static final SimpleDateFormat SDF=new SimpleDateFormat("yyyy-MM-dd");
    private static final SimpleDateFormat SDFTM=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    
    private static final DateTimeFormatter DTF = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
    
    private static final Calendar CAL=new GregorianCalendar();

    public static Date parseDate(String str) throws ParseException {
        synchronized(SDF) {
            return SDF.parse(str);
        }
    }

    public static String formatDate(Date dt) {
        synchronized(SDF) {
            return SDF.format(dt);
        }
    }
    
    public static String formatLocalDateTime(LocalDateTime dt) {
        synchronized(DTF) {
            return dt.format(DTF);
        }
    }

    public static Date parseDateTime(String str) throws ParseException {
        synchronized(SDFTM) {
            return SDFTM.parse(str);
        }
    }
    
    public static LocalDateTime parseLocalDateTime(String str) {
        synchronized(DTF) {
            return LocalDateTime.parse(str, DTF);
        }
    }

    public static String formatDateTime(Date dt) {
        return String.format("%1$tF %1$tT", dt);
    }
    
    public static String formatDateTime(Date dt,String pattern) {
        SimpleDateFormat sdf = new SimpleDateFormat(pattern);
        return sdf.format(dt);
    }
    
    public static String formatLocalDateTime(LocalDateTime dt,String pattern) {
        DateTimeFormatter dtf =  DateTimeFormatter.ofPattern(pattern);
        return dt.format(dtf);
    }

    public static String formatForHuman(Date dt) {
        return String.format("%1$ta, %1$td %1$tb, %1$tY", dt);
    }

    public static Date addDays(Date dt, int days) {
        synchronized(CAL) {
            CAL.setTime(dt);
            CAL.add(Calendar.DATE, days);
            return CAL.getTime();
        }
    }

    public static int getDayOfWeek(Date dt) {
        synchronized(CAL) {
            CAL.setTime(dt);
            return CAL.get(Calendar.DAY_OF_WEEK);
        }
    }

    public static int getDayOfMonth(Date dt) {
        synchronized(CAL) {
            CAL.setTime(dt);
            return CAL.get(Calendar.DAY_OF_MONTH);
        }
    }

    public static int getDayOfYear(Date dt) {
        synchronized(CAL) {
            CAL.setTime(dt);
            return CAL.get(Calendar.DAY_OF_YEAR);
        }
    }

    public static int getHourOfDay(Date dt) {
        synchronized(CAL) {
            CAL.setTime(dt);
            return CAL.get(Calendar.HOUR_OF_DAY);
        }
    }

    public static int getMonthOfYear(Date dt) {
        synchronized(CAL) {
            CAL.setTime(dt);
            return CAL.get(Calendar.MONTH)+1;
        }
    }

    public static int getYear(Date dt) {
        synchronized(CAL) {
            CAL.setTime(dt);
            return CAL.get(Calendar.YEAR);
        }
    }

    public static Date getNextDay(Date dt) {
        return new Date(dt.getTime()+(86400*1000));
    }

    public static Date getPreviousDay(Date dt) {
        return new Date(dt.getTime()-(86400*1000));
    }

    public static Date getFirstDayOfWeek(Date dt) {
        int wd;

        synchronized(CAL) {
            CAL.setTime(dt);
            wd=CAL.get(Calendar.DAY_OF_WEEK)-Calendar.SUNDAY;
            CAL.add(Calendar.DAY_OF_WEEK, 0-wd);
            return CAL.getTime();
        }
    }

    public static Date getLastDayOfWeek(Date dt) {
        int wd;

        synchronized(CAL) {
            CAL.setTime(dt);
            wd=Calendar.SATURDAY-CAL.get(Calendar.DAY_OF_WEEK);
            CAL.add(Calendar.DAY_OF_WEEK, wd);
            return CAL.getTime();
        }
    }

    public static Date getPreviousWeek(Date dt) {
        return addToWeek(dt, -1);
    }

    public static Date getNextWeek(Date dt) {
        return addToWeek(dt, 1);
    }

    private static Date addToWeek(Date dt, int weekcount) {
        synchronized(CAL) {
            CAL.setTime(dt);
            CAL.add(Calendar.DATE, weekcount*7);
            return CAL.getTime();
        }
    }

    public static Date getFirstDayOfPreviousWeek(Date dt) {
        return getPreviousWeek(getFirstDayOfWeek(dt));
    }

    public static Date getFirstDayOfNextWeek(Date dt) {
        return getNextWeek(getFirstDayOfWeek(dt));
    }

    public static Date getFirstDayOfMonth(Date dt) {
        int d;

        synchronized(CAL) {
            CAL.setTime(dt);
            d=CAL.get(Calendar.DAY_OF_MONTH)-1;
            CAL.add(Calendar.DAY_OF_WEEK, 0-d);
            return CAL.getTime();
        }
    }

    public static Date getLastDayOfMonth(Date dt) {
        synchronized(CAL) {
            CAL.setTime(dt);
            CAL.set(Calendar.DAY_OF_MONTH, getDaysForMonth(CAL));
            return CAL.getTime();
        }
    }

    public static Date getPreviousMonth(Date dt) {
        return addToMonth(dt, -1);
    }

    public static Date getNextMonth(Date dt) {
        return addToMonth(dt, 1);
    }

    public static Date addToMonth(Date dt, int monthcount) {
        synchronized(CAL) {
            CAL.setTime(dt);
            CAL.add(Calendar.MONTH, monthcount);
            return CAL.getTime();
        }
    }

    public static Date getFirstDayOfPreviousMonth(Date dt) {
        return getPreviousMonth(getFirstDayOfMonth(dt));
    }

    public static Date getFirstDayOfNextMonth(Date dt) {
        return getNextMonth(getFirstDayOfMonth(dt));
    }

    public static int getDaysForMonth(Calendar cal) {
        int yr;
        int mon;

        yr=cal.get(Calendar.YEAR);
        mon=cal.get(Calendar.MONTH);
        return getDaysForMonth(mon, (yr%4)==0);
    }

    public static int getDaysForMonth(int m, boolean leapyear) {
        switch(m) {
            case Calendar.JANUARY:    return 31;
            case Calendar.FEBRUARY:   return leapyear?29:28;
            case Calendar.MARCH:      return 31;
            case Calendar.APRIL:      return 30;
            case Calendar.MAY:        return 31;
            case Calendar.JUNE:       return 30;
            case Calendar.JULY:       return 31;
            case Calendar.AUGUST:     return 31;
            case Calendar.SEPTEMBER:  return 30;
            case Calendar.OCTOBER:    return 31;
            case Calendar.NOVEMBER:   return 30;
            case Calendar.DECEMBER:   return 31;

            default: throw new IllegalArgumentException("bad month passed:"+m);
        }
    }

    public static Date getFirstDayOfYear(Date dt) {
        synchronized(CAL) {
            CAL.setTime(dt);
            CAL.set(Calendar.DAY_OF_YEAR, 1);
            return CAL.getTime();
        }
    }

    public static Date getLastDayOfYear(Date dt) {
        synchronized(CAL) {
            CAL.setTime(dt);
            CAL.set(Calendar.MONTH, Calendar.DECEMBER);
            CAL.set(Calendar.DAY_OF_MONTH, 31);
            return CAL.getTime();
        }
    }

    public static Date getPreviousYear(Date dt) {
        return addToYear(dt, -1);
    }

    public static Date getNextYear(Date dt) {
        return addToYear(dt, 1);
    }

    private static Date addToYear(Date dt, int yearcount) {
        synchronized(CAL) {
            CAL.setTime(dt);
            CAL.add(Calendar.YEAR, yearcount);
            return CAL.getTime();
        }
    }

    public static Date getFirstDayOfPreviousYear(Date dt) {
        return getPreviousYear(getFirstDayOfYear(dt));
    }

    public static Date getFirstDayOfNextYear(Date dt) {
        return getNextYear(getFirstDayOfYear(dt));
    }

    public static int getQuarterOfYear(Date dt) {
        return (getMonthOfYear(dt)+2)/3;
    }

    public static Date stripTime(Date dt) {
        long tm=dt.getTime();

        return new Date(tm-(tm%(86400000)));
    }

}
