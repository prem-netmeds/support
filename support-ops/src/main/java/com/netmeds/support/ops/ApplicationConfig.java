/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.netmeds.support.ops;

import java.util.Set;
import javax.ws.rs.core.Application;

/**
 *
 * @author NET00266
 */
@javax.ws.rs.ApplicationPath("rest/")
public class ApplicationConfig extends Application {
    
    public static final String URL_SESSION="/session";
    
    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> resources = new java.util.HashSet<>();
        addRestClasses(resources);
        return resources;
    }

    /**
     * Do not modify addRestResourceClasses() method.
     * It is automatically populated with
     * all resources defined in the project.
     * If required, comment out calling this method in getClasses().
     */
    private void addRestClasses(Set<Class<?>> resources) {
        resources.add(com.netmeds.support.ops.web.RequestAuthFilter.class);
        resources.add(com.netmeds.support.ops.web.SessionManagerREST.class);
        resources.add(com.netmeds.support.ops.web.MstarREST.class);
    }

}
