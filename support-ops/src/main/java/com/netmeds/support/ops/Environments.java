/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.netmeds.support.ops;

import com.netmeds.support.ops.utils.SQLFront;
import java.util.function.Supplier;

/**
 *
 * @author prem
 */
public enum Environments {
    NetmedsStage(
        "Netmeds Stage",
        ()->sf("com.mysql.jdbc.Driver","jdbc:mysql://v3-stage-env.cip33pywvytw.ap-south-1.rds.amazonaws.com:3306/s1_mstar"/*?zeroDateTimeBehavior=convertToNull&rewriteBatchedStatements=true*/, "<username>", "<password>"),
        ()->sf("com.mysql.jdbc.Driver","jdbc:mysql://v3-stage-env.cip33pywvytw.ap-south-1.rds.amazonaws.com:3306/tetrapod"/*?zeroDateTimeBehavior=convertToNull&rewriteBatchedStatements=true*/, "<username>", "<password>"),
        ()->sf("com.mysql.jdbc.Driver","jdbc:mysql://v3-stage-env.cip33pywvytw.ap-south-1.rds.amazonaws.com:3306/tpdlog"/*?zeroDateTimeBehavior=convertToNull&rewriteBatchedStatements=true*/, "<username>", "<password>"),
        "https://stg-tetrapod.netmeds.com"
    ),
    NetmedsProd(
        "Netmeds Prod",
        ()->sf("com.mysql.jdbc.Driver","jdbc:mysql://prod-mstar.cluster-ro-cip33pywvytw.ap-south-1.rds.amazonaws.com:3306/prod_mstar"/*?zeroDateTimeBehavior=convertToNull&rewriteBatchedStatements=true*/, "<username>", "<password>"),
        ()->sf("com.mysql.jdbc.Driver","jdbc:mysql://prod-mstar.cluster-ro-cip33pywvytw.ap-south-1.rds.amazonaws.com:3306/tp2_tetrapod"/*?zeroDateTimeBehavior=convertToNull&rewriteBatchedStatements=true*/, "<username>", "<password>"),
        ()->sf("com.mysql.jdbc.Driver","jdbc:mysql://prod-logs-cluster.cluster-cip33pywvytw.ap-south-1.rds.amazonaws.com:3306/tp2log"/*?zeroDateTimeBehavior=convertToNull&rewriteBatchedStatements=true*/, "<username>", "<password>"),
        "https://tp2.netmeds.com"
    ),
    JiomartStage(
        "JioMart Stage",
        ()->sf("com.mysql.jdbc.Driver","jdbc:mysql://jio-stage-env.cluster-cip33pywvytw.ap-south-1.rds.amazonaws.com:3306/s_jiomart_mstar"/*?zeroDateTimeBehavior=convertToNull&rewriteBatchedStatements=true*/, "<username>", "<password>"),
        ()->sf("com.mysql.jdbc.Driver","jdbc:mysql://jio-stage-env.cluster-cip33pywvytw.ap-south-1.rds.amazonaws.com:3306/s_jiomart_tetrapod"/*?zeroDateTimeBehavior=convertToNull&rewriteBatchedStatements=true*/, "<username>", "<password>"),
        ()->sf("com.mysql.jdbc.Driver","jdbc:mysql://jio-stage-env.cluster-cip33pywvytw.ap-south-1.rds.amazonaws.com:3306/s_jiomart_tetrapod_log"/*?zeroDateTimeBehavior=convertToNull&rewriteBatchedStatements=true*/, "<username>", "<password>"),
        "https://stage-jiomart.netmeds.com"
    ),
    JiomartPreProd(
        "JioMart PreProd",
        ()->sf("com.mysql.jdbc.Driver","jdbc:mysql://pp-jiomart-mstar.cluster-ro-cip33pywvytw.ap-south-1.rds.amazonaws.com:3306/pp_jiomart_mstar"/*?zeroDateTimeBehavior=convertToNull&rewriteBatchedStatements=true*/, "<username>", "<password>"),
        ()->sf("com.mysql.jdbc.Driver","jdbc:mysql://pp-jiomart-mstar.cluster-ro-cip33pywvytw.ap-south-1.rds.amazonaws.com:3306/pp_jiomart_tetrapod"/*?zeroDateTimeBehavior=convertToNull&rewriteBatchedStatements=true*/, "<username>", "<password>"),
        ()->sf("com.mysql.jdbc.Driver","jdbc:mysql://pp-jiomart-logs.cluster-ro-cip33pywvytw.ap-south-1.rds.amazonaws.com:3306/pp_jiomart_tetrapod_log"/*?zeroDateTimeBehavior=convertToNull&rewriteBatchedStatements=true*/, "<username>", "<password>"),
        "https://pp-jiomart.jionetmeds.com"
    ),
    JiomartProd(
        "JioMart Prod",
        ()->sf("com.mysql.jdbc.Driver","jdbc:mysql://p-jiomart-mstar.cluster-cip33pywvytw.ap-south-1.rds.amazonaws.com:3306/p_jiomart_mstar"/*?zeroDateTimeBehavior=convertToNull&rewriteBatchedStatements=true*/, "<username>", "<password>"),
        ()->sf("com.mysql.jdbc.Driver","jdbc:mysql://p-jiomart-mstar.cluster-ro-cip33pywvytw.ap-south-1.rds.amazonaws.com:3306/p_jiomart_tetrapod"/*?zeroDateTimeBehavior=convertToNull&rewriteBatchedStatements=true*/, "<username>", "<password>"),
        ()->sf("com.mysql.jdbc.Driver","jdbc:mysql://p-jiomart-logs.cluster-ro-cip33pywvytw.ap-south-1.rds.amazonaws.com:3306/p_jiomart_tetrapod_log"/*?zeroDateTimeBehavior=convertToNull&rewriteBatchedStatements=true*/, "<username>", "<password>"),
        "https://prod-jiomart.netmeds.com"
    ),
    JiomartExpStage(
        "JioMartExp Stage",
        ()->sf("com.mysql.jdbc.Driver","jdbc:mysql://sit.cluster-cjgmahseimzt.ap-south-1.rds.amazonaws.com:3306/s_jexp_mstar?zeroDateTimeBehavior=convertToNull&rewriteBatchedStatements=true", "<username>", "<password>"),
        ()->sf("com.mysql.jdbc.Driver","jdbc:mysql://sit.cluster-cjgmahseimzt.ap-south-1.rds.amazonaws.com:3306/s_jexp_tetrapod?zeroDateTimeBehavior=convertToNull&rewriteBatchedStatements=true", "<username>", "<password>"),
        ()->sf("com.mysql.jdbc.Driver","jdbc:mysql://sit.cluster-cjgmahseimzt.ap-south-1.rds.amazonaws.com:3306/s_jexp_tetrapodlog?zeroDateTimeBehavior=convertToNull&rewriteBatchedStatements=true", "<username>", "<password>"),
            "https://sit-jexp-int.jionetmeds.com"
    ),
    JiomartExpProd(
        "JioMartExp Prod",
        ()->sf("com.mysql.jdbc.Driver","jdbc:mysql://prod-mstar.cluster-cjgmahseimzt.ap-south-1.rds.amazonaws.com:3306/p_jexp_mstar"/*?zeroDateTimeBehavior=convertToNull&rewriteBatchedStatements=true*/, "<username>", "<password>"),
        ()->sf("com.mysql.jdbc.Driver","jdbc:mysql://prod-mstar.cluster-cjgmahseimzt.ap-south-1.rds.amazonaws.com:3306/p_jexp_tetrapod"/*?zeroDateTimeBehavior=convertToNull&rewriteBatchedStatements=true*/, "<username>", "<password>"),
        ()->sf("com.mysql.jdbc.Driver","jdbc:mysql://prod-jexp-logs.cluster-ro-cjgmahseimzt.ap-south-1.rds.amazonaws.com:3306/p_jexp_tetrapodlog"/*?zeroDateTimeBehavior=convertToNull&rewriteBatchedStatements=true*/, "<username>", "<password>"),
        "https://prod-jexp-int.jionetmeds.com"
    );

    private final String displayName;
    private final Supplier<SQLFront> mstarSFSupplier;
    private final Supplier<SQLFront> tpodSFSupplier;
    private final Supplier<SQLFront> tpodLogSFSupplier;
    private final String tetrapodURL;

    private Environments(String displayName, Supplier<SQLFront> mstarSFSupplier, Supplier<SQLFront> tpodSFSupplier, Supplier<SQLFront> tpodLogSFSupplier, String tetrapodURL) {
        this.displayName = displayName;
        this.mstarSFSupplier = mstarSFSupplier;
        this.tpodSFSupplier = tpodSFSupplier;
        this.tpodLogSFSupplier = tpodLogSFSupplier;
        this.tetrapodURL = tetrapodURL;
    }

    public String getDisplayName() { return displayName; }
    public SQLFront getMstarSF() { return mstarSFSupplier.get(); }
    public SQLFront getTpodSF() { return tpodSFSupplier.get(); }
    public SQLFront getTpodLogSF() { return tpodLogSFSupplier.get(); }
    public String getTetrapodURL() { return tetrapodURL; }


    private static SQLFront sf(String driver, String url, String userid, String passwd) {
        try {
            return new SQLFront(driver, url, userid, passwd);
        }
        catch(Exception e) {
            throw new RuntimeException(e);
        }
    }
}
