/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.netmeds.support.ops.utils;

import com.netmeds.support.ops.Environments;
import com.netmeds.support.ops.mstar.service.MStarService;
import static com.netmeds.support.ops.mstar.service.MStarService.getMStarSQLFront;
import static com.netmeds.support.ops.mstar.service.MStarService.getTetrapodSQLFront;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringReader;
import java.io.UncheckedIOException;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.ResultSet;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Stream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonValue;
import org.apache.commons.codec.binary.Base64;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 *
 * @author prem
 */
public class Utils {

    private static final Logger LOG=Logger.getLogger(Utils.class.getName());

    private Utils(){}

    public static final String[] specialCharsToRemove={"."," ","*","#","'","`","(",")","~","-",";","&","@","^","!","_",",","?","{","}","[","]","|","="};

    public static Stream<String[]> loadCSVFileAsFieldsStream(String resourcepath) {
        BufferedReader br;
        Stream<String[]> lineStream;

        try {
            br=new BufferedReader(new InputStreamReader(Utils.class.getResourceAsStream(resourcepath)));

            lineStream=br.lines()
                    .onClose(()->{ try { br.close(); } catch(IOException ioe) { throw new UncheckedIOException(ioe); } })
                    .skip(1)
                    .map(s->s.split(","))
                    ;
        }
        catch(UncheckedIOException ioe) {
            throw new UncheckedException(ioe.getCause());
        }

        return lineStream;
    }

    public static Boolean tobool(String str) {
        if(str.toLowerCase().equals("y")) return true;
        if(str.toLowerCase().equals("n")) return false;
        return null;
    }

    public static String getUploadFileBasePath() {
        return getServerFilesBasePath()+"/uploads";
    }
     public static String getTempFileBasePath() {
        return getServerFilesBasePath()+"/temp";
    }
    
    public static String getGdnLocalFileUploadBasePath() {
        return getServerFilesBasePath()+"/uploads/gdn-local-uploads";
    }
    
    public static String getDownloadFileBasePath() {
        return getServerFilesBasePath()+"/downloads";
    }
    
    public static String getZipFileBasePath() {
        return getServerFilesBasePath()+"/zip";
    }

    public static String getGeneratedFileBasePath() {
        return getServerFilesBasePath()+"/gen";
    }

    public static void createPathForFile(String file) {
        createPath(new File(file).getParentFile());
    }

    public static String getNamePartOfPath(String fullpath) {
        return new File(fullpath).getName();
    }

    public static void createPath(File ofp) {
        try {
            if (!ofp.exists()) {
                ofp.mkdirs();
            }
        } catch (Exception e) {
            LOG.log(Level.SEVERE, "attempt create base directory for output failed. thrown exception");
            LOG.log(Level.SEVERE, "exception:", e);
        }
    }

    public static String getServerFilesBasePath() {
        if(isThisWindowsOS()) {
            return "c:/var/gdn";
        } else {
            return "/var/gdn";
        }
    }

    private static boolean isThisWindowsOS() {
        return System.getProperty("os.name").toLowerCase().contains("windows");
    }

    @FunctionalInterface
    public static interface TriFunction<P1,P2,P3,R> {
        public R apply(P1 p1, P2 p2, P3 p3);
    }

    @FunctionalInterface
    public static interface QuadFunction<P1,P2,P3,P4,R> {
        public R apply(P1 p1, P2 p2, P3 p3, P4 p4);
    }

    @FunctionalInterface
    public static interface CheckedConsumer<T> {
        public void accept(T t) throws Exception;
    }

    public static <T> Consumer<T> uncheckedConsumer(CheckedConsumer<T> cs) {
        return t -> {
            try {
                cs.accept(t);
            } catch(Exception e) {
                throw new RuntimeException(e);
            }
        };
    }

    @FunctionalInterface
    public static interface CheckedClosedSubroutine {
        public void go() throws Exception;
    }

    @FunctionalInterface
    public static interface ClosedSubroutine {
        public void go();
    }

    public static ClosedSubroutine uncheckedCS(CheckedClosedSubroutine ccs) {
        return ()->{
            try { ccs.go(); }
            catch(Exception e) {
                throw new RuntimeException(e);
            }
        };
    }

    @FunctionalInterface
    public static interface CheckedFunction<P,R> {
        R apply(P p) throws Exception;
    }

    public static String digestStringToSHA1(String input) {
        return digestString(input, "SHA-1");
    }

    public static String digestStringToSHA256(String input) {
        return digestString(input, "SHA-256");
    }
    
    public static String encodeStringToBase64(String input){
        byte[] encodedBytes=Base64.encodeBase64(input.getBytes());
        return new String(encodedBytes);
    }
    
    public static String decodeBase64ToString(byte[] endocedBytes){
        byte[] decodeBytes=Base64.decodeBase64(endocedBytes);
        return new String(decodeBytes);
    }

    private static String digestString(String input, String algo) {
        MessageDigest md;
        byte[] digest;

        try {
            md = MessageDigest.getInstance(algo);
            digest = md.digest(input.getBytes("UTF-8"));

            return encodeToHex(digest);
        } catch (NoSuchAlgorithmException | UnsupportedEncodingException ne) {
            return null;
        }
    }

    public static String encodeToHex(byte[] data) {
        return new BigInteger(1, data).toString(16).toLowerCase();
    }
    
    public static double convertStringToDouble(String strVal){
        double doubleVal=0.0;
        try{
            doubleVal=Double.valueOf(strVal);
        }catch(Exception ex){
            //LOG.log(Level.INFO, String.format("convert string to double: %1$s | String value: %2$s", ex,strVal));
        }
        return doubleVal;        
    }
    
    public static int convertStringToInt(String strVal){
        int intVal=0;
        double stkDouble;
        try{
            stkDouble=Double.valueOf(strVal);
            intVal=(int)stkDouble;
        }catch(Exception ex){
            //LOG.log(Level.INFO, String.format("convert string to int: %1$s | String value: %2$s", ex,strVal));
        }
        return intVal;        
    }       
    
    public static boolean zipFiles(List<String> files,File fileName){
        boolean zipStatus=false;
        FileOutputStream fos = null;
        ZipOutputStream zipOut = null;
        FileInputStream fis = null;
        try {
            fos = new FileOutputStream(fileName);
            zipOut = new ZipOutputStream(new BufferedOutputStream(fos));
            for(String filePath:files){
                File input = new File(filePath);
                fis = new FileInputStream(input);
                ZipEntry ze = new ZipEntry(input.getName());
                //System.out.println("Zipping the file: "+input.getName());
                LOG.log(Level.INFO, "Zipping the file: ", input.getName());
                zipOut.putNextEntry(ze);
                byte[] tmp = new byte[4*1024];
                int size = 0;
                while((size = fis.read(tmp)) != -1){
                    zipOut.write(tmp, 0, size);
                }
                zipOut.flush();
                fis.close();
            }
            zipOut.close();
            zipStatus=true;
            LOG.log(Level.INFO, "Done... Zipped the files...");            
        } catch (FileNotFoundException e) {            
            LOG.log(Level.SEVERE, "Exception Occured while zipping files:", e);
           
        } catch (IOException e) {
            LOG.log(Level.SEVERE, "Exception Occured while zipping files:", e);
        } finally{
            try{
                if(fos != null) fos.close();
            } catch(Exception ex){
                 
            }
        }
        return zipStatus;
    }    
    
    public static long getLastRow(ResultSet resultSet){
        if(resultSet==null){
            return 0;
        }
        try{
            resultSet.last();
            return resultSet.getRow();
        }catch(Exception ex){
            LOG.log(Level.INFO,String.format("Exception occurred while get last row: %1$s",ex));
        }
        finally{
            try{
                resultSet.beforeFirst();
            }catch(Exception ex){
                LOG.log(Level.INFO,String.format("Exception occurred while get last row: %1$s",ex));
            }            
        }        
        return 0;
    }
    public static JsonObject getJSOB(String str) {
        JsonReader jr = Json.createReader(new StringReader(str));
        JsonObject jobj = jr.readObject();
        jr.close();
        return jobj;
    }
    public static Map<String,List<String>> getFileList(String rootFolderName,List<String>folderList,List<String>fileList) throws IOException{
        File rootFolder=new File(rootFolderName);                
        for(File fi:rootFolder.listFiles()){
            if(fi.isDirectory()){
                getFileList(fi.getPath()+"\\",folderList,fileList);
                folderList.add(fi.getPath());
            }else{
                fileList.add(fi.getPath());
            }
        }
        Map<String,List<String>> fileandFolderMap=new HashMap<>();
        fileandFolderMap.put("folder_list",folderList);
        fileandFolderMap.put("file_list", fileList);
        return fileandFolderMap;
    }
    private static void getTpEventIdAndStatus(List<String>orderIdLi,String envName) {
        SQLFront mstSF = null;
        SQLFront tpdSF = null;
        try {
            System.out.println("order_id|tp_event_id|queue_status");
            mstSF = getMStarSQLFront(envName); 
            tpdSF = getTetrapodSQLFront(envName); 
            int mobNoLiSize = orderIdLi.size();
            int loopCount;
            int batchSize = 500;
            int bal = orderIdLi.size() % batchSize;
            if(mobNoLiSize >= batchSize){
                mobNoLiSize = orderIdLi.size() - (orderIdLi.size() % batchSize);
                loopCount = mobNoLiSize/batchSize;
            }else{
                loopCount = 1;
            }
            for (int i = 0; i < loopCount; i++) {
                int fromIdx;
                int toIdx;
                
                if(mobNoLiSize >= batchSize){
                    fromIdx = i * batchSize;
                    toIdx = (i * batchSize)+batchSize;
                    if((i+1) == loopCount){
                        toIdx = toIdx + bal;
                    }
                }else{
                    fromIdx = 0;
                    toIdx = mobNoLiSize;
                }
                boolean first = true;
                String orderId;
                StringBuilder inputSB = new StringBuilder();
                for (int j = fromIdx; j < toIdx; j++) {
                    orderId = orderIdLi.get(j);
                    if(first){
                        inputSB.append("'").append(orderId).append("'");
                        first = false;
                    }else{
                        inputSB.append(",'").append(orderId).append("'");
                    }
                }

                String sql = "SELECT order_id,tp_event_id FROM mst_cart\n" +
                            "where order_id in ("+inputSB+")\n" +
                            ";";
                List<Map<String,Object>> cartIdLi = mstSF.getResult(sql);
                first = true;
                inputSB = new StringBuilder();
                int tpEventId;
                String queueStatus;
                Map<Integer,String> tpEventIdNOrderId = new LinkedHashMap();
                Map<String,Integer> orderIdNTpEventId = new LinkedHashMap();
                for (int j = 0; j < cartIdLi.size(); j++) {
                    Map<String, Object> cartIdRec = cartIdLi.get(j);
                    tpEventId = (cartIdRec.get("tp_event_id")!=null)?(int)cartIdRec.get("tp_event_id"):0;
                    orderId = (String)cartIdRec.get("order_id");
                    orderIdNTpEventId.put(orderId,tpEventId);
                    if(tpEventId!=0){
                        tpEventIdNOrderId.put(tpEventId,orderId);
                        if(first){
                            inputSB.append(tpEventId);
                            first = false;
                        }else{
                            inputSB.append(",").append(tpEventId);
                        }
                    }
                }
                sql = "SELECT event_queue_id,queue_status FROM tpd_action_queue\n" +
                            "where event_queue_id in ("+inputSB+")\n" +
                            ";";
                
                List<Map<String,Object>> eventQRecLi = tpdSF.getResult(sql);
                Map<Integer,String> tpEventIdNQueueStatus = new LinkedHashMap();
                for (int j = 0; j < eventQRecLi.size(); j++) {
                    Map<String, Object> eventQRec = eventQRecLi.get(j);
                    tpEventId = (int)eventQRec.get("event_queue_id");
                    queueStatus = (String)eventQRec.get("queue_status");
                    tpEventIdNQueueStatus.put(tpEventId,queueStatus);
                }
                for (String odrId : orderIdNTpEventId.keySet()) {
                    if(orderIdNTpEventId.get(odrId)!=0){
                        System.out.println(odrId+"|"+orderIdNTpEventId.get(odrId)+"|"+tpEventIdNQueueStatus.get(orderIdNTpEventId.get(odrId)));
                    }else{
                        System.out.println(odrId+"|null|null");
                    }
                }
            }  
        } catch (Exception e) {
            e.printStackTrace();
        }finally{
            if(mstSF!=null)mstSF.close();
            if(tpdSF!=null)tpdSF.close();
        }
    }
    private static void getMOPFromDatabase(List<String>orderIdLi,String envName) {
        SQLFront mstSF = null;
        try {
            List<Map<String,Object>> mopRecLi = new ArrayList();
            mstSF = getMStarSQLFront(envName); 
            int mobNoLiSize = orderIdLi.size();
            int loopCount;
            int batchSize = 2000;
            int bal = orderIdLi.size() % batchSize;
            if(mobNoLiSize >= batchSize){
                mobNoLiSize = orderIdLi.size() - (orderIdLi.size() % batchSize);
                loopCount = mobNoLiSize/batchSize;
            }else{
                loopCount = 1;
            }
            for (int i = 0; i < loopCount; i++) {
                int fromIdx;
                int toIdx;
                
                if(mobNoLiSize >= batchSize){
                    fromIdx = i * batchSize;
                    toIdx = (i * batchSize)+batchSize;
                    if((i+1) == loopCount){
                        toIdx = toIdx + bal;
                    }
                }else{
                    fromIdx = 0;
                    toIdx = mobNoLiSize;
                }
                boolean first = true;
                String orderId;
                StringBuilder inputSB = new StringBuilder();
                for (int j = fromIdx; j < toIdx; j++) {
                    orderId = orderIdLi.get(j);
                    if(first){
                        inputSB.append("'").append(orderId).append("'");
                        first = false;
                    }else{
                        inputSB.append(",'").append(orderId).append("'");
                    }
                }
                String sql;
                
                sql = "select * from mst_customer where mobile_no in ("+inputSB+");";
                
                //sql = "select * from mst_myjio_webhook_log where order_id in ("+inputSB+");";
                
                /*
                sql = "select \n" +
                "	oih.order_id,\n" +
                "    cust.mobile_no \n" +
                "from mst_cart cart \n" +
                "left join mst_customer cust on cart.customer_id = cust.id\n" +
                "left join mst_order_id_history oih on cart.id = oih.cart_id\n" +
                "where oih.order_id in ("+inputSB+");";
                System.out.println("sql: "+sql);
                */
                
                /*
                sql = "SELECT cart.id as `cart;id`,\n" +
                        "cart.customer_id as `cart;customer_id`,\n" +
                        "cart.cart_status as `cart;cart_status`,\n" +
                        "cart.closed_by_client as `cart;closed_by_client`,\n" +
                        "cart.closed_by_server as `cart;closed_by_server`,\n" +
                        "cart.method2 as `cart;method2`,\n" +
                        "cart.created_channel as `cart;created_channel`,\n" +
                        "cart.checkout_channel as `cart;checkout_channel`,\n" +
                        "cart.checkout_ip as `cart;checkout_ip`,\n" +
                        "cart.is_edited as `cart;is_edited`,\n" +
                        "cart.edit_scope as `cart;edit_scope`,\n" +
                        "cart.payment_type as `cart;payment_type`,\n" +
                        "cart.billing_address_id as `cart;billing_address_id`,\n" +
                        "cart.shipping_address_id as `cart;shipping_address_id`,\n" +
                        "cart.created_time as `cart;created_time`,\n" +
                        "cart.applied_iho_vouchers as `cart;applied_iho_vouchers`,\n" +
                        "cart.applied_general_vouchers as `cart;applied_general_vouchers`,\n" +
                        "cart.applied_coupons as `cart;applied_coupons`,\n" +
                        "cart.use_wallet_balance as `cart;use_wallet_balance`,\n" +
                        "cart.use_nms_cash as `cart;use_nms_cash`,\n" +
                        "cart.use_super_cash as `cart;use_super_cash`,\n" +
                        "cart.use_store_credit as `cart;use_store_credit`,\n" +
                        "cart.use_employee_limit as `cart;use_employee_limit`,\n" +
                        "cart.use_loyalty_points as `cart;use_loyalty_points`,\n" +
                        "cart.useable_wallet_amount as `cart;useable_wallet_amount`,\n" +
                        "cart.useable_nmscash_amount as `cart;useable_nmscash_amount`,\n" +
                        "cart.useable_supercash_amount as `cart;useable_supercash_amount`,\n" +
                        "cart.available_store_credit as `cart;available_store_credit`,\n" +
                        "cart.available_employee_limit as `cart;available_employee_limit`,\n" +
                        "cart.available_loyalty_points as `cart;available_loyalty_points`,\n" +
                        "cart.iho_voucher_amount as `cart;iho_voucher_amount`,\n" +
                        "cart.general_voucher_amount as `cart;general_voucher_amount`,\n" +
                        "cart.prepaid_amount as `cart;prepaid_amount`,\n" +
                        "cart.cod_amount as `cart;cod_amount`,\n" +
                        "cart.wallet_amount as `cart;wallet_amount`,\n" +
                        "cart.nmscash_amount as `cart;nmscash_amount`,\n" +
                        "cart.supercash_amount as `cart;supercash_amount`,\n" +
                        "cart.doctor_consultation_needed as `cart;doctor_consultation_needed`,\n" +
                        "cart.order_id as `cart;order_id`,\n" +
                        "cart.updated_time as `cart;updated_time`,\n" +
                        "cart.payment_aggregator as `cart;payment_aggregator`,\n" +
                        "cart.payment_from as `cart;payment_from`,\n" +
                        "cart.payment_status as `cart;payment_status`,\n" +
                        "cart.payment_amount as `cart;payment_amount`,\n" +
                        "cart.emi_interest_amount as `cart;emi_interest_amount`,\n" +
                        "cart.instant_discount_amount as `cart;instant_discount_amount`,\n" +
                        "cart.payment_txnid as `cart;payment_txnid`,\n" +
                        "cart.webhook_status as `cart;webhook_status`,\n" +
                        "cart.tp_event_id as `cart;tp_event_id`,\n" +
                        "cart.tp_status as `cart;tp_status`,\n" +
                        "cart.order_status as `cart;order_status`,\n" +
                        "cart.payment_conclusion_time as `cart;payment_conclusion_time`,\n" +
                        "cart.cart_type as `cart;cart_type`,\n" +
                        "cart.m2_status as `cart;m2_status`,\n" +
                        "cart.reference_id as `cart;reference_id`,\n" +
                        "cart.wasuc as `cart;wasuc`\n"
                        + " FROM mst_cart cart\n" +
                            "where order_id in ("+inputSB+")\n" +
                            ";";
                */
//                sql = "select \n" +
//                "	order_id,push_result,push_time \n" +
//                "from mst_juspay_webhook_log \n" +
//                "where order_id in ("+inputSB+") \n" +
//                "and push_result is not null\n" +
//                ";";
//                sql = "select * from mst_payment_history where order_id in ("+inputSB+");";
                /*
                sql = "select \n" +
                "	order_id,\n" +
                "    cust.mobile_no,\n" +
                "    payment_aggregator,\n" +
                "    payment_from,\n" +
                "    payment_amount,\n" +
                "    payment_txnid,\n" +
                "    payment_type,\n" +
                "    payment_status,\n" +
                "    payment_conclusion_time \n" +
                "from mst_cart cart\n" +
                "left join mst_customer cust on cust.id = cart.customer_id\n" +
                "where order_id in ("+inputSB+");";
                */
                List<Map<String,Object>> recList = mstSF.getResult(sql);
                mopRecLi.addAll(recList);
                if(mopRecLi.size()>300000){
                    writeMopDataInFile(mopRecLi);
                    mopRecLi = new ArrayList(); 
                }
                System.out.println("part"+(i+1)+" of "+loopCount);
                System.out.println("mopRecLi size: "+mopRecLi.size());
            }
            if(!mopRecLi.isEmpty())writeMopDataInFile(mopRecLi);
        } catch (Exception e) {
            e.printStackTrace();
        }finally{
            if(mstSF!=null)mstSF.close();
        }
    }
    private static void writeMopDataInFile(List<Map<String,Object>> mopRecLi){
        System.out.println("mop rec. start writing into file...");
        try {
            LocalDateTime now=LocalDateTime.now();
            String filename=String.format("%1$s/Desktop/all-mop-%2$s.csv", System.getProperty("user.home"), String.format("%1$tY%1$tm%1$td-%1$tH%1$tM%1$tS", now));
            PrintWriter out;
            out=new PrintWriter(new FileWriter(filename));
            for (int i = 0; i < mopRecLi.size(); i++) {
                Map<String, Object> mopRec = mopRecLi.get(i);
                boolean first = true;
                StringBuilder sb = new StringBuilder();
                for (String key : mopRec.keySet()) {
                    Object value = (mopRec.get(key)!=null)?mopRec.get(key):"";
                    if(first){
                        sb.append(value);
                        first = false;
                    }else{
                        sb.append("|").append(value);
                    }
                }
                out.println(sb);
            }
            out.close();
            System.out.println("mop rec. successfully writen into file!");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public static List<String> getOrderIdListFromFile(String fileName,int sheetIndex,int startFromRow,int orderIdColNum){
        List<String> orderIdLi = new LinkedList();
        try {
            File file = new File(fileName);
            Workbook wkb = new XSSFWorkbook(new FileInputStream(file));
            
            Sheet sht = wkb.getSheetAt(sheetIndex);
            int rowCount = sht.getLastRowNum();
            Row row;
            String orderId;
            for (int i = startFromRow; i < rowCount + 1; i++) {
                row = sht.getRow(i);
                orderId = row.getCell(orderIdColNum).toString();
                orderIdLi.add(orderId.replace("\u00A0"," ").trim());
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return orderIdLi;
    }
    
    public static List<Map<String,Object>> getMStarOrderAnalysisRecordList(List<String> orderIdLi,String env,boolean writeReqResJson){
        List<Map<String,Object>> orderAnalysisRecLi = new LinkedList();
        SQLFront mstarSF = null;
        SQLFront tetrapodLogSF = null;
        
        try {
            mstarSF = MStarService.getMStarSQLFront(env);
            tetrapodLogSF = MStarService.getTetrapodLogSQLFront(env);
            
            orderAnalysisRecLi = new LinkedList();
            System.out.println("cart_id|cart_status|given_order_id|channel_id|cart_order_id|order_id_passed_to_oms|"
                    + "fynd_order_id|additional_message|push_result|myjio_process_log|total_coupon_discount|"
                    + "total_product_discount|total_promo_discount|total_order_discount|final_order_amount|order_type|"
                    + "order_type|gross_total|delivery_charges|cod_charges|cashback_value|http_status|"
                    + "status_message|response_json|out_of_server_skus|payment_method|created_time");
            
            if(orderIdLi!=null && !orderIdLi.isEmpty()){
                    int batchSize = 500;
                    int orderIdLiSize = orderIdLi.size();
                    int loopCount;
                    int bal = orderIdLi.size() % batchSize;
                    if(orderIdLiSize >= batchSize){
                        orderIdLiSize = orderIdLi.size() - (orderIdLi.size() % batchSize);
                        loopCount = orderIdLiSize/batchSize;
                    }else{
                        loopCount = 1;
                    }
                    for (int i = 0; i < loopCount; i++) {
                        int fromIdx;
                        int toIdx;
                        if(orderIdLiSize >= batchSize){
                            fromIdx = i * batchSize;
                            toIdx = (i * batchSize)+batchSize;
                            if((i+1) == loopCount){
                            toIdx = toIdx + bal;
                        }
                        }else{
                            fromIdx = 0;
                            toIdx = orderIdLiSize;
                        }
                        //System.out.println("fromIdx: "+fromIdx+", toIdx: "+toIdx);
                        boolean first = true;
                        StringBuilder inputSB = new StringBuilder();
                        for (int j = fromIdx; j < toIdx; j++) {
                            String orderId = orderIdLi.get(j);
                            if(first){
                                inputSB.append("'").append(orderId).append("'");
                                first = false;
                            }else{
                                inputSB.append(",'").append(orderId).append("'");
                            }
                        }
                        String sql = "select\n" +
                        "	oih.cart_id,\n" +
                        "	cart.cart_status,\n" +
                        "    oih.order_id as given_order_id,\n" +
                        "    cart.order_id as cart_order_id,\n" +
                        "    case when cart.tp_event_id is null then\n" +
                        "		concat('order not reached to tetrapod system.',\n" +
                        "				' payment_status: ',case when cart.payment_status is null then 'null' else cart.payment_status end,\n" +
                        "				', order_status: ',case when cart.order_status is null then 'null' else cart.order_status end\n" +
                        "                )\n" +
                        "        else\n" +
                        "			case when oih.order_id <> cart.order_id then 'order id found through order_id_history' else null end\n" +
                        "	end as additional_message," +
                        "    cart.tp_event_id,\n" +
                        "    cart.payment_conclusion_time    \n" +
                        "from mst_order_id_history oih\n" +
                        "left join mst_cart cart on cart.id = oih.cart_id\n" +
                        "where \n" +
                        "	oih.order_id in ("+inputSB+")\n" +
                        ";";
                        List<Map<String,Object>> orderHisRecLi = mstarSF.getResult(sql);
                        sql = "select \n" +
                        "	order_id,concat('push_result: ',push_result) as push_result \n" +
                        "from mst_juspay_webhook_log \n" +
                        "where order_id in ("+inputSB+") \n" +
                        "and push_result is not null\n" +
                        ";";
                        
                        List<Map<String,Object>> pushRsltRecLi = mstarSF.getResult(sql);
                        Map<String,Object> pushRsltRec = new HashMap();
                        for (int j = 0; j < pushRsltRecLi.size(); j++) {
                            Map<String, Object> rec = pushRsltRecLi.get(j);
                            pushRsltRec.put(rec.get("order_id")+"",rec.get("push_result"));
                        }
                        sql = "select \n" +
                        "	order_id,concat('myjio_process_log: ',process_log) as process_log \n" +
                        "from mst_myjio_webhook_log \n" +
                        "where order_id in ("+inputSB+")\n" +
                        "group by order_id,process_log\n" +
                        ";";
                        List<Map<String,Object>> myjioPrsLogRecLi = mstarSF.getResult(sql);
                        Map<String,Object> myjioPrsLogRec = new HashMap();
                        for (int j = 0; j < myjioPrsLogRecLi.size(); j++) {
                            Map<String, Object> rec = myjioPrsLogRecLi.get(j);
                            myjioPrsLogRec.put(rec.get("order_id")+"",rec.get("process_log"));
                        }
                        first = true;
                        inputSB = new StringBuilder();
                        for (int j = 0; j < orderHisRecLi.size(); j++) {
                            Map<String, Object> orderHisRec = orderHisRecLi.get(j);
                            if(orderHisRec.get("tp_event_id") != null){
                                int tpEventId = (int)orderHisRec.get("tp_event_id");
                                if(first){
                                    inputSB.append(tpEventId);
                                    first = false;
                                }else{
                                    inputSB.append(",").append(tpEventId);
                                }
                            }
                        }
                        Map<Integer,Map<String,Object>> tpEventIdActionLogRec = new HashMap();
                        if(!inputSB.toString().isEmpty()){
                            sql = "select * from tpd_action_log where event_queue_id in ("+inputSB+") order by http_status;";
                            List<Map<String,Object>> actionLogRecLi = tetrapodLogSF.getResult(sql);
                            for (int j = 0; j < actionLogRecLi.size(); j++) {
                                Map<String, Object> actionLog = actionLogRecLi.get(j);
                                int eventQueueId = (int)actionLog.get("event_queue_id");
                                tpEventIdActionLogRec.put(eventQueueId, actionLog);
                            }
                        }
                        for (int j = 0; j < orderHisRecLi.size(); j++) {
                            String givenOrderId,cartOrderId,cartStatus,addMsg,httpStatus = null,reqJsonStr,resJsonStr = null,
                                    paymentMethod = null,outOfServerSkus = null,orderIdPassedToOMS = null,
                                    fyndOrderId = null,statusMsg = null,reqResJsonVer = null,channelId = null,
                                    pushResult = null, myjioProcessLog = null,orderType=null;
                            int cartId,tpEventId;
                            JsonArray paymentMethods;
                            double finalOrderAmt = 0,totalCouponDis = 0,totalPrdDis = 0,
                                    totalPromoDis = 0,totalOrderDis = 0,grossTotatl = 0, delCharges = 0,
                                    codCharges = 0, cashbackValue = 0;
                            
                            Date createdTime=null;
                            Map<String, Object> orderHisRec = orderHisRecLi.get(j);
                            cartId = (int)orderHisRec.get("cart_id");
                            cartStatus = (String)orderHisRec.get("cart_status");
                            givenOrderId = (String)orderHisRec.get("given_order_id");
                            cartOrderId = (String)orderHisRec.get("cart_order_id");
                            addMsg = (orderHisRec.get("additional_message")!=null)?(String)orderHisRec.get("additional_message"):null;
                            pushResult = (pushRsltRec.get(givenOrderId)!=null)?(String)pushRsltRec.get(givenOrderId):null;
                            myjioProcessLog = (myjioPrsLogRec.get(givenOrderId)!=null)?(String)myjioPrsLogRec.get(givenOrderId):null;
                            if(myjioProcessLog!=null && myjioProcessLog.length()>150)
                                myjioProcessLog = myjioProcessLog.substring(0, 150) + "...";
                            if(orderHisRec.get("tp_event_id") != null){
                                tpEventId = (int)orderHisRec.get("tp_event_id");
                                if(tpEventIdActionLogRec.containsKey(tpEventId)){
                                    Map<String, Object> actionLogRec =  tpEventIdActionLogRec.get(tpEventId);
                                    httpStatus = actionLogRec.get("http_status")+"";
                                    if(httpStatus.equals("200")){
                                            reqJsonStr = actionLogRec.get("request_msg")+"";
                                            resJsonStr = actionLogRec.get("response_msg")+"";
                                            resJsonStr = resJsonStr.replaceAll("[\\n\\t ]", "");
                                            
                                            if(writeReqResJson){
                                                writeFile(reqJsonStr, cartOrderId+"-request");
                                                writeFile(resJsonStr, cartOrderId+"-response");
                                            }
                                            
                                            createdTime = (Date)actionLogRec.get("created_time");
                                            JsonObject reqJsonObj = JsonUtils.convertStringToJsonObject(reqJsonStr);
                                            JsonObject resJsonObj = null;
                                            
                                            if(reqJsonObj.containsKey("final_order_amount")){
                                                reqResJsonVer = "v1";
                                            }else{
                                                reqResJsonVer = "v2";
                                            }
                                            
                                            //JsonUtils.printOrderReqJson(reqJsonObj);
                                            try {
                                                resJsonObj = JsonUtils.convertStringToJsonObject(resJsonStr);
                                            } catch (Exception e) {
                                            }
                                            
                                            paymentMethods = reqJsonObj.getJsonArray("payment_methods");
                                            /*
                                            for (int j = 0; j < paymentMethods.size(); j++) {
                                                JsonObject pm = (JsonObject)paymentMethods.get(j);
                                                if(orderAppCodeAndAmt == null){
                                                    orderAppCodeAndAmt = pm.getString("order_app_code")+":"+pm.getJsonNumber("payment_amt").doubleValue();
                                                }else{
                                                    orderAppCodeAndAmt = ";"+pm.getString("order_app_code")+":"+pm.getJsonNumber("payment_amt").doubleValue();
                                                }
                                            }
                                            */
                                            if(reqResJsonVer.equalsIgnoreCase("v1")){
                                                try {                                                    
                                                    finalOrderAmt = reqJsonObj.getJsonNumber("final_order_amount").doubleValue();
                                                    paymentMethod = reqJsonObj.getString("payment_method");
                                                    JsonArray products = reqJsonObj.getJsonArray("products");
                                                    StringBuilder skus = new StringBuilder();
                                                    first = true;

                                                    for (int k = 0; k < products.size(); k++) {
                                                        JsonObject product = products.getJsonObject(k);
                                                        if(product.getInt("processed_quantity")==0){
                                                            if(first){
                                                                try {skus.append(product.getJsonNumber("sku"));} catch (Exception e) {}
                                                                first = false;
                                                            }else{
                                                                try {skus.append(", ").append(product.getJsonNumber("sku"));} catch (Exception e) {}

                                                            }
                                                        }
                                                    }
                                                    outOfServerSkus = skus.toString();
                                                } catch (Exception e) {
                                                    
                                                }
                                            }
                                            
                                            if(reqResJsonVer.equalsIgnoreCase("v2")){
                                                try {//                                                    
                                                    JsonObject orderPrices = reqJsonObj.getJsonObject("order_prices");
                                                    
                                                    finalOrderAmt = orderPrices.getJsonNumber("final_amount").doubleValue();
                                                    totalCouponDis = orderPrices.getJsonNumber("total_coupon_discount").doubleValue();
                                                    totalPrdDis = orderPrices.getJsonNumber("total_product_discount").doubleValue();
                                                    totalPromoDis = orderPrices.getJsonNumber("total_promo_discount").doubleValue();
                                                    totalOrderDis = orderPrices.getJsonNumber("total_order_discount").doubleValue();
                                                    grossTotatl = orderPrices.getJsonNumber("gross_total").doubleValue();
                                                    delCharges = orderPrices.getJsonNumber("delivery_charges").doubleValue();
                                                    codCharges = orderPrices.getJsonNumber("cod_charges").doubleValue();
                                                    cashbackValue = orderPrices.getJsonNumber("cashback_value").doubleValue();
                                                    orderType = orderPrices.getString("order_type");
                                                    
                                                    JsonArray products = reqJsonObj.getJsonArray("order_products_sku");
                                                    channelId = reqJsonObj.getJsonObject("order_details").getString("channel_id");
                                                } catch (Exception e) {
                                                }
                                            }
                                            
                                            if(resJsonObj!=null){
                                                if(reqResJsonVer.equalsIgnoreCase("v1")){
                                                    try {
                                                        orderIdPassedToOMS = resJsonObj.getString("temp_order_id");
                                                        fyndOrderId = JsonUtils.convertJsonValueToObject(resJsonObj, "order_id", Arrays.asList(JsonValue.ValueType.STRING,JsonValue.ValueType.NULL))+"";
                                                        statusMsg = resJsonObj.getString("status_Message").replaceAll("\r", "").replaceAll("\n", "");
                                                    } catch (Exception e) {
                                                    }
                                                }else if(reqResJsonVer.equalsIgnoreCase("v2")){
                                                    try {
                                                        orderIdPassedToOMS = resJsonObj.getString("jiomart_order_id");
                                                        fyndOrderId = JsonUtils.convertJsonValueToObject(resJsonObj, "fynd_order_id", Arrays.asList(JsonValue.ValueType.STRING,JsonValue.ValueType.NULL))+"";
                                                        statusMsg = resJsonObj.getString("message").replaceAll("\r", "").replaceAll("\n", "");
                                                    } catch (Exception e) {
                                                    }
                                                }
                                            }
                                        }else{
                                            reqJsonStr = actionLogRec.get("request_msg")+"";
                                            writeFile(reqJsonStr, givenOrderId+"-request");
                                            resJsonStr = actionLogRec.get("response_msg")+"";
                                        }
                                }else{
                                    addMsg="invalid/no records for the 'event_queue_id' from table 'tpd_action_log'";
                                }
                            }else{
                                if(addMsg==null)addMsg="order not reached to tetrapod system";
                            }        
                            
                            System.out.println(
                                    cartId+"|"+cartStatus+"|"+givenOrderId+"|"+channelId+"|"+cartOrderId+"|"+orderIdPassedToOMS+"|"+
                                    fyndOrderId+"|"+addMsg+"|"+pushResult+"|"+myjioProcessLog+"|"+totalCouponDis+"|"+
                                    totalPrdDis+"|"+totalPromoDis+"|"+totalOrderDis+"|"+finalOrderAmt+"|"+orderType+"|"+
                                    grossTotatl+"|"+delCharges+"|"+codCharges+"|"+cashbackValue+"|"+httpStatus+"|"+
                                    statusMsg+"|"+resJsonStr+"|"+outOfServerSkus+"|"+paymentMethod+"|"+
                                    ((createdTime!=null)?DateUtils.formatDateTime(createdTime):null)
                            );
                            Map<String,Object> orderAnalysisRec = new LinkedHashMap();
                            orderAnalysisRec.put("cart_id",cartId);
                            orderAnalysisRec.put("cart_status",cartStatus);
//                            orderAnalysisRec.put("given_order_id",givenOrderId);
                            orderAnalysisRec.put("order_id",givenOrderId);
                            orderAnalysisRec.put("channel_id",channelId);
                            //orderAnalysisRec.put("cart_order_id",cartOrderId);
                            orderAnalysisRec.put("retry_order_id",(orderIdPassedToOMS!=null)?(!orderIdPassedToOMS.equals(givenOrderId))?orderIdPassedToOMS:null:(!givenOrderId.equals(cartOrderId))?cartOrderId:null);
                            orderAnalysisRec.put("fynd_order_id",fyndOrderId);
                            orderAnalysisRec.put("additional_message",addMsg);
                            orderAnalysisRec.put("push_result",pushResult);
                            orderAnalysisRec.put("myjio_process_log",myjioProcessLog);
                            
                            orderAnalysisRec.put("total_coupon_discount",totalCouponDis);
                            orderAnalysisRec.put("total_product_discount",totalPrdDis);
                            orderAnalysisRec.put("total_promo_discount",totalPromoDis);
                            orderAnalysisRec.put("total_order_discount",totalOrderDis);
                            orderAnalysisRec.put("final_amount",finalOrderAmt);
                            orderAnalysisRec.put("order_type",orderType);
                            orderAnalysisRec.put("gross_total",grossTotatl);
                            orderAnalysisRec.put("delivery_charges",delCharges);
                            orderAnalysisRec.put("cod_charges",codCharges);
                            orderAnalysisRec.put("cashback_value",cashbackValue);                           
                            
                            orderAnalysisRec.put("http_status",httpStatus);
                            orderAnalysisRec.put("status_message",statusMsg);
                            orderAnalysisRec.put("response_json",resJsonStr);
//                            orderAnalysisRec.put("out_of_server_skus",outOfServerSkus);
//                            orderAnalysisRec.put("payment_method",paymentMethod);
                            orderAnalysisRec.put("created_time",DateUtils.formatDateTime(createdTime));
                            
                            orderAnalysisRecLi.add(orderAnalysisRec);
                        }
                    }
                }
            
            
            /*
            for (int i = 0; i < orderIdLi.size(); i++) {
                Map<String,Object> orderAnalysisRec = new LinkedHashMap();
                
                String orderId = orderIdLi.get(i);
                String sql,addMsg = null,httpStatus = null,tempOrderId = null,theirOrderId = null,statusMsg = null,
                        reqJsonStr = null, resJsonStr = null,outOfServerSkus = null,paymentMethod = null,
                        paymentStatus = null, orderStatus = null, orderAppCodeAndAmt = null,queueStatus = null,
                        cartId = null;
                int tpEventId = 0;
                JsonArray paymentMethods;
                JsonNumber finalOrderAmt = null;
                Date createdTime=null;
                try {
                    sql = "SELECT * FROM mst_payment_history where order_id in ('"+orderId+"');";
                    List<Map<String,Object>> paymentRecLi = smartPrd.getResult(sql);
                    if(paymentRecLi.size()>0){
                        Map<String,Object> paymentRec = paymentRecLi.get(0);
                        paymentStatus = paymentRec.get("payment_status")+"";
                    }
                    
                    sql = "SELECT * FROM mst_cart WHERE payment_status='OKAY/SHORT' AND order_id = '"+orderId+"';";
                    List<Map<String,Object>> shortPayRecList = smartPrd.getResult(sql);
                    
                    sql = "SELECT * FROM mst_cart WHERE order_status='MISSING_ROUTE' AND order_id = '"+orderId+"';";
                    List<Map<String,Object>> misRouteRecList = smartPrd.getResult(sql);
                    
                    if(shortPayRecList.isEmpty() && misRouteRecList.isEmpty()){
                        sql = "select * from mst_cart where order_id = '"+orderId+"'";
                        List<Map<String,Object>> cartRecList = smartPrd.getResult(sql);
                        if(cartRecList.isEmpty()){
                            sql = "select * from mst_cart where id in (select cart_id from mst_order_id_history where order_id = '"+orderId+"');";
                            cartRecList = smartPrd.getResult(sql);
                            addMsg = "order_id found through mst_order_id_history";
                        }
                        if(cartRecList.size()>0){
                            Map<String,Object> cartRec = cartRecList.get(0);
                            cartId = cartRec.get("id")+"";
                            orderStatus = cartRec.get("order_status")+"";
                            if(cartRec.get("tp_event_id") != null){
                                tpEventId = (int)cartRec.get("tp_event_id");
                                sql = "select id,queue_status from tpd_action_queue where event_queue_id = "+tpEventId+";";
                                List<Map<String,Object>> actionRecList = smartTetrapod.getResult(sql);
                                if(actionRecList.size()>0){
                                    Map<String,Object> actionRec = actionRecList.get(0);
                                    queueStatus = (actionRec.get("queue_status")!=null)?(String)actionRec.get("queue_status"):"";
                                    int tpActionId = (int)actionRec.get("id");
                                    sql = "select * from tpd_action_log where action_queue_id = "+tpActionId+" order by created_time desc;";
                                    List<Map<String,Object>> actionLogRecList = smartTetrapodLog.getResult(sql);
                                    if(actionLogRecList.size()>0){
                                        Map<String,Object> actionLogRec = null;
                                        for (int j = 0; j < actionLogRecList.size(); j++) {
                                            actionLogRec = actionLogRecList.get(j);
                                            httpStatus = actionLogRec.get("http_status")+"";
                                            if(!actionLogRec.get("http_status").equals("0"))break;                                          
                                        }

                                        if(httpStatus.equals("200")){
                                            reqJsonStr = actionLogRec.get("request_msg")+"";
                                            resJsonStr = actionLogRec.get("response_msg")+"";
                                            resJsonStr = resJsonStr.replaceAll("[\\n\\t ]", "");
                                            
                                            writeFile(reqJsonStr, orderId+"-request");
                                            writeFile(resJsonStr, orderId+"-response");
                                            
                                            createdTime = (Date)actionLogRec.get("created_time");
                                            JsonObject reqJsonObj = JsonUtils.convertStringToJsonObject(reqJsonStr);
                                            JsonObject resJsonObj = null;
                                            
                                            //JsonUtils.printOrderReqJson(reqJsonObj);
                                            try {
                                                resJsonObj = JsonUtils.convertStringToJsonObject(resJsonStr);
                                            } catch (Exception e) {
                                            }
                                            
                                            paymentMethods = reqJsonObj.getJsonArray("payment_methods");
                                            
//                                            for (int j = 0; j < paymentMethods.size(); j++) {
//                                                JsonObject pm = (JsonObject)paymentMethods.get(j);
//                                                if(orderAppCodeAndAmt == null){
//                                                    orderAppCodeAndAmt = pm.getString("order_app_code")+":"+pm.getJsonNumber("payment_amt").doubleValue();
//                                                }else{
//                                                    orderAppCodeAndAmt = ";"+pm.getString("order_app_code")+":"+pm.getJsonNumber("payment_amt").doubleValue();
//                                                }
//                                            }
                                            
                                            if(reqResJsonVer.equalsIgnoreCase("v1")){
                                                finalOrderAmt = reqJsonObj.getJsonNumber("final_order_amount");
                                                paymentMethod = reqJsonObj.getString("payment_method");
                                                JsonArray products = reqJsonObj.getJsonArray("products");
                                                StringBuilder skus = new StringBuilder();
                                                boolean first = true;

                                                for (int j = 0; j < products.size(); j++) {
                                                    JsonObject product = products.getJsonObject(j);
                                                    if(product.getInt("processed_quantity")==0){
                                                        if(first){
                                                            try {skus.append(product.getJsonNumber("sku"));} catch (Exception e) {}
                                                            first = false;
                                                        }else{
                                                            try {skus.append(", ").append(product.getJsonNumber("sku"));} catch (Exception e) {}

                                                        }
                                                    }
                                                }
                                                outOfServerSkus = skus.toString();
                                            }
                                            
                                            if(reqResJsonVer.equalsIgnoreCase("v2")){
                                                try {
                                                    finalOrderAmt = reqJsonObj.getJsonObject("order_prices").getJsonNumber("final_amount");
                                                    JsonArray products = reqJsonObj.getJsonArray("order_products_sku");
                                                } catch (Exception e) {
                                                }
                                            }
                                            
                                            if(resJsonObj!=null){
                                                if(reqResJsonVer.equalsIgnoreCase("v1")){
                                                    tempOrderId = resJsonObj.getString("temp_order_id");
                                                    theirOrderId = JsonUtils.convertJsonValueToObject(resJsonObj, "order_id", Arrays.asList(JsonValue.ValueType.STRING,JsonValue.ValueType.NULL))+"";
                                                    statusMsg = resJsonObj.getString("status_Message").replaceAll("\r", "").replaceAll("\n", "");
                                                }else if(reqResJsonVer.equalsIgnoreCase("v2")){
                                                    tempOrderId = resJsonObj.getString("jiomart_order_id");
                                                    theirOrderId = JsonUtils.convertJsonValueToObject(resJsonObj, "fynd_order_id", Arrays.asList(JsonValue.ValueType.STRING,JsonValue.ValueType.NULL))+"";
                                                    statusMsg = resJsonObj.getString("message").replaceAll("\r", "").replaceAll("\n", "");
                                                }
                                            }
                                        }else{
                                            reqJsonStr = actionLogRec.get("request_msg")+"";
                                            writeFile(reqJsonStr, orderId+"-request");
                                            resJsonStr = actionLogRec.get("response_msg")+"";
                                        }
                                    }else{
                                        addMsg = "invalid/no records for the 'action_queue_id' from table 'tpd_action_log'";
                                    }
                                }else{
                                    addMsg = "invalid/no records for the 'event_queue_id' from table 'tpd_action_queue'";
                                }
                            }else{
                                tempOrderId = orderId;
                                addMsg = "order not reached to tetrapod system;order_status: "+orderStatus;
                            }
                        }else{
                            addMsg = "invalid/no records for the 'order_id' from table 'mst_cart' and 'order_id_history'";
                        }
                    }else{
                        if(!shortPayRecList.isEmpty()){
                            cartId = shortPayRecList.get(0).get("id")+"";
                            addMsg = "payment_status='OKAY/SHORT';short payment";
                        }
                        if(!misRouteRecList.isEmpty()){ 
                            cartId = misRouteRecList.get(0).get("id")+"";
                            addMsg = "order_status='MISSING_ROUTE'";
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Throwable t = ExceptionUtils.getRootCause(e);
                    //ExceptionUtils.logExceptionStackTrace(t);
                    addMsg = ExceptionUtils.getExceptionMessage(t,"exception: unable to get log!");
                }
                orderAnalysisRec.put("cart_id",cartId);
                orderAnalysisRec.put("order_id",orderId);
                orderAnalysisRec.put("tp_event_id",orderId);
                orderAnalysisRec.put("tp_event_id",tpEventId);
                orderAnalysisRec.put("queue_status",queueStatus);
                orderAnalysisRec.put("additional_message",addMsg);
                orderAnalysisRec.put("payment_status",paymentStatus);
                orderAnalysisRec.put("final_order_amount",finalOrderAmt);
                orderAnalysisRec.put("http_status",httpStatus);
                orderAnalysisRec.put("temp_order_id",tempOrderId);
                orderAnalysisRec.put("their_order_id",theirOrderId);
                orderAnalysisRec.put("status_message",statusMsg);
                orderAnalysisRec.put("response_json",resJsonStr);
                orderAnalysisRec.put("out_of_server_skus",outOfServerSkus);
                orderAnalysisRec.put("payment_method",paymentMethod);
                orderAnalysisRec.put("created_time",DateUtils.formatDateTime(createdTime));
                orderAnalysisRec.put("rrn",orderAppCodeAndAmt);
                
                
                System.out.println(cartId+"|"+orderId+"|"+tpEventId+"|"+queueStatus+"|"+addMsg+"|"+paymentStatus+"|"+finalOrderAmt+"|"+httpStatus+"|"+tempOrderId+"|"+theirOrderId+"|"+statusMsg+"|"+
                        resJsonStr+"|"+outOfServerSkus+"|"+paymentMethod+"|"+((createdTime!=null)?DateUtils.formatDateTime(createdTime):null)+"|"+orderAppCodeAndAmt);
                
                
                orderAnalysisRecLi.add(orderAnalysisRec);
                if(i % 500 == 0){
                    if(smartPrd!=null)smartPrd.close();
                    if(smartTetrapod!=null)smartTetrapod.close();
                    if(smartTetrapodLog!=null)smartTetrapodLog.close();
                    if(env.equalsIgnoreCase("stg")){
                        smartPrd = SQLFront.jiomartMStarStg();
                        smartTetrapod = SQLFront.jiomartTetrapodStg();
                        smartTetrapodLog = SQLFront.jiomartTetrapodLogStg();
                    }else if(env.equalsIgnoreCase("prd")){
                        smartPrd = SQLFront.newSmartPrd();
                        smartTetrapod = SQLFront.newSmartTetrapod();
                        smartTetrapodLog = SQLFront.newSmartTetrapodLog();
                    }else if(env.equalsIgnoreCase("pre-prd")){
                        smartPrd = SQLFront.newSmartPrePrd();
                        smartTetrapod = SQLFront.newSmartPrePrdTetrapod();
                        smartTetrapodLog = SQLFront.newSmartPrePrdTetrapodLog();
                    }else{
                        throw new RuntimeException("invalid environment passed: "+env);
                    }
                }
            }
            */
                    
            /*
            writeCsvFile(orderAnalysisRecLi,Arrays.asList("order_id",
                                                        "tp_event_id",
                                                        "queue_status",
                                                        "additional_message",
                                                        "payment_status",
                                                        "final_order_amount",
                                                        "http_status",
                                                        "retry_order_id",
                                                        "fynd_order_id",
                                                        "status_message",
                                                        "response_json",
                                                        "out_of_server_skus",
                                                        "payment_method",
                                                        "created_time",
                                                        "order_app_code_and_amount"),"order-analysis");
            */
            
        } catch (Exception e) {
            e.printStackTrace();
            Throwable t = ExceptionUtils.getRootCause(e);
            ExceptionUtils.logExceptionStackTrace(t);
            String msg = ExceptionUtils.getExceptionMessage(t,"get smart order analysis log failed!");
            throw new RuntimeException(msg);
        }finally{
            if(mstarSF!=null)mstarSF.close();
            if(tetrapodLogSF!=null)tetrapodLogSF.close();
        }
        
        return orderAnalysisRecLi;
    }
    public static void writeFile(String sourceStr,String fileName){
        try {
            BufferedWriter bw = new BufferedWriter(new FileWriter(new File(String.format("%1$s%2$sDesktop%2$sTemp%2$sSamples%2$s%3$s.json", System.getProperty("user.home"),File.separator,fileName))));
            bw.write(sourceStr);
            bw.close();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }    
    }
    public static void main(String[] args) {
        List<String> orderIdLi = getOrderIdListFromFile("C:\\Users\\muthu\\Downloads\\mobile_no.xlsx",0,1,0);
        //getTpEventIdAndStatus(orderIdLi,"JioMart Prod");
        getMOPFromDatabase(orderIdLi,"JioMart Prod");
    }
    
}
