/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.netmeds.support.ops.mstar.service;

import com.netmeds.support.ops.Environments;
import com.netmeds.support.ops.utils.DateUtils;
import com.netmeds.support.ops.utils.ExceptionUtils;
import com.netmeds.support.ops.utils.JsonUtils;
import com.netmeds.support.ops.utils.OrderIdNotFoundException;
import com.netmeds.support.ops.utils.SQLFront;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author Admin
 */
public class MStarService {
    
    private static final Logger LOGGER = Logger.getLogger(MStarService.class.getName());

    public static SQLFront getTetrapodSQLFront(String envName){
        SQLFront sf = null;
        try {
            boolean isValidEnv = false;
            for (Environments envs : Environments.values()) {
                if(envs.getDisplayName().equals(envName)){
                    isValidEnv = true;
                    sf=envs.getTpodSF();
                }
            }
            if(!isValidEnv)throw new RuntimeException(String.format("invalid environment '%1$s' passed!",envName));
        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, null, e);
            Throwable t = ExceptionUtils.getRootCause(e);
            ExceptionUtils.logExceptionStackTrace(t);
            String msg = ExceptionUtils.getExceptionMessage(t,"get tetrapod datasource failed!");
            throw new RuntimeException(msg);
        }
        return sf;
    }
    public static SQLFront getTetrapodLogSQLFront(String envName){
        SQLFront sf = null;
        try {
            boolean isValidEnv = false;
            for (Environments envs : Environments.values()) {
                if(envs.getDisplayName().equals(envName)){
                    isValidEnv = true;
                    sf=envs.getTpodLogSF();
                }
            }
            if(!isValidEnv)throw new RuntimeException(String.format("invalid environment '%1$s' passed!",envName));
        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, null, e);
            Throwable t = ExceptionUtils.getRootCause(e);
            ExceptionUtils.logExceptionStackTrace(t);
            String msg = ExceptionUtils.getExceptionMessage(t,"get tetrapod log datasource failed!");
            throw new RuntimeException(msg);
        }
        return sf;
    }
    public static SQLFront getMStarSQLFront(String envName){
        SQLFront sf = null;
        try {
            boolean isValidEnv = false;
            for (Environments envs : Environments.values()) {
                if(envs.getDisplayName().equals(envName)){
                    isValidEnv = true;
                    sf=envs.getMstarSF();
                }
            }
            if(!isValidEnv)throw new RuntimeException(String.format("invalid environment '%1$s' passed!",envName));
        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, null, e);
            Throwable t = ExceptionUtils.getRootCause(e);
            ExceptionUtils.logExceptionStackTrace(t);
            String msg = ExceptionUtils.getExceptionMessage(t,"get mstar datasource failed!");
            throw new RuntimeException(msg);
        }
        return sf;
    }

    public static List<Map<String,Object>> getResponseByOrderId(String envName, String order_id){
        List<Map<String,Object>> cartItems = null;
        List<Map<String,Object>> result = null;
        SQLFront sf = null;
        try {
            sf = getMStarSQLFront(envName);
            String whereCond = String.format("order_id = '%1$s'", order_id);
            String sql = String.format("select order_status,payment_status,tp_event_id from mst_cart where %1$s order by created_time",whereCond);
            result = sf.getResult(sql);
            if(result.size()==0){
                throw new OrderIdNotFoundException("No data found for order_id: "+order_id+" in "+envName+" environment.");
            }
            if(result.get(0).get("tp_event_id")==null){
                return result;
            }else{
                String result_tp_id = result.get(0).get("tp_event_id").toString();
                try{
                    sf = getTetrapodLogSQLFront(envName);
                    whereCond = String.format("event_queue_id = '%1$s'", result_tp_id);
                    sql = String.format("select response_msg,request_msg from tpd_action_log where %1$s order by created_time",whereCond);
                    cartItems = sf.getResult(sql);
                    if(cartItems.size()==0){
                        return result;
                    }
                    return cartItems;
                }
                catch(Exception e){
                    return result;
                }
            }

        } catch(OrderIdNotFoundException oe){
            LOGGER.log(Level.SEVERE, null, oe);
            Throwable t = ExceptionUtils.getRootCause(oe);
            ExceptionUtils.logExceptionStackTrace(t);
            String msg = ExceptionUtils.getExceptionMessage(t,"get response by order id failed");
            throw new OrderIdNotFoundException(msg);
        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, null, e);
            Throwable t = ExceptionUtils.getRootCause(e);
            ExceptionUtils.logExceptionStackTrace(t);
            String msg = ExceptionUtils.getExceptionMessage(t,"get response by order id failed");
            throw new RuntimeException(msg);
        }finally{
            if(sf!=null)sf.close();
        }
            
    }
    
    public static List<Map<String,Object>> getCustomerList(String envName,String mobileNo,String email,String custId){
        List<Map<String,Object>> custList = null;
        SQLFront sf = null;
        try {
            sf = getMStarSQLFront(envName); 
            String whereCond = null;
            if(custId != null && !custId.trim().isEmpty() && Integer.parseInt(custId) > 0){
                whereCond = String.format("id = %1$s", custId);
            }else if(mobileNo != null && !mobileNo.trim().isEmpty()){
                whereCond = String.format("mobile_no = '%1$s'", mobileNo);
            }else if(email != null && !email.trim().isEmpty()){
                whereCond = String.format("email = '%1$s'", email);
            }else{
                throw new RuntimeException("invalid input");
            }
            String sql = String.format("select * from mst_customer where %1$s",whereCond);
            custList = sf.getResult(sql);
        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, null, e);
            Throwable t = ExceptionUtils.getRootCause(e);
            ExceptionUtils.logExceptionStackTrace(t);
            String msg = ExceptionUtils.getExceptionMessage(t,"get customer list failed");
            throw new RuntimeException(msg);
        }finally{
            if(sf!=null)sf.close();
        }
        return custList;
    }
    
    public static List<Map<String,Object>> getCartList(String envName, String orderId, String cartId, String custId, boolean searchByOrderIdHistory){
        List<Map<String,Object>> cartList = null;
        SQLFront sf = null;
        try {
            sf = getMStarSQLFront(envName); 
            String whereCond = null;
            if(cartId != null  && !cartId.trim().isEmpty() && Integer.parseInt(cartId) > 0){
                whereCond = String.format("id = %1$s", cartId);
            }else if(custId != null  && !custId.trim().isEmpty() && Integer.parseInt(custId) > 0){
                whereCond = String.format("customer_id = %1$s", custId);
            }else if(orderId != null && !orderId.trim().isEmpty()){
                whereCond = String.format("order_id = '%1$s'", orderId);
            }else{
                throw new RuntimeException("invalid input");
            }
            if(searchByOrderIdHistory && (orderId == null || orderId.trim().isEmpty()))throw new RuntimeException("order_id is null!. order_id required for search by order id history");
            String sql;
            if(searchByOrderIdHistory){
                sql = String.format("select * from mst_cart where id in (select cart_id from mst_order_id_history where order_id = '%1$s');",orderId);
            }else{
                sql = String.format("select * from mst_cart where %1$s",whereCond);
            } 
            cartList = sf.getResult(sql);
        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, null, e);
            Throwable t = ExceptionUtils.getRootCause(e);
            ExceptionUtils.logExceptionStackTrace(t);
            String msg = ExceptionUtils.getExceptionMessage(t,"get cart list failed");
            throw new RuntimeException(msg);
        }finally{
            if(sf!=null)sf.close();
        }
        return cartList;
    }
    
    public static List<Map<String,Object>> getCartItems(String envName,int cartId){
        List<Map<String,Object>> cartItems = null;
        SQLFront sf = null;
        try {
            sf = getMStarSQLFront(envName); 
            String whereCond = String.format("cart_id = %1$s", cartId);
            String sql = String.format("select * from mst_cart_item where %1$s order by created_time",whereCond);
            cartItems = sf.getResult(sql);
        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, null, e);
            Throwable t = ExceptionUtils.getRootCause(e);
            ExceptionUtils.logExceptionStackTrace(t);
            String msg = ExceptionUtils.getExceptionMessage(t,"get cart items failed");
            throw new RuntimeException(msg);
        }finally{
            if(sf!=null)sf.close();
        }
        return cartItems;
    }
    
    public static List<Map<String,Object>> getShippingAndBillingAddress(String envName,int cartId){
        List<Map<String,Object>> addressList = null;
        SQLFront sf = null;
        try {
            addressList = new ArrayList();
            List<Map<String,Object>> recList;
            Map<String,Object> rec;
            sf = getMStarSQLFront(envName); 
            String sql = String.format("SELECT * FROM mst_cart WHERE id = %1$s", cartId);
            recList = sf.getResult(sql);
            if(recList.size()>0){
                Map<String,Object> cartRec = recList.get(0);
                int bllingAddId = (cartRec.get("billing_address_id")!=null)?(int)cartRec.get("billing_address_id"):0;
                int shippingAddId = (cartRec.get("shipping_address_id")!=null)?(int)cartRec.get("shipping_address_id"):0;
                
                sql = String.format("SELECT * FROM mst_address_v2 WHERE id = %1$s", bllingAddId);
                recList = sf.getResult(sql);
                if(recList.size()>0){
                    rec = recList.get(0);
                    rec.put("address_type","billing_address");
                    addressList.add(rec);
                }
                
                sql = String.format("SELECT * FROM mst_address_v2 WHERE id = %1$s", shippingAddId);
                recList = sf.getResult(sql);
                if(recList.size()>0){
                    rec = recList.get(0);
                    rec.put("address_type","shipping_address");
                    addressList.add(rec);
                }
            }else{
                throw new RuntimeException(String.format("no records available for the id '%1$s' from table 'mst_cart'.",cartId));
            }
        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, null, e);
            Throwable t = ExceptionUtils.getRootCause(e);
            ExceptionUtils.logExceptionStackTrace(t);
            String msg = ExceptionUtils.getExceptionMessage(t,"get shipping/billing address failed");
            throw new RuntimeException(msg);
        }finally{
            if(sf!=null)sf.close();
        }
        return addressList;
    }
    
    
    
    public static List<Map<String,Object>> getCartAddressBackup(String envName,int cartId){
        List<Map<String,Object>> addressList = null;
        SQLFront sf = null;
        try {
            sf = getMStarSQLFront(envName); 
            String sql = String.format("SELECT * FROM mst_cart_address WHERE cart_id = %1$s", cartId);
            addressList = sf.getResult(sql);
        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, null, e);
            Throwable t = ExceptionUtils.getRootCause(e);
            ExceptionUtils.logExceptionStackTrace(t);
            String msg = ExceptionUtils.getExceptionMessage(t,"get cart address failed");
            throw new RuntimeException(msg);
        }finally{
            if(sf!=null)sf.close();
        }
        return addressList;
    }
    
    public static List<Map<String,Object>> getCartPaymentInfo(String envName,int cartId){
        List<Map<String,Object>> addressList = null;
        SQLFront sf = null;
        try {
            sf = getMStarSQLFront(envName); 
            String sql = String.format("SELECT * FROM mst_cart_payment_info WHERE cart_id = %1$s", cartId);
            addressList = sf.getResult(sql);
        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, null, e);
            Throwable t = ExceptionUtils.getRootCause(e);
            ExceptionUtils.logExceptionStackTrace(t);
            String msg = ExceptionUtils.getExceptionMessage(t,"get cart payment info failed");
            throw new RuntimeException(msg);
        }finally{
            if(sf!=null)sf.close();
        }
        return addressList;
    }
    
    public static List<Map<String,Object>> getOrderIdHistory(String envName,String cartId,String orderId){
        List<Map<String,Object>> orderIdHistory = null;
        SQLFront sf = null;
        try {
            sf = getMStarSQLFront(envName); 
            String whereCond = null;
            if(cartId!=null || orderId!=null){
                if(cartId!=null)whereCond = String.format("cart_id = %1$s", cartId);
                if(orderId!=null)whereCond = String.format("order_id = '%1$s'", orderId);
            }else{
                throw new RuntimeException("invalid cart id or order id!");
            }
            String sql = String.format("select * from mst_order_id_history where %1$s order by created_time",whereCond);
            orderIdHistory = sf.getResult(sql);
        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, null, e);
            Throwable t = ExceptionUtils.getRootCause(e);
            ExceptionUtils.logExceptionStackTrace(t);
            String msg = ExceptionUtils.getExceptionMessage(t,"get order id history failed");
            throw new RuntimeException(msg);
        }finally{
            if(sf!=null)sf.close();
        }
        return orderIdHistory;
    }
    
    public static List<Map<String,Object>> getPaymentHistory(String envName,int cartId){
        List<Map<String,Object>> paymentHistory = null;
        SQLFront sf = null;
        try {
            sf = getMStarSQLFront(envName); 
            String whereCond = String.format("cart_id = %1$s", cartId);
            String sql = String.format("select * from mst_payment_history where %1$s order by created_time",whereCond);
            paymentHistory = sf.getResult(sql);
        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, null, e);
            Throwable t = ExceptionUtils.getRootCause(e);
            ExceptionUtils.logExceptionStackTrace(t);
            String msg = ExceptionUtils.getExceptionMessage(t,"get payment history failed");
            throw new RuntimeException(msg);
        }finally{
            if(sf!=null)sf.close();
        }
        return paymentHistory;
    }
    
    public static List<Map<String,Object>> getJuspayWebhookList(String envName,int cartId){
        List<Map<String,Object>> webhookList = new ArrayList();
        SQLFront sf = null;
        try {
            sf = getMStarSQLFront(envName);
            String sql = "SELECT order_id FROM mst_order_id_history WHERE cart_id="+cartId+";";
            List<Map<String,Object>> orderIdRecLi = sf.getResult(sql);
            StringBuilder orderIdSB = new StringBuilder();
            boolean first = true;
            for (int i = 0; i < orderIdRecLi.size(); i++) {
                Map<String, Object> orderIdRec = orderIdRecLi.get(i);
                if(first){
                    orderIdSB.append(String.format("%1$s%2$s%1$s","'",orderIdRec.get("order_id")));
                    first = false;
                }else{
                    orderIdSB.append(String.format("%1$s%2$s%3$s%2$s",",","'",orderIdRec.get("order_id")));
                }
            }
            if(!orderIdSB.toString().isEmpty()){
                sql = "SELECT * FROM mst_juspay_webhook_log where order_id in ("+orderIdSB+") order by created_time;";
                webhookList = sf.getResult(sql);
            }
        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, null, e);
            Throwable t = ExceptionUtils.getRootCause(e);
            ExceptionUtils.logExceptionStackTrace(t);
            String msg = ExceptionUtils.getExceptionMessage(t,"get juspay webhook list failed");
            throw new RuntimeException(msg);
        }finally{
            if(sf!=null)sf.close();
        }
        return webhookList;
    }
    
    public static List<Map<String,Object>> getMyJioWebhookList(String envName,int cartId){
        List<Map<String,Object>> webhookList = new ArrayList();
        SQLFront sf = null;
        try {
            sf = getMStarSQLFront(envName);
            String sql = "SELECT order_id FROM mst_order_id_history WHERE cart_id="+cartId+";";
            List<Map<String,Object>> orderIdRecLi = sf.getResult(sql);
            StringBuilder orderIdSB = new StringBuilder();
            boolean first = true;
            for (int i = 0; i < orderIdRecLi.size(); i++) {
                Map<String, Object> orderIdRec = orderIdRecLi.get(i);
                if(first){
                    orderIdSB.append(String.format("%1$s%2$s%1$s","'",orderIdRec.get("order_id")));
                    first = false;
                }else{
                    orderIdSB.append(String.format("%1$s%2$s%3$s%2$s",",","'",orderIdRec.get("order_id")));
                }
            }
            if(!orderIdSB.toString().isEmpty()){
                sql = "SELECT * FROM mst_myjio_webhook_log where order_id in ("+orderIdSB+") order by created_time;";
                webhookList = sf.getResult(sql);
            }
        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, null, e);
            Throwable t = ExceptionUtils.getRootCause(e);
            ExceptionUtils.logExceptionStackTrace(t);
            String msg = ExceptionUtils.getExceptionMessage(t,"get myjio webhook list failed");
            throw new RuntimeException(msg);
        }finally{
            if(sf!=null)sf.close();
        }
        return webhookList;
    }
    
    public static List<Map<String,Object>> getSessions(String envName,int custId){
        List<Map<String,Object>> sessions = null;
        SQLFront sf = null;
        try {
            sf = getMStarSQLFront(envName); 
            String whereCond = String.format("customer_id = %1$s", custId);
            String sql = String.format("select * from mst_session where %1$s",whereCond);
            sessions = sf.getResult(sql);
        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, null, e);
            Throwable t = ExceptionUtils.getRootCause(e);
            ExceptionUtils.logExceptionStackTrace(t);
            String msg = ExceptionUtils.getExceptionMessage(t,"get sessions failed");
            throw new RuntimeException(msg);
        }finally{
            if(sf!=null)sf.close();
        }
        return sessions;
    }
    
    public static List<Map<String,Object>> getAddresses(String envName,int custId){
        List<Map<String,Object>> addresses = null;
        SQLFront sf = null;
        try {
            sf = getMStarSQLFront(envName); 
            String whereCond = String.format("customer_id = %1$s", custId);
            String sql = String.format("select * from mst_address_v2 where %1$s",whereCond);
            addresses = sf.getResult(sql);
        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, null, e);
            Throwable t = ExceptionUtils.getRootCause(e);
            ExceptionUtils.logExceptionStackTrace(t);
            String msg = ExceptionUtils.getExceptionMessage(t,"get addresses failed");
            throw new RuntimeException(msg);
        }finally{
            if(sf!=null)sf.close();
        }
        return addresses;
    }
    
    public static void customerBlock(String envName,int custId,String reason){
        SQLFront mStartSF = null;
        SQLFront sptOps = null;
        try {
            sptOps = SQLFront.newSptOps();
            mStartSF = getMStarSQLFront(envName); 
            if(reason == null || reason.trim().isEmpty())throw new RuntimeException("invalid customer block reason");
            String whereCond = String.format("customer_id = %1$s", custId);
            Map<String,Object> updateRec = new HashMap();
            updateRec.put("block_status", "Y");
            int blockStatus = mStartSF.update("mst_customer", updateRec, whereCond);
            if(blockStatus>0){
                updateRec = new HashMap();
                updateRec.put("is_valid", 0);
                int sessionInvalidSt = mStartSF.update("mst_session", updateRec, whereCond);
                Map<String,Object> insertRec = new HashMap();
                insertRec.put("customer_id",custId);
                insertRec.put("reason",reason);
                sptOps.insert("mstar_customer_block_reason", insertRec);
            }else{
                throw new RuntimeException("customer block failed!");
            }
        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, null, e);
            Throwable t = ExceptionUtils.getRootCause(e);
            ExceptionUtils.logExceptionStackTrace(t);
            String msg = ExceptionUtils.getExceptionMessage(t,"get addresses failed");
            throw new RuntimeException(msg);
        }finally{
            if(mStartSF!=null)mStartSF.close();
            if(sptOps!=null)sptOps.close();
        }
    }

    public static Map<String, Object> validateSmartReconRequest(String clientIp, LocalDateTime startTime, LocalDateTime endTime) {
        Map<String, Object> validationRec = new HashMap();
        SQLFront sptOps = null;
        try {
            sptOps = SQLFront.newSptOps();
            List<Map<String,Object>> configRecLi = sptOps.getResult("select * from spt_smart_recon_config;");
            long maxDuration = 0,maxTimeToGoBack = 0, freqCheck = 0, freqAllowed = 0;
            for (int i = 0; i < configRecLi.size(); i++) {
                Map<String, Object> rec = configRecLi.get(i);
                String key = rec.get("config_key")+"";
                long value = (int)rec.get("config_value");
                switch(key){
                    case "max_duration_in_hours":maxDuration = value;break;
                    case "max_time_to_go_back_in_days":maxTimeToGoBack = value;break;
                    case "frequency_check_duration_in_minutes":freqCheck = value;break;
                    case "frequency_allowed_count":freqAllowed = value;break;
                    default:break;
                }
            }
            LOGGER.log(Level.INFO, String.format("max_duration_in_hours: %1$s, max_time_to_go_back_in_days: %2$s,frequency_check_duration_in_minutes: %3$s,"
                    + "frequency_allowed_countfrequency_allowed_count: %4$s", maxDuration,maxTimeToGoBack,freqCheck,freqAllowed));
            LocalDateTime nowLC = LocalDateTime.now();
            long hoursDiff = startTime.until(endTime,ChronoUnit.HOURS);
            LOGGER.log(Level.INFO, String.format("hoursDiff: %1$s",hoursDiff));
            if(hoursDiff <= maxDuration){
                long daysDiff = startTime.until(nowLC,ChronoUnit.DAYS);
                LOGGER.log(Level.INFO, String.format("daysDiff: %1$s, daysDiff < maxTimeToGoBack: %2$s",daysDiff,(daysDiff < maxTimeToGoBack)));
                if(daysDiff < maxTimeToGoBack){
                    LocalDateTime freqCheckTime = nowLC.minusMinutes(freqCheck);
                    List<Map<String,Object>> reportFreqRecLi = sptOps.getResult(String.format("select * from spt_smart_recon_report_request "
                            + "where request_time >= '%1$s';",DateUtils.formatLocalDateTime(freqCheckTime)));
                    if(reportFreqRecLi.size()<= freqAllowed){
                        Map<String,Object> insertRec = new HashMap();
                        insertRec.put("client_ip", clientIp);
                        insertRec.put("request_parameters", "start_time: "+DateUtils.formatLocalDateTime(startTime)+", end_time: "+DateUtils.formatLocalDateTime(endTime));
                        sptOps.insert("spt_smart_recon_report_request", insertRec);
                        
                        validationRec.put("status",true);
                    }else{
                        validationRec.put("status",false);
                        validationRec.put("message",String.format("frequency_allowed_count limit of %1$s crossed in frequency_check_duration_in_minutes limit of %2$s",freqAllowed,freqCheck));
                    }
                }else{
                    validationRec.put("status",false);
                    validationRec.put("message",String.format("max_time_to_go_back_in_days limit of %1$s crossed",maxTimeToGoBack));
                }
            }else{
                validationRec.put("status",false);
                validationRec.put("message",String.format("max_duration_in_hours limit of %1$s between start_time and end_time crossed",maxDuration));
            }
        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, null, e);
            Throwable t = ExceptionUtils.getRootCause(e);
            ExceptionUtils.logExceptionStackTrace(t);
            String msg = ExceptionUtils.getExceptionMessage(t,"validate smart reconciliation request failed!");
            throw new RuntimeException(msg);
        }finally{
            if(sptOps!=null)sptOps.close();
        }
        return validationRec;
    }
}
