/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.netmeds.support.ops.utils;

import static com.netmeds.support.ops.utils.DateUtils.formatDateTime;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.sql.ResultSet;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Properties;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import static java.util.stream.Collectors.toList;
import java.util.stream.IntStream;
import java.util.stream.Stream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.mail.MessagingException;
import javax.xml.stream.XMLStreamWriter;

/**
 *
 * @author prem
 */
public class CommonUtils {
    private static final Logger LOGGER=Logger.getLogger(CommonUtils.class.getName());

    private CommonUtils(){}

    public static final BigDecimal BD100=new BigDecimal(100);

    public static final String SYSPROP_TESTING      = "netmeds.dailyreports.testing";
    public static final String SYSPROP_TESTING_MAIL = "netmeds.dailyreports.testing.mail";

    public static final String EMAILADDR_ADVAIT            ="advait@netmeds.com";
    public static final String EMAILADDR_ANAND             ="anand@netmeds.com";
    public static final String EMAILADDR_ANTHONY           ="anthonygalliano@covenantim.com";
    public static final String EMAILADDR_ANU               ="anuradha.binny@netmeds.com";
    public static final String EMAILADDR_ARUN              ="SadhanandhamA@OrbiMed.com";
    public static final String EMAILADDR_ARUNKUMAR         ="arunkumaar.m@netmeds.com";
  //public static final String EMAILADDR_ATIT              ="atit.jain@netmeds.com";
    public static final String EMAILADDR_BABU              ="babu@netmeds.com";
    public static final String EMAILADDR_BRUCE             ="bruce@netmeds.com";
    public static final String EMAILADDR_GAVASKAR          ="gavaskar.alagesan@netmeds.com";
    public static final String EMAILADDR_GOMATHI           ="gomathi.thiyagu@netmeds.com";
    public static final String EMAILADDR_JAYAKRISHNA       ="jayakrishna.s@netmeds.com";
    public static final String EMAILADDR_JOHN              ="john@netmeds.com";
    public static final String EMAILADDR_KAARTHICK         ="kaarthick.r@netmeds.com";
    public static final String EMAILADDR_KC                ="karamchand@netmeds.com";
    public static final String EMAILADDR_KIRILL            ="kirill@sistemaasiafund.com";
    public static final String EMAILADDR_KUMARI            ="kumari.manoharan@netmeds.com";
    public static final String EMAILADDR_LIAM              ="liam@cambodianinvestmentmanagement.com";
    public static final String EMAILADDR_LOWRIL            ="lowril@netmeds.com";
    public static final String EMAILADDR_MANI              ="manikandan.thangaraj@netmeds.com";
    public static final String EMAILADDR_MARKETING         ="marketing@netmeds.com";
    public static final String EMAILADDR_MATHANGI          ="mathangi@netmeds.com";
  //public static final String EMAILADDR_MUKUL             ="mukul.bansal@netmeds.com";
    public static final String EMAILADDR_MURUGESA          ="murugesa@mapegroup.com";
    public static final String EMAILADDR_NAGS              ="nagarajan@netmeds.com";
  //public static final String EMAILADDR_NIRMAL            ="nirmald@digitactical.com";
    public static final String EMAILADDR_PARAMESH          ="paramesh.manickavasagam@netmeds.com";
    public static final String EMAILADDR_PRADEEP           ="pradeep@netmeds.com";
    public static final String EMAILADDR_PREM              ="prem.kumar@netmeds.com";
    public static final String EMAILADDR_PRIYA             ="priya@netmeds.com";
  //public static final String EMAILADDR_PUNEET            ="puneet.kaura@netmeds.com";
    public static final String EMAILADDR_RAM               ="ram@mapegroup.com";
    public static final String EMAILADDR_RAVIANGAMUTHU     ="ravi.angamuthu@netmeds.com";
    public static final String EMAILADDR_RK                ="rk@netmeds.com";
    public static final String EMAILADDR_SALAKHIDDIN       ="bsalakhiddin@gmail.com";
    public static final String EMAILADDR_SALONI            ="saloni.jain@mapegroup.com";
    public static final String EMAILADDR_SANKET            ="sanket.golechha@netmeds.com";
    public static final String EMAILADDR_SARAVANAN         ="saravanan.arimuthu@netmeds.com";
    public static final String EMAILADDR_SUNNY             ="sharmas@orbimed.com";
    public static final String EMAILADDR_SWAMI             ="swaminathan@mapegroup.com";
  //public static final String EMAILADDR_TARUN             ="tarun@netmeds.com";
    public static final String EMAILADDR_THIYAGARAJAN      ="thiyagarajan.m@netmeds.com";
    public static final String EMAILADDR_UNNI              ="unni.menon@netmeds.com";
    public static final String EMAILADDR_VASAN             ="vasan@netmeds.com";
    public static final String EMAILADDR_VIJAY             ="vijay@netmeds.com";
    public static final String EMAILADDR_VIMALA            ="vimala.b@netmeds.com";
  //public static final String EMAILADDR_MADHULIKA         ="madhulika@netmeds.com";

    public static enum FC {
        FC601("601", "Chn-SM",  "SM",  "Chennai",    "TN"),
        FC602("602", "Chn-FM",  "FM",  "Chennai",    "TN"),
        FC605("605", "Hyd-PPW", "PPW", "Hyderabad",  "TG"),
        FC606("606", "Ccn-PPW", "PPW", "Cochin",     "KL"),
        FC607("607", "Blr-PPW", "PPW", "Bangalore",  "KA"),
        FC608("608", "Del-PPW", "PPW", "Delhi",      "DL"),
        FC609("609", "Del-FC2", "FC2", "Delhi",      "DL"),
        FC610("610", "Chn-CM",  "CM",  "Chennai",    "TN"),
        FC611("611", "Mum-EUP", "EUP", "Mumbai",     "MH"),
        FC612("612", "Ahm-PPW", "PPW", "Ahmedabad",  "GJ"),
        FC613("613", "Chn-HLM", "HLM", "Chennai",    "TN"),
        FC614("614", "Kol-PPW", "PPW", "Kolkatta",   "WB"),
        FC615("615", "Mum-PPW", "PPW", "Mumbai",     "MH"),
        ;

        private FC(String code, String displayName, String companyInitials, String city, String state) {
            this.id = code;
            this.displayName = displayName;
            this.companyInitials=companyInitials;
            this.city=city;
            this.state=state;
        }

        private final String id;
        private final String displayName;
        private final String companyInitials;
        private final String city;
        private final String state;

        public String getId() { return id; }
        public String getDisplayName() { return displayName; }
        public String getCompanyInitials() { return companyInitials; }
        public String getCity() { return city; }
        public String getState() { return state; }

        public static Optional<FC> of(String str) {
            return Arrays.stream(values())
                    .filter((en)->en.getId().equalsIgnoreCase(str))
                    .findFirst();
        }
    }
    
    public static Throwable getRootCause(Throwable exception){
        Throwable rootException = exception;
        while(rootException.getCause() != null){
            rootException = exception.getCause();
        }
        return rootException;
    }

    public static void writeOutReport(String reportData) {
        writeOutReport(reportData, getOSSpecificBasePath()+"report.html");
    }

    public static void writeOutReport(String reportData, String filepath) {
        FileOutputStream fos;

        try {
            fos=new FileOutputStream(filepath);
            fos.write(reportData.getBytes());
            fos.close();
        }
        catch(IOException ioe) {
            ioe.printStackTrace();
        }
    }

    public static void writeOutZippedReport(String reportData, String filepathWOext) {
        FileOutputStream fos=null;
        String zipFileName=filepathWOext+".zip";
        ZipOutputStream zos=null;
        String inputBaseName=new File(filepathWOext).getName()+".csv";

        try {
            fos=new FileOutputStream(zipFileName);
            zos=new ZipOutputStream(fos);

            zos.putNextEntry(new ZipEntry(inputBaseName));

            zos.write(reportData.getBytes());
        }
        catch(IOException ioe) {
            ioe.printStackTrace();
        }
        finally {
            if(zos!=null) try { zos.close(); } catch(IOException ioe) { ioe.printStackTrace(); }
        }
    }

    public static void writeOutCSV(String filepath, Stream<Map<String,Object>> recStream, List<String> fieldList) throws Exception {
        writeOut(filepath, recStream, fieldList, ',', String.format("file generated at: %1$s", formatDateTime(new Date())));
    }

    public static void writeOut(String filepath, Stream<Map<String,Object>> recStream, List<String> fieldList, char fieldSeparator, String fileHeader) throws Exception {
        FileOutputStream fos=new FileOutputStream(filepath);
        PrintWriter out=new PrintWriter(fos);
        Consumer<List<String>> writeFields=sl->{
            out.print(sl.get(0));
            IntStream.range(1, sl.size())
                    .mapToObj(sl::get)
                    .map(s->fieldSeparator+s)
                    .forEach(out::print);
            out.println();
        };

        // write file header
        if(fileHeader!=null && !fileHeader.isEmpty()) {
            out.println(fileHeader);
        }

        // write column-names header
        writeFields.accept(fieldList);

        // write records
        recStream.map(rec->{
                    return fieldList.stream().map(fn->rec.getOrDefault(fn, "").toString()).collect(toList());
                 })
                .forEach(writeFields)
                ;

        out.close();
    }

    public static String getOSSpecificBasePath() {
        if(isThisWindowsOS()) {
            //return "c:/temp/";
            return System.getProperty("user.home")+"/Desktop";
        }
        if(isThisMacOS()) {
            return System.getProperty("user.home")+"/Desktop";
        } else {
            return "/tmp/";
        }
    }

    private static boolean isThisWindowsOS() {
        return System.getProperty("os.name").toLowerCase().contains("windows");
    }

    private static boolean isThisMacOS() {
        return System.getProperty("os.name").toLowerCase().startsWith("mac ");
    }

    public static void writeHead(XMLStreamWriter xmlSw) throws Exception {
        XMLUtils.writeStartElement(xmlSw, 1, "head");
        XMLUtils.writeEmptyElement(xmlSw, 2, "meta", "http-equiv=Content-Type|content=text/html; charset=utf-8");
        XMLUtils.writeStartElement(xmlSw, 2, "style");

        XMLUtils.writeIndentedCharacters(xmlSw, 3, "table, th, td  {");
        XMLUtils.writeIndentedCharacters(xmlSw, 3, "    border:1px solid gray;");
        XMLUtils.writeIndentedCharacters(xmlSw, 3, "    border-collapse:collapse;");
        XMLUtils.writeIndentedCharacters(xmlSw, 3, "}");
        XMLUtils.writeIndentedCharacters(xmlSw, 0, "");

        XMLUtils.writeIndentedCharacters(xmlSw, 3, "td  {");
        XMLUtils.writeIndentedCharacters(xmlSw, 3, "    font-family:calibri;");
        XMLUtils.writeIndentedCharacters(xmlSw, 3, "}");
        XMLUtils.writeIndentedCharacters(xmlSw, 0, "");

        XMLUtils.writeIndentedCharacters(xmlSw, 3, "th  {");
        XMLUtils.writeIndentedCharacters(xmlSw, 3, "    background-color:#ccccee;");
        XMLUtils.writeIndentedCharacters(xmlSw, 3, "    font-family:calibri;");
        XMLUtils.writeIndentedCharacters(xmlSw, 3, "    font-weight:bold;");
        XMLUtils.writeIndentedCharacters(xmlSw, 3, "}");
        XMLUtils.writeIndentedCharacters(xmlSw, 0, "");

        XMLUtils.writeIndentedCharacters(xmlSw, 3, ".avg  {");
        XMLUtils.writeIndentedCharacters(xmlSw, 3, "    width:100px;");
        XMLUtils.writeIndentedCharacters(xmlSw, 3, "    text-align:right;");
        XMLUtils.writeIndentedCharacters(xmlSw, 3, "}");
        XMLUtils.writeIndentedCharacters(xmlSw, 0, "");

        XMLUtils.writeEndElement(xmlSw, 2);
        XMLUtils.writeEndElement(xmlSw, 1);
    }

    public static void writeTableHeader(XMLStreamWriter xmlSw, int indentLevel, String title, int colcount) throws Exception {
        XMLUtils.writeStartElement(xmlSw, indentLevel, "tr");
        XMLUtils.writeFullElement(xmlSw, indentLevel, "th", title, "colspan="+colcount+"|style=background-color:gray;color:white;font-weight:bold;text-align:left;");
        XMLUtils.writeEndElement(xmlSw, indentLevel); // tr
    }

    public static void writeFooter(XMLStreamWriter xmlSw, int indentLevel, Date dt) throws Exception {
        XMLUtils.writeEmptyElement(xmlSw, indentLevel, "br");
        XMLUtils.writeFullElementWithStyle(xmlSw, indentLevel, "hr", "", "margin-bottom=1px;");
        XMLUtils.writeFullElementWithStyle(xmlSw, indentLevel, "span", String.format("Report generated at:%1$tY-%1$tm-%1$td %1$tH:%1$tM:%1$tS", dt), "font-family:calibri;font-size:10pt;");
    }

    public static void writeVerticalBar(XMLStreamWriter xmlSw, int indentLevel, int[] values, String[] colors) throws Exception {
        final int HEIGHT=200;
        final int WIDTH=25;
        int i;
        float sum=0;
        int height;
        BiConsumer<String,String> f=(tag,style)->{try{
            xmlSw.writeStartElement(tag);
            xmlSw.writeAttribute("style", style);
        }catch(Exception e){throw new RuntimeException(e);}};

        for(int x : values) {
            sum+=x;
        }

        f.accept("table", "border:none;");
        for(i=0; i<values.length; i++) {
            height=(int)((HEIGHT*values[i]/sum)+0.5);
            /*
            XMLUtils.writeStartElement(xmlSw, indentLevel, "tr", "style=height:"+height+"px;");
            XMLUtils.writeFullElementWithStyle(xmlSw, indentLevel, "td", "", "border:none;width:40px;padding:0px;");
            XMLUtils.writeFullElementWithStyle(xmlSw, indentLevel, "td", "", "border:none;width:"+WIDTH+"px;padding:0px;background-color:"+colors[i]);
            XMLUtils.writeFullElementWithStyle(xmlSw, indentLevel, "td", "", "border:none;width:40px;padding:0px;");
            XMLUtils.writeEndElement(xmlSw, indentLevel);
            */

            f.accept("tr", "height:"+height+"px;");
            f.accept("td", "border:none;width:40px;padding:0px;"); xmlSw.writeEndElement();
            f.accept("td", "border:none;width:"+WIDTH+"px;padding:0px;background-color:"+colors[i]); xmlSw.writeEndElement();
            f.accept("td", "border:none;width:40px;padding:0px;"); xmlSw.writeEndElement();
            xmlSw.writeEndElement();
        }
        XMLUtils.writeEndElement(xmlSw, indentLevel);
    }

    public static <K,V> Map<K,V> cloneMap(Map<K,V> originalMap) {
        Map<K,V> clonedMap=new HashMap<>();

        originalMap.forEach((k,v)->{
            clonedMap.put(k, v);
        });

        return clonedMap;
    }

    public static Mailer createAWSMailer() {
        Properties props = new Properties();
        props.setProperty("mail.smtp.host", "email-smtp.us-east-1.amazonaws.com");
        props.setProperty("mail.smtp.port", "587");
        props.setProperty("mail.smtp.auth", "true");
        props.setProperty("mail.smtp.auth.user", "AKIAIOOYGDZGLTCWH2HQ");
        props.setProperty("mail.smtp.auth.pass", "AkfAGDH5Xjaw15I0aCvBXbBNDyhU/uMo/MmCUM3nMn8a");
        props.setProperty("mail.smtp.starttls.enable", "true");
        return new Mailer(props, "AKIAIOOYGDZGLTCWH2HQ", "AkfAGDH5Xjaw15I0aCvBXbBNDyhU/uMo/MmCUM3nMn8a");
    }

    public static Mailer createDigiMailer(String username, String passwd) {
        Properties props = new Properties();
        props.setProperty("mail.smtp.host", "smtp.gmail.com");
        props.setProperty("mail.smtp.port", "587");
        props.setProperty("mail.smtp.auth", "true");
        props.setProperty("mail.smtp.auth.user", username);
        props.setProperty("mail.smtp.auth.pass", passwd);
        props.setProperty("mail.smtp.starttls.enable", "true");
        return new Mailer(props, username, passwd);
    }

    public static Mailer createGMailMailer(String username, String passwd) {
        Properties props = new Properties();
        props.setProperty("mail.smtp.host", "smtp.gmail.com");
        props.setProperty("mail.smtp.port", "587");
        props.setProperty("mail.smtp.auth", "true");
        props.setProperty("mail.smtp.auth.user", username);
        props.setProperty("mail.smtp.auth.pass", passwd);
        props.setProperty("mail.smtp.starttls.enable", "true");
        return new Mailer(props, username, passwd);
    }

    public static void sendBIMail(String[] receipients, String subject, String content) throws MessagingException {
        sendBIMail(receipients, subject, content, null);
    }

    public static void sendBIMail(String[] receipients, String subject, String content, File[] stdAttachments) throws MessagingException {
        Mailer mailer=CommonUtils.createAWSMailer();
        final String FROMADDR="netmedsbi@netmeds.com";
        final String FROMNAME="Netmeds BI";

        mailer.sendEmail(FROMADDR, FROMNAME, receipients, subject, content, stdAttachments);
    }

    public static void setTesting() {
        System.setProperty(CommonUtils.SYSPROP_TESTING, "true");
    }

    public static String[] getReceipientsAfterConsideringTest(String[] actualReceipients) {
        String val;

        val=System.getProperty(SYSPROP_TESTING);
        if(val==null || !val.trim().toLowerCase().equals("true")) {
            return actualReceipients;
        }

        if(System.getProperties().containsKey(SYSPROP_TESTING_MAIL)) {
            return System.getProperty(SYSPROP_TESTING_MAIL).split("\\s*,\\s*");
        } else {
            return new String[]{"prem.kumar@netmeds.com"};
        }
    }

    public static String getLocationNameForFCCode(String fccode) {
        if(fccode==null) return "-null-";
        fccode=fccode.trim();

        for(FC fc : FC.values()) {
            if(fc.getId().equals(fccode)) {
                return fc.getCity()+" / "+fc.getCompanyInitials();
            }
        }

        return "FC:"+fccode;
    }

    public static Optional<FC> getFCForWarehouseCode(String whcode) {
        if(whcode==null) return Optional.empty();
        whcode="6"+whcode.substring(1);

        for(FC fc : FC.values()) {
            if(fc.getId().equals(whcode)) {
                return Optional.of(fc);
            }
        }

        return Optional.empty();
    }

    public static Map<String,BigDecimal> apportion(BigDecimal value, Map<String,BigDecimal> ratioSourceMap) {
        Map<String,BigDecimal> resultMap=new HashMap<>();
        BigDecimal denom=BigDecimal.ZERO;

        for(String key : ratioSourceMap.keySet()) {
            denom=denom.add(ratioSourceMap.get(key));
        }

        if(denom.compareTo(BigDecimal.ZERO)==0) {
            //ratioSourceMap; // is this right?????????
            return ratioSourceMap.keySet().stream().collect(Collectors.toMap(Function.identity(), x->BigDecimal.ZERO));
        }

        for(String key : ratioSourceMap.keySet()) {
            BigDecimal incv=ratioSourceMap.get(key);

            if(incv==null || denom==null) {
                resultMap.put(key, BigDecimal.ZERO);
            } else {
                resultMap.put(key, value.multiply(incv).divide(denom, 2, RoundingMode.HALF_UP));
            }
        }

        return resultMap;
    }

    private static final Calendar cal=new GregorianCalendar();
    private static final long EPOCH=getEpoch();

    private static long getEpoch() {
    	SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    	try {
    		return sdf.parse("2015-01-01 00:00:00").getTime();
    	}
    	catch(ParseException pe) {
    		return -1;
    	}
    }

    public static int getTimeId(Date dt) {
    	long diff;

        if(EPOCH==-1) throw new RuntimeException("unable to identify EPOCH!");
        synchronized(cal) {
            cal.setTime(dt);
            cal.add(Calendar.MILLISECOND, 59999);

            diff=(cal.getTimeInMillis()-EPOCH);

            return (int)(diff/60000);
        }
    }

    public static boolean compareTwoResultSets(ResultSet rs1, ResultSet rs2, String idcol) throws Exception {
        Map<String,Object> rec1;
        Logger logger=Logger.getLogger(CommonUtils.class.getName());
        BiFunction<ResultSet,String,Object> getVal=(rs,fn)->{
            try { return rs.getObject(fn); }
            catch(Exception e) {
                throw new RuntimeException(e);
            }
        };
        Functional.Tuple1<Boolean> result=new Functional.Tuple1<>();

        result._1=true;

        while(rs1.next()) {
            if(!rs2.next()) {
                logger.log(Level.INFO, "rs2 is shorter");
                return false;
            }

            rec1=SQLFront.mapResultSetToMap(rs1);


            rec1.forEach((k,v1)->{
                if(k.equals("rank_by_processed_value")) return;
                if(k.equals("rank_identification_time")) return;
                if(k.equals("rank_by_received_value")) return;
                Object v2=getVal.apply(rs2, k);
                String idval=getVal.apply(rs1, idcol).toString();

                if(v1==null && v2==null) {
                    return;
                }
                if(v1==null && v2!=null) {
                    logger.log(Level.INFO, "id:{0}. col:{1}. second table has non-null value where first has null value", new Object[]{idval,k});
                    result._1=false;
                    return;
                }
                if(v1!=null && v2==null) {
                    logger.log(Level.INFO, "id:{0}. col:{1}. second table has null value where first has non-null value", new Object[]{idval,k});
                    result._1=false;
                    return;
                }
                if(!v1.equals(v2)) {
                    logger.log(Level.INFO, "id:{0}. col:{1}. values differ. v1:{2}. v2:{3}", new Object[]{idval,k, v1, v2});
                    result._1=false;
                    return;
                }
            });
        }

        if(rs2.next()) {
            logger.log(Level.INFO, "rs2 is longer");
            return false;
        }

        return result._1;
    }

    public static void zipFile(String inputFileName, String zipFileName) throws IOException {
        FileOutputStream fos=null;
        ZipOutputStream zos=null;
        FileInputStream fis=null;
        String inputBaseName=new File(inputFileName).getName();
        final int LEN=2048;
        byte[] buf=new byte[LEN];
        int red;

        try {
            fos=new FileOutputStream(zipFileName);
            zos=new ZipOutputStream(fos);

            zos.putNextEntry(new ZipEntry(inputBaseName));

            fis=new FileInputStream(inputFileName);

            while((red=fis.read(buf, 0, LEN))!=-1) {
                zos.write(buf, 0, red);
            }
        }
        finally {
            if(zos!=null) zos.close();
            if(fis!=null) fis.close();
        }
    }
    public static byte[] computeChecksum(String data, String algo, String secretKey) {
        //String algo=ocxt.getConfigManager().getStringParam("myjio.oatoken.checksum_algo", "HMACSHA256");
        //String secretKey=ocxt.getConfigManager().getStringParam("myjio.oatoken.checksum_key", "yo9d4258bfa1e5a4424a7981a07dbc23c08");
        try {
            SecretKeySpec key=new SecretKeySpec(secretKey.getBytes(), algo);
            Mac mac=Mac.getInstance(algo);
            mac.init(key);
            //return Base64.getEncoder().encodeToString(mac.doFinal(data.getBytes()));
            return mac.doFinal(data.getBytes());
        }
        catch(Exception e) {
            LOGGER.log(Level.SEVERE, "thrown exception while generating checksum", e);
            return null;
        }
    }
        public static String encodeToHex(byte[] data, int minDigits) {
        String hexstr=encodeToHex(data);

        if(hexstr.length()<minDigits) {
            int neededLen=minDigits-hexstr.length();
            int i=0;
            StringBuilder sb=new StringBuilder();

            while(i<neededLen) {
                sb.append('0');
                i++;
            }

            hexstr=sb.toString()+hexstr;
        }

        return hexstr;
    }

    public static String encodeToHex(byte[] data) {
        return new BigInteger(1, data).toString(16).toLowerCase();
    }
}
