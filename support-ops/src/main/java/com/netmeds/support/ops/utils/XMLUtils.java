/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.netmeds.support.ops.utils;

import java.io.Writer;
import java.util.HashMap;
import java.util.Map;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

/**
 *
 * @author prem
 */
public class XMLUtils {

    private static final XMLOutputFactory XOF=XMLOutputFactory.newInstance();

    private XMLUtils(){}

    public static XMLStreamWriter createXMLStreamWriter(Writer w) throws XMLStreamException {
        return XOF.createXMLStreamWriter(w);
    }

    public static void writeStartElement(XMLStreamWriter xmlSw, int indentLevel, String elementName) throws Exception {
        writeStartElement(xmlSw, indentLevel, elementName, (Map)null);
    }

    public static void writeStartElement(XMLStreamWriter xmlSw, int indentLevel, String elementName, String attrs) throws Exception {
        writeStartElement(xmlSw, indentLevel, elementName, getAttrMap(attrs));
    }

    private static Map<String,String> handleSpecialCaseForEmailingHTML(String elementName, Map<String,String> attrsMap) {
        String cvalue;

        if(attrsMap==null) {
            attrsMap=new HashMap<>();
        }

        switch(elementName) {
            case "table":
                    cvalue="border:1px solid gray;border-collapse:collapse;";
                    break;

            case "th":
                    cvalue="border:1px solid gray;background-color:#CCCCEE;font-family:calibri;font-weight:bold;";
                    break;

            case "td":
                    cvalue="border:1px solid gray;font-family:calibri;";
                    break;

            default: return attrsMap;
        }

        if(attrsMap.containsKey("class")) {
            cvalue=cvalue+"width:100px;text-align:right;";
        }

        if(attrsMap.containsKey("style")) {
            cvalue=cvalue+";"+attrsMap.get("style");
        }
        attrsMap.put("style", cvalue);

        return attrsMap;
    }

    public static void writeStartElement(XMLStreamWriter xmlSw, int indentLevel, String elementName, Map<String,String> attrsMap) throws Exception {
        startLevel(xmlSw, indentLevel);
        xmlSw.writeStartElement(elementName);
        attrsMap=handleSpecialCaseForEmailingHTML(elementName, attrsMap);
        writeAttrs(xmlSw, attrsMap);
    }

    public static void writeEmptyElement(XMLStreamWriter xmlSw, int indentLevel, String elementName) throws Exception {
        startLevel(xmlSw, indentLevel);
        xmlSw.writeEmptyElement(elementName);
    }

    public static void writeEmptyElement(XMLStreamWriter xmlSw, int indentLevel, String elementName, String attrs) throws Exception {
        startLevel(xmlSw, indentLevel);
        xmlSw.writeEmptyElement(elementName);
        writeAttrs(xmlSw, getAttrMap(attrs));
    }

    public static void writeEndElement(XMLStreamWriter xmlSw, int indentLevel) throws Exception {
        startLevel(xmlSw, indentLevel);
        xmlSw.writeEndElement();
    }

    /* function name pun un-intended */
    public static void writeFullElementWithStyle(XMLStreamWriter xmlSw, int indentLevel, String elementName, String content, String style) throws Exception {
        Map styleMap=new HashMap<>();

        styleMap.put("style", style);
        writeFullElement(xmlSw, indentLevel, elementName, content, styleMap);
    }

    public static void writeFullElement(XMLStreamWriter xmlSw, int indentLevel, String elementName, String content) throws Exception {
        writeFullElement(xmlSw, indentLevel, elementName, content, (Map)null);
    }

    public static void writeFullElement(XMLStreamWriter xmlSw, int indentLevel, String elementName, String content, String attrs) throws Exception {
        writeFullElement(xmlSw, indentLevel, elementName, content, getAttrMap(attrs));
    }

    public static void writeFullTDWithNo(XMLStreamWriter xmlSw, int indentLevel, Number num, String attrs) throws Exception {
        String content;
        Map<String,String> attrMap;

        content=(num==null || num.longValue()==0)?"-":num.toString();
        attrMap=getAttrMap(attrs);
        addToStyle(attrMap, "text-align:right;");

        writeFullElement(xmlSw, indentLevel, "td", content, attrMap);
    }

    private static Map<String,String> getAttrMap(String attrstr) {
        Map<String, String> attrmap=new HashMap<>();
        String[] attra=attrstr.split("\\|");

        for(String attr : attra) {
            if(attr.contains("="))
            attrmap.put(attr.split("=")[0], attr.split("=")[1]);
        }

        return attrmap;
    }

    private static void addToStyle(Map<String,String> attrMap, String value) {
        String ev;

        ev=attrMap.get("style");
        if(ev==null) {
            ev=value;
        } else {
            ev=ev+";"+value;
        }

        attrMap.put("style", ev);
    }

    private static void writeAttrs(XMLStreamWriter xmlSw, Map<String,String> attrsMap) throws Exception {
        if(attrsMap!=null) {
            for(String attrName : attrsMap.keySet()) {
                xmlSw.writeAttribute(attrName, attrsMap.get(attrName));
            }
        }
    }

    public static void writeFullElement(XMLStreamWriter xmlSw, int indentLevel, String elementName, String content, Map<String,String> attrsMap) throws Exception {
        writeStartElement(xmlSw, indentLevel, elementName, attrsMap);
        xmlSw.writeCharacters(content);
        xmlSw.writeEndElement();
    }

    public static void writeIndentedCharacters(XMLStreamWriter xmlSw, int indentLevel, String chars) throws Exception {
        startLevel(xmlSw, indentLevel);
        xmlSw.writeCharacters(chars);
    }

    public static void startLevel(XMLStreamWriter xmlSw, int n) throws Exception {
        xmlSw.writeCharacters("\n");
        for(int i=0;  i<n;  i++) xmlSw.writeCharacters("\t");
    }
}
