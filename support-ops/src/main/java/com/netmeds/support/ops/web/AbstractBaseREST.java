/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.netmeds.support.ops.web;

import com.netmeds.support.ops.web.SessionManagerREST.User;
import java.net.URI;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.UriInfo;

/**
 *
 * @author prem
 */
public abstract class AbstractBaseREST {

    private static final Logger logger=Logger.getLogger(AbstractBaseREST.class.getName());

    @Context
    protected UriInfo uriInfo;

    @Context
    protected HttpHeaders headers;

    @Context
    protected ServletContext sc;

    @Context
    protected HttpServletRequest servreq;

    /**
     * would return the URL to the base path of the application on which
     * this REST context is deployed. For example for the call:
     *     http://devserver:8080/wpp/test/test_this_call
     * this method would return 'http://devserver:8080/wpp'
     */
    protected String getBasePath() {
        String appContextPath;
        URI baseUri;

        appContextPath=sc.getContextPath();
        baseUri=uriInfo.getBaseUri();

        return String.format("%1$s://%2$s:%3$d%4$s", baseUri.getScheme(), baseUri.getHost(), baseUri.getPort(), appContextPath);
    }

    protected String getContextPath() {
        return sc.getContextPath();
    }

    protected Principal getPrincipal() {
          return getPrincipal(headers);
    }

    protected Principal getPrincipal(HttpHeaders headers) {
        return getPrincipal((String)headers.getHeaderString("userid"));
    }

    protected Principal getPrincipal(String userIdStr) {
        EntityManager em=null;
        User user=null;
        Principal principal;

        try {
            user=User.forId(userIdStr);
            principal=new Principal(user, this);
        }
        finally {
            if(em!=null) em.close();
        }
        return principal;
    }

    public static class Principal {
        private final User user;
        private final AbstractBaseREST base;

        public Principal(User user, AbstractBaseREST base) {
            this.user=user;
            this.base=base;
        }

        public User getUser() {
            return this.user;
        }

        public boolean hasRole() {
            return user.getRole().equalsIgnoreCase("admin");
        }
    }
}
