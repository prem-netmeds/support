/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.netmeds.support.ops;


/**
 *
 * @author prem
 */
public class EnvTest {

    public static void mainNetmeds(String[] args) {
        System.out.println("checking stage mstar...");
        Environments.NetmedsStage.getMstarSF();
        System.out.println("checking stage tpod...");
        Environments.NetmedsStage.getTpodSF();
        System.out.println("checking stage tpod logs...");
        Environments.NetmedsStage.getTpodLogSF();

        System.out.println("checking prod mstar...");
        Environments.NetmedsProd.getMstarSF();
        System.out.println("checking prod tpod...");
        Environments.NetmedsProd.getTpodSF();
        System.out.println("checking prod tpod logs...");
        Environments.NetmedsProd.getTpodLogSF();
    }

    public static void mainJiomart(String[] args) {
        System.out.println("checking stage mstar...");
        Environments.JiomartStage.getMstarSF();
        System.out.println("checking stage tpod...");
        Environments.JiomartStage.getTpodSF();
        System.out.println("checking stage tpod logs...");
        Environments.JiomartStage.getTpodLogSF();

        System.out.println("checking prod mstar...");
        Environments.JiomartProd.getMstarSF();
        System.out.println("checking prod tpod...");
        Environments.JiomartProd.getTpodSF();
        System.out.println("checking prod tpod logs...");
        Environments.JiomartProd.getTpodLogSF();
    }

    public static void main(String[] args) {
        //mainNetmeds(args);
        mainJiomart(args);
    }

}
