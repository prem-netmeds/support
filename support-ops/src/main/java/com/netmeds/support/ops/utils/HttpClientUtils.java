/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.netmeds.support.ops.utils;

import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Map;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContextBuilder;
import org.apache.http.ssl.TrustStrategy;
import org.apache.http.util.EntityUtils;

/**
 *
 * @author Admin
 */
public class HttpClientUtils {
    
    public static CloseableHttpClient getClosableHttpClient(boolean reqSSLCerVer) throws NoSuchAlgorithmException, KeyStoreException, KeyManagementException{
        SSLContextBuilder builder = new SSLContextBuilder();
        builder.loadTrustMaterial(null, new TrustStrategy() {
             @Override
             public boolean isTrusted(final X509Certificate[] chain, String authType) throws CertificateException {
                  return true;
             }
        });
        SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(builder.build());
        CloseableHttpClient httpClient;
        if(reqSSLCerVer){
            httpClient = HttpClients.createDefault();
        }else{
            httpClient = HttpClients.custom().setSSLSocketFactory(sslsf).build();
        }        
        return httpClient;    
    }
    
    public static String performHttpGet(Map<String,Object> httpRec,boolean reqSSLCerVer,String apiTitle)throws Exception{
        String responseText = null, statusLine, url, userName, password, authType;
        try {
            CloseableHttpClient httpClient = getClosableHttpClient(reqSSLCerVer);
            boolean authReq = (boolean)httpRec.get("auth_status");
            url = httpRec.get("url").toString();
            URIBuilder uriBuilder = new URIBuilder(url);
            if(httpRec.containsKey("params")){
                Map<String,Object> rec = (Map<String,Object>) httpRec.get("params");
                for (String key : rec.keySet()) {
                    uriBuilder.setParameter(key, rec.get(key)+"");
                }
            } 
            
            HttpGet httpGet = new HttpGet(uriBuilder.build().toString());
            if(authReq){
                userName = httpRec.get("user_name").toString();
                password = httpRec.get("password").toString();
                authType = httpRec.get("auth_type").toString();
                
                String encodedStr = Utils.encodeStringToBase64(userName+":"+password);
                httpGet.setHeader("Authorization", authType+" "+encodedStr);
            }
            if(httpRec.containsKey("headers")){
                Map<String,Object> rec = (Map<String,Object>) httpRec.get("headers");
                for (String key : rec.keySet()) {
                    httpGet.setHeader(key, rec.get(key)+"");
                }
            }
            
            CloseableHttpResponse httpResponse = httpClient.execute(httpGet);
            statusLine = httpResponse.getStatusLine().toString();
            HttpEntity entity = httpResponse.getEntity();
            
            responseText = EntityUtils.toString(entity);
            httpClient.close();
            httpResponse.close();
            
        } catch (Exception e) {
            Throwable t = ExceptionUtils.getRootCause(e);
            ExceptionUtils.logExceptionStackTrace(t);
            String msg = ExceptionUtils.getExceptionMessage(t,String.format("http get api '%1$s' failed", apiTitle));
            throw new RuntimeException(msg);
        }
        if(statusLine == null || !statusLine.equalsIgnoreCase("HTTP/1.1 200 OK")){
            throw new RuntimeException(String.format("http get api '%1$s' failed. http status: %2$s. response text (%3$s)", apiTitle, statusLine, responseText));
        }
        return responseText;        
    }
}
