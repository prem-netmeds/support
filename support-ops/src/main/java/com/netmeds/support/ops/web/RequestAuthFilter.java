/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.netmeds.support.ops.web;

import com.netmeds.support.ops.ApplicationConfig;
import static com.netmeds.support.ops.utils.RESTUtils.createFailedResponseJson;
import java.io.IOException;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.Response;

/**
 *
 * @author prem
 */
public class RequestAuthFilter implements ContainerRequestFilter {

    private static final Logger LOGGER=Logger.getLogger(RequestAuthFilter.class.getName());

    @Override
    public void filter(ContainerRequestContext requestContext) throws IOException {
        String path;
        path=requestContext.getUriInfo().getPath();
        LOGGER.log(Level.INFO, "RequestAuthFilter invoked for path:{0}", path);
        
        if(path.startsWith(ApplicationConfig.URL_SESSION) || path.startsWith("/mstar/get/smart-recon-report")) return;
        
        Map<String,Object> sessionDetails = SessionManagerREST.getSessionDetails(requestContext);
        if(!(boolean)sessionDetails.get("is_valid_session"))
            requestContext.abortWith(Response.status(Response.Status.UNAUTHORIZED).entity(createFailedResponseJson(sessionDetails.get("message")+"")).build());
    }

}
