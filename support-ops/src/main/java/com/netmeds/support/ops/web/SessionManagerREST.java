/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.netmeds.support.ops.web;

import static com.netmeds.support.ops.ApplicationConfig.URL_SESSION;
import com.netmeds.support.ops.utils.CommonUtils;
import com.netmeds.support.ops.utils.DateUtils;
import com.netmeds.support.ops.utils.ExceptionUtils;
import com.netmeds.support.ops.utils.RESTUtils;
import static com.netmeds.support.ops.utils.RESTUtils.createSuccessResponseJson;
import static com.netmeds.support.ops.utils.RESTUtils.sendBadRequestResponse;
import static com.netmeds.support.ops.utils.RESTUtils.sendUnAuthorizedResponse;
import com.netmeds.support.ops.utils.SQLFront;
import static com.netmeds.support.ops.utils.Utils.digestStringToSHA1;
import static com.netmeds.support.ops.utils.Utils.digestStringToSHA256;
import java.io.StringReader;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.json.Json;
import javax.json.JsonObjectBuilder;
import javax.json.JsonReader;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author prem
 */
@Path(URL_SESSION)
public class SessionManagerREST extends AbstractBaseREST {

    private static final Logger LOGGER=Logger.getLogger(SessionManagerREST.class.getName());

    public static class User {

        private String userName;
        private String userId;
        private String passwordHashSha1;
        private int passwordResetCode;
        private Date resetCodeValidTill;
        private String userEmail;
        private String role;
        private String team;

        public User(String userName, String userId, String passwordHashSha1, int passwordResetCode, Date resetCodeValidTill, 
                String userEmail, String role, String team) {
            this.userName = userName;
            this.userId = userId;
            this.passwordHashSha1 = passwordHashSha1;
            this.passwordResetCode = passwordResetCode;
            this.resetCodeValidTill = resetCodeValidTill;
            this.userEmail = userEmail;
            this.role = role;
            this.team = team;
        }

        public String getUserName() {return userName;}
        public String getUserId() {return userId;}
        public String getPasswordHashSha1() {return passwordHashSha1;}
        public int getPasswordResetCode() {return passwordResetCode;}
        public Date getResetCodeValidTill() {return resetCodeValidTill;}
        public String getUserEmail() {return userEmail;}
        public String getRole() {return role;}
        public String getTeam() {return team;}

        public static User forIdx(String userid) {
            return new User(userid, userid, "", 0, null, userid+"@netmeds.com", "user", "users");
        }
        
        public static User forId(String curUserId) {
            User user = null;
            try(Connection con = SQLFront.getJNDIConnection("sptOps")) {
                String sql = "select * from spt_ops_user where user_id = '"+curUserId+"' AND active = 1;";
                Statement stmt = con.createStatement();
                ResultSet rs = stmt.executeQuery(sql);
                String userName,userId,passHashSha1,userEmail,role,team;
                int passResetCode;
                Date resetCodeValidTill;
                while(rs.next()){
                    userName = rs.getString("user_name");
                    userId = rs.getString("user_id");
                    passHashSha1 = rs.getString("pass_hash_sha1");
                    passResetCode = rs.getInt("pass_reset_code");
                    resetCodeValidTill = rs.getDate("reset_code_valid_till");
                    userEmail = rs.getString("user_email");
                    role = rs.getString("role");
                    //role = rs.getString("roles");
                    team = rs.getString("team");
                    
                    if(curUserId.equalsIgnoreCase(userId)){
                        user = new User(userName,userId,passHashSha1,passResetCode,resetCodeValidTill,userEmail,role,team);
                    }
                }
            } catch (Exception e) {
                LOGGER.log(Level.SEVERE, null, e);
                throw new RuntimeException(String.format("unable to retrieve user data. message: %1$s", e.getMessage()));
            }            
            return user;            
        }
        
        public static User forEmail(String curUserEmail) {
            User user = null;
            try(Connection con = SQLFront.getJNDIConnection("sptOps")) {
                String sql = "select * from spt_ops_user where user_email = '"+curUserEmail+"' AND active = 1;";
                Statement stmt = con.createStatement();
                ResultSet rs = stmt.executeQuery(sql);
                String userName,userId,passHashSha1,userEmail,role,team;
                int passResetCode;
                Date resetCodeValidTill;
                while(rs.next()){
                    userName = rs.getString("user_name");
                    userId = rs.getString("user_id");
                    passHashSha1 = rs.getString("pass_hash_sha1");
                    passResetCode = rs.getInt("pass_reset_code");
                    resetCodeValidTill = rs.getDate("reset_code_valid_till");
                    userEmail = rs.getString("user_email");
                    role = rs.getString("role");
                    team = rs.getString("team");
                    
                    if(curUserEmail.equalsIgnoreCase(userEmail)){
                        user = new User(userName,userId,passHashSha1,passResetCode,resetCodeValidTill,userEmail,role,team);
                    }
                }
            } catch (Exception e) {
                LOGGER.log(Level.SEVERE, null, e);
                throw new RuntimeException(String.format("unable to retrieve user data. message: %1$s", e.getMessage()));
            }            
            return user;            
        }
    }
    
     public static User getUserByReqContext(ContainerRequestContext requestContext){
        User user = null;
        String sessionId=requestContext.getHeaderString("session_id");
        if(sessionId!=null && !sessionId.trim().isEmpty()) {
            if(sessionMap.containsKey(sessionId)){
                SessionData sd=sessionMap.get(sessionId);
                user = sd.getUser();
            }               
        }
        return user;
    }
    
    public static User getUserByHttpServletReq(HttpServletRequest request){
        User user = null;
        String sessionId=request.getHeader("session_id");
        if(sessionId!=null && !sessionId.trim().isEmpty()) {
            if(sessionMap.containsKey(sessionId)){
                SessionData sd=sessionMap.get(sessionId);
                user = sd.getUser();
            }               
        }
        return user;
    }
    
    public static User getUserBySessionId(String sessionId){
        User user = null;
        if(sessionId!=null && !sessionId.trim().isEmpty()) {
            if(sessionMap.containsKey(sessionId)){
                SessionData sd=sessionMap.get(sessionId);
                user = sd.getUser();
            }               
        }
        return user;
    }
    
     private static class SessionData {
        private final String sessionId;
        private final User user;
        private final Date validTill;

        public SessionData(String sessionId,User user,Date validTill) {
            this.sessionId = sessionId;
            this.user = user;
            this.validTill = validTill;
        }

        public String getSessionId() {
            return sessionId;
        }

        public User getUser() {
            return user;
        }
        
        public Date getValidTill() {
            return validTill;
        }      
    }

    private static final Map<String,SessionData> sessionMap=new HashMap<>();
    
     public static String getSessionId(){
        String sessionId = null;
        for (String sId : sessionMap.keySet()) {
            sessionId = sId;
        }
        return sessionId;
    }

    public static Map<String,Object> getSessionDetails(ContainerRequestContext requestContext) {
        Map<String,Object> rec = new HashMap();
        SessionData sd=null;

        String sessionId=requestContext.getHeaderString("session_id");
        if(sessionId!=null && !sessionId.trim().isEmpty()) {
            if(sessionMap.containsKey(sessionId)){
                sd=sessionMap.get(sessionId);
            }               
        }
        if(sd!=null){
            if(sd.getValidTill().after(new Date())){
                rec.put("is_valid_session", true);
                rec.put("message", "Session is Valid!");
            }else{
                sessionMap.remove(sessionId);
                rec.put("is_valid_session", false);
                rec.put("message", "Session Expired!");
            }
        }else{
            rec.put("is_valid_session", false);
            rec.put("message", "Unauthorized!");
        }
        return rec;
    }
    
    public static Map<String,Object> executeSendPasswordResetCode(String userEmail){
        Map<String,Object> resetCodeStRec = new HashMap();
        SQLFront sf = null;
        try {
            sf = SQLFront.newSptOps();
            String sql = "select * from spt_ops_user where user_email = '"+userEmail+"' and active;";
            List<Map<String,Object>> recList = sf.getResult(sql);
            
            if(recList.size()>0){
                Map<String, Object> rec = recList.get(0);
                Date codeValidTill = (Date)rec.get("reset_code_valid_till");
                String userId = (String)rec.get("user_id");
                if(codeValidTill.after(new Date())){
                    resetCodeStRec.put("status", false);
                    resetCodeStRec.put("message", String.format("Password reset code already sent to your registered email id. Please, check your email or try after '%1$s'",DateUtils.formatDateTime(codeValidTill)));
                }else{
                    Random rnd = new Random();
                    int number = rnd.nextInt(999999);
                    String resetCode = String.format("%06d", number);
                    
                    String content = "<table>\n" +
                    "	<tbody>\n" +
                    "		<tr>\n" +
                    "			<td style=\"padding:0;font-family:'Segoe UI Semibold','Segoe UI Bold','Segoe UI','Helvetica Neue Medium',Arial,sans-serif;font-size:17px;color:#707070\">\n" +
                    "				Spotlight account\n" +
                    "			</td>\n" +
                    "		</tr>\n" +
                    "		<tr>\n" +
                    "			<td style=\"padding:0;font-family:'Segoe UI Light','Segoe UI','Helvetica Neue Medium',Arial,sans-serif;font-size:41px;color:#2672ec\">\n" +
                    "				Password reset code\n" +
                    "			</td>\n" +
                    "		</tr>\n" +
                    "		<tr>\n" +
                    "			<td id=\"m_-467144735163255209i3\" style=\"padding:0;padding-top:25px;font-family:'Segoe UI',Tahoma,Verdana,Arial,sans-serif;font-size:14px;color:#2a2a2a\">\n" +
                    "				Please use this code to reset the password for the Spotlight account id <strong>"+userId+"</strong>.\n" +
                    "			</td>\n" +
                    "		</tr>\n" +
                    "		<tr>\n" +
                    "			<td style=\"padding:0;padding-top:25px;font-family:'Segoe UI',Tahoma,Verdana,Arial,sans-serif;font-size:14px;color:#2a2a2a\">\n" +
                    "				Here is your code: <strong>"+resetCode+"</strong>\n" +
                    "			</td>\n" +
                    "		</tr>\n" +
                    "		<tr>\n" +
                    "			<td style=\"padding:0;padding-top:25px;font-family:'Segoe UI',Tahoma,Verdana,Arial,sans-serif;font-size:14px;color:#2a2a2a\">Thanks,</td></tr>\n" +
                    "		<tr>\n" +
                    "			<td style=\"padding:0;font-family:'Segoe UI',Tahoma,Verdana,Arial,sans-serif;font-size:14px;color:#2a2a2a\">\n" +
                    "				The Netmeds BI account team\n" +
                    "			</td>\n" +
                    "		</tr>\n" +
                    "	</tbody>\n" +
                    "</table>";
                    
                    CommonUtils.sendBIMail(new String[]{userEmail}, "Spotlight account password reset", content);
                    
                    Map<String, Object> updateRec = new HashMap();
                    updateRec.put("pass_reset_code", resetCode);
                    updateRec.put("reset_code_valid_till", new Date(System.currentTimeMillis()+(1000*60*10)));
                    sf.update("spt_ops_user", updateRec, "user_email = '"+userEmail+"' and active");
                    
                    resetCodeStRec.put("status", true);
                    resetCodeStRec.put("message", String.format("Password reset code has been sent successfully to your resgistered email id '%1$s'.",userEmail));
                }
            }else{
                resetCodeStRec.put("status", false);
                resetCodeStRec.put("message", String.format("User email '%1$s' not registered with Spotlight. Please, contact support.",userEmail));
            }            
        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, null, e);
            Throwable t = ExceptionUtils.getRootCause(e);
            ExceptionUtils.logExceptionStackTrace(t);
            String msg = ExceptionUtils.getExceptionMessage(t,"Password reset code send failed");
            throw new RuntimeException(msg);
        }finally{
            if(sf!=null)sf.close();
        }
        return resetCodeStRec;    
    }
    
    public static boolean validateNewPassword(String password) {
        Pattern pattern = Pattern.compile("((?=.*[a-z])(?=.*\\d)(?=.*[A-Z])(?=.*[@#$%!]).{8,40})");
        Matcher matcher = pattern.matcher(password);
        return matcher.matches();
    }
    
    public static Map<String,Object> executeResetPassword(String userEmail,String resetCode,String newPass){
        Map<String,Object> resetPassStRec=  new HashMap();
        SQLFront sf = null;
        try {
            sf = SQLFront.newSptOps();
            String sql = "select * from spt_ops_user where user_email = '"+userEmail+"' and active;";
            List<Map<String,Object>> recList = sf.getResult(sql);
            if(recList.size()>0){
                Map<String, Object> rec = recList.get(0);
                Date codeValidTill = (Date)rec.get("reset_code_valid_till");
                if(codeValidTill.after(new Date())){
                    String dbResetCode = rec.get("pass_reset_code")+"";
                    if(dbResetCode.equals(resetCode)){
                        if(validateNewPassword(newPass)){
                            Map<String, Object> updateRec = new HashMap();
                            updateRec.put("pass_hash_sha1", digestStringToSHA1(newPass));
                            sf.update("spt_ops_user", updateRec, "user_email = '"+userEmail+"' and active");

                            resetPassStRec.put("status", true);
                            resetPassStRec.put("message", "Password has been updated successfully!");
                        }else{
                            StringBuilder sb = new StringBuilder();
                            sb.append("Password policy violation!\n");
                            sb.append("Password Policy:\n");
                            sb.append("1. Be between 8 and 40 characters long.\n");
                            sb.append("2. Contain at least one digit.\n");
                            sb.append("3. Contain at least one lower case character.\n");
                            sb.append("4. Contain at least one upper case character.\n");
                            sb.append("5. Contain at least on special character from [ @ # $ % ! . ].\n");

                            resetPassStRec.put("status", false);
                            resetPassStRec.put("message", sb.toString());
                        }                        
                    }else{
                        resetPassStRec.put("status", false);
                        resetPassStRec.put("message", "Invalid password reset code!");
                    }                    
                }else{
                    resetPassStRec.put("status", false);
                    resetPassStRec.put("message", "Password reset code expired!");
                }
            }else{
                resetPassStRec.put("status", false);
                resetPassStRec.put("message", String.format("User email '%1$s' not registered with Spotlight. Please, contact support.",userEmail));
            }   
        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, null, e);
            Throwable t = ExceptionUtils.getRootCause(e);
            ExceptionUtils.logExceptionStackTrace(t);
            String msg = ExceptionUtils.getExceptionMessage(t,"Password reset failed!");
            throw new RuntimeException(msg);
        }finally{
            if(sf!=null)sf.close();
        }
        return resetPassStRec;    
    }
    
    @POST
    @Path("/login")
    @Produces(MediaType.APPLICATION_JSON)
    public Response login(@FormParam("userid") String userid, @FormParam("passwd") String passwd) {
        List<String> errorMessageList=new ArrayList<>();
        User user;
        SessionData sd=null;
        String sessionId=null;
        JsonObjectBuilder jsob;

        //logger.log(Level.INFO, "login called with parameters: userid:{0}, passwd:{1}", new String[]{userid, passwd});

        if(userid==null || userid.trim().isEmpty()) {
            errorMessageList.add("required parameter 'userid' is missing in request");
        }
        if(passwd==null || passwd.trim().isEmpty()) {
            errorMessageList.add("required parameter 'passwd' is missing in request");
        }
        if(!errorMessageList.isEmpty()) {
            return sendBadRequestResponse(errorMessageList);
        }

        user=User.forIdx(userid);
        if(user==null) {
            return sendUnAuthorizedResponse("Invalid User Id!");
        }

        //if(!user.getPasswordHashSha1().equalsIgnoreCase(digestStringToSHA1(passwd))) {
        //    return sendUnAuthorizedResponse("Invalid Password!");
        //}
        
        for (String key : sessionMap.keySet()) {
            SessionData sessionData = sessionMap.get(key);
            User sessionUser = sessionData.getUser();
            if(sessionUser.getUserId().equals(userid)){
                sd=sessionData;
                sessionId=key;
            }
        }
        
        if(sd!=null && sessionId!=null){
            if(sd.getValidTill().before(new Date())) {
                sessionMap.remove(sessionId);
                sd=null;
            }
        }

        if(sd==null) {
            sd=new SessionData(digestStringToSHA256(user.getUserName() + new Date()),user, new Date(System.currentTimeMillis()+(1000*60*1440))); // for 24 hr
            sessionMap.put(sd.getSessionId(), sd);
        }else{
            sessionId=sd.getSessionId();
            sd=new SessionData(sessionId, user, new Date(System.currentTimeMillis()+(1000*60*1440))); // for 24 hr
            sessionMap.put(sessionId, sd);
        }
        
        LOGGER.log(Level.INFO, String.format("session created for user id '%1$s' and session valid till '%2$s'", 
                user.getUserId(),DateUtils.formatDateTime(sd.getValidTill())));
        LOGGER.log(Level.INFO, String.format("session map count: %1$s", sessionMap.size()));
        
        jsob=Json.createObjectBuilder();
        jsob.add("user_id", user.getUserId());
        jsob.add("session_id", sd.getSessionId());
        jsob.add("user_name", user.getUserName());
        jsob.add("role", user.getRole());
        jsob.add("team", user.getTeam());
        jsob.add("valid_till", DateUtils.formatDateTime(sd.getValidTill()));

        return Response.ok(createSuccessResponseJson(jsob.build())).build();
    }
    
    @GET
    @Path("/logout")
    public Response logout(@HeaderParam("session_id") String sessionId) {
        try {
            sessionId = getSessionId();
            if(sessionId!=null && !sessionId.trim().isEmpty()) {
                if(sessionMap.containsKey(sessionId)){
                    sessionMap.remove(sessionId);
                    LOGGER.log(Level.INFO, String.format("session map count: %1$s", sessionMap.size()));
                    return Response.status(Response.Status.OK).entity(RESTUtils.createSuccessResponseJson("success")).build();
                }else{
                    return Response.status(Response.Status.UNAUTHORIZED).entity(RESTUtils.createFailedResponseJson("invalid session id")).build();
                }               
            }else{
                return Response.status(Response.Status.UNAUTHORIZED).entity(RESTUtils.createFailedResponseJson("session_id missing in headers")).build();
            }
        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, null, e);
            Throwable t = ExceptionUtils.getRootCause(e);
            ExceptionUtils.logExceptionStackTrace(t);
            String msg = ExceptionUtils.getExceptionMessage(t,"API 'logout' failed");
            JsonReader jr = Json.createReader(new StringReader(msg));
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(RESTUtils.createFailedResponseJson(jr.readObject().toString())).build();
        } 
    }
    
    @GET
    @Path("/validate/sessionid")
    public Response validateSessionId(@HeaderParam("session_id") String sessionId) {
        try {
            sessionId = getSessionId();
            if(sessionId!=null && !sessionId.trim().isEmpty()) {
                if(sessionMap.containsKey(sessionId)){
                    SessionData sd=sessionMap.get(sessionId);
                    if(sd.getValidTill().after(new Date())){
                        return Response.status(Response.Status.OK).entity(RESTUtils.createSuccessResponseJson("sessionid is valid")).build();
                    }else{
                        sessionMap.remove(sessionId);
                        LOGGER.log(Level.INFO, String.format("session map count: %1$s", sessionMap.size()));
                        return Response.status(Response.Status.UNAUTHORIZED).entity(RESTUtils.createFailedResponseJson("session expired")).build();
                    }
                }else{
                    return Response.status(Response.Status.UNAUTHORIZED).entity(RESTUtils.createFailedResponseJson("invalid session id")).build();
                }               
            }else{
                return Response.status(Response.Status.UNAUTHORIZED).entity(RESTUtils.createFailedResponseJson("session_id missing in headers")).build();
            }         
        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, null, e);
            Throwable t = ExceptionUtils.getRootCause(e);
            ExceptionUtils.logExceptionStackTrace(t);
            String msg = ExceptionUtils.getExceptionMessage(t,"API 'logout' failed");
            JsonReader jr = Json.createReader(new StringReader(msg));
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(RESTUtils.createFailedResponseJson(jr.readObject().toString())).build();
        } 
    }
    
    @POST
    @Path("/send/pass-reset-code/byemail")
    public Response sendPasswordResetCode(@FormParam("user_email") String userEmail){
        try {
            if(userEmail==null || userEmail.trim().isEmpty()) {
                throw new RuntimeException("user email missing/invalid in request");
            }
            Map<String,Object> rec = executeSendPasswordResetCode(userEmail);
            if((boolean)rec.get("status")){
                return Response.status(Response.Status.OK).entity(RESTUtils.createSuccessResponseJson(rec.get("message")+"")).build();
            }else{
                return Response.status(Response.Status.BAD_REQUEST).entity(RESTUtils.createFailedResponseJson(rec.get("message")+"")).build();
            }
        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, null, e);
            Throwable t = ExceptionUtils.getRootCause(e);
            ExceptionUtils.logExceptionStackTrace(t);
            String msg = ExceptionUtils.getExceptionMessage(t,"API 'send password reset code by email' failed");
            JsonReader jr = Json.createReader(new StringReader(msg));
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(RESTUtils.createFailedResponseJson(jr.readObject().toString())).build();
        }
    }
    
    
    @POST
    @Path("/reset-pass")
    public Response resetPassword(@FormParam("user_email") String userEmail,@FormParam("reset_code") String resetCode,@FormParam("new_pass") String newPass){
        try {
            if(userEmail==null || userEmail.trim().isEmpty()) {
                throw new RuntimeException("user email missing/invalid in request");
            }
            if(resetCode==null || resetCode.trim().isEmpty()) {
                throw new RuntimeException("password reset code missing/invalid in request");
            }
            if(newPass==null || newPass.trim().isEmpty()) {
                throw new RuntimeException("new password missing/invalid in request");
            }
            Map<String,Object> rec = executeResetPassword(userEmail,resetCode,newPass);
            if((boolean)rec.get("status")){
                return Response.status(Response.Status.OK).entity(RESTUtils.createSuccessResponseJson(rec.get("message")+"")).build();
            }else{
                return Response.status(Response.Status.BAD_REQUEST).entity(RESTUtils.createFailedResponseJson(rec.get("message")+"")).build();
            }
        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, null, e);
            Throwable t = ExceptionUtils.getRootCause(e);
            ExceptionUtils.logExceptionStackTrace(t);
            String msg = ExceptionUtils.getExceptionMessage(t,"API 'reset password' failed");
            JsonReader jr = Json.createReader(new StringReader(msg));
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(RESTUtils.createFailedResponseJson(jr.readObject().toString())).build();
        }
    
    }
    
}
