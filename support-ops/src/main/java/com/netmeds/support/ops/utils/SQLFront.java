/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.netmeds.support.ops.utils;

import com.netmeds.support.ops.Environments;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.Spliterators;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.logging.Level;
import java.util.logging.Logger;
import static java.util.stream.Collectors.toList;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

/**
 *
 * @author prem
 */
public class SQLFront {

    private static final Logger logger=Logger.getLogger(SQLFront.class.getName());

    private static Function<String,Connection> connectionFactory=SQLFront::getConnection;

    public static Connection getAlternateConnection(String targetSchema) {
        try {
            switch(targetSchema.toLowerCase()) {
                default: throw new RuntimeException(MessageFormat.format("unknown target:{0} passed!", targetSchema));
            }
        }
        catch(Exception e) {
            throw new RuntimeException(e);
        }
    }

    private static Connection getConnection(String targetSchema) {
        try {
            switch(targetSchema.toLowerCase()) {
                case "sptops": return getJNDIConnection("sptOps");
                default: throw new RuntimeException(MessageFormat.format("unknown target:{0} passed!", targetSchema));
            }
        }
        catch(SQLException | NamingException e) {
            throw new RuntimeException(e);
        }
    }   

    public static Connection getJNDIConnection(String name) throws NamingException, SQLException {
        InitialContext ctx;
        DataSource ds;

        ctx=new InitialContext();
        ds=(DataSource)ctx.lookup("java:/comp/env/jdbc/"+name);

        return ds.getConnection();
    }

    private static Connection getConnection(String driver, String url, String userid, String passwd) throws Exception {
        Class.forName(driver);
        return DriverManager.getConnection(url, userid, passwd);
    }

    public static void alternateConnectionFactory() {
        connectionFactory=SQLFront::getAlternateConnection;
    }

    public static void setConnectionFactory(Function<String,Connection> conFac) {
        connectionFactory=conFac;
    }
    
    public static SQLFront newSptOps() throws Exception {
        return new SQLFront("com.mysql.jdbc.Driver", "jdbc:mysql://localhost:3306/support_ops?zeroDateTimeBehavior=convertToNull&rewriteBatchedStatements=true", "root", "root");
    }
    
    public static SQLFront jiomartMStarPrd() throws Exception {
        return Environments.JiomartProd.getMstarSF();
//        return new SQLFront("com.mysql.jdbc.Driver", "jdbc:mysql://p-jiomart-mstar.cluster-cip33pywvytw.ap-south-1.rds.amazonaws.com:3306/p_jiomart_mstar?zeroDateTimeBehavior=convertToNull&rewriteBatchedStatements=true", "muthu", "70a683e5ae7898e717dec7d7d57eff75");
    }
    
    public static SQLFront jiomartTetrapodPrd() throws Exception {
        return Environments.JiomartProd.getTpodSF();
//        return new SQLFront("com.mysql.jdbc.Driver","jdbc:mysql://p-jiomart-mstar.cluster-ro-cip33pywvytw.ap-south-1.rds.amazonaws.com:3306/p_jiomart_tetrapod?zeroDateTimeBehavior=convertToNull&rewriteBa;chedStatements=true", "muthu", "70a683e5ae7898e717dec7d7d57eff75");
    }
    public static SQLFront jiomartTetrapodLogPrd() throws Exception {
        return Environments.JiomartProd.getTpodLogSF();
//        return new SQLFront("com.mysql.jdbc.Driver","jdbc:mysql://p-jiomart-logs.cluster-ro-cip33pywvytw.ap-south-1.rds.amazonaws.com:3306/p_jiomart_tetrapod_log?zeroDateTimeBehavior=convertToNull&rewriteBatchedStatements=true", "muthu", "70a683e5ae7898e717dec7d7d57eff75");
    } 
    
    public static SQLFront jiomartMStarStg() throws Exception {
        return Environments.JiomartStage.getMstarSF();
    }
    public static SQLFront jiomartTetrapodStg() throws Exception {
        return Environments.JiomartStage.getTpodSF();
    }
    public static SQLFront jiomartTetrapodLogStg() throws Exception {
        return Environments.JiomartStage.getTpodLogSF();
    } 
    
    
    private final Connection con;
    private final Map<String,PreparedStatement> psMap=new HashMap<>();

    public SQLFront(String driver, String url, String userid, String passwd) throws Exception {
        this(getConnection(driver, url, userid, passwd));
    }

    public SQLFront(Connection con) throws Exception {
        this.con=con;
    }

    public void close() {
        try { con.close(); }
        catch(SQLException sqle) {
            logger.log(Level.WARNING, "thrown SQLException while closing..", sqle);
        }
    }

    public Stream<ResultSet> getRSStreamForQuery(String query) throws SQLException {
        Statement stmt;
        ResultSet rs;
        Iterator<ResultSet> iterator;

        stmt=con.createStatement(java.sql.ResultSet.TYPE_FORWARD_ONLY,java.sql.ResultSet.CONCUR_READ_ONLY);
        stmt.setFetchSize(1000);
        rs=stmt.executeQuery(query);

        if(!rs.next()) {
            iterator=new Iterator<ResultSet>() {
                @Override
                public boolean hasNext() {
                    return false;
                }

                @Override
                public ResultSet next() {
                    return null;
                }
            };
        } else {
            iterator=new Iterator<ResultSet>() {
                boolean first=true;

                @Override
                public boolean hasNext() {
                    try {
                        if(first) return true;
                        return !rs.isLast();
                    }
                    catch(SQLException sqle) {
                        throw new RuntimeException(sqle);
                    }
                }

                @Override
                public ResultSet next() {
                    try {
                        if(first) {
                            first=false;
                        } else {
                            if(!rs.next() || rs.isClosed()) {
                                if(!rs.isClosed()) rs.close();
                                throw new NoSuchElementException();
                            }
                        }
                        return rs;
                    }
                    catch(SQLException sqle) {
                        throw new RuntimeException(sqle);
                    }
                }
            };
        }

        return StreamSupport.stream(Spliterators.spliteratorUnknownSize(iterator, 0), false);
    }

    public Stream<Map<String,Object>> getStreamForQuery(String query) throws SQLException {
        return getRSStreamForQuery(query).map(SQLFront::mapResultSetToMap);
    }

    public static Map<String,Object> mapResultSetToMap(ResultSet rs) {
        Map<String,Object> map=new LinkedHashMap<>();
        ResultSetMetaData rsmd=null;
        int colcount=-1;
        String colname;
        Object colValue;
        int i;

        try {
            rsmd=rs.getMetaData();
            colcount=rsmd.getColumnCount();

            for(i=1;  i<=colcount; i++) {
                colname=rsmd.getColumnLabel(i);
                colValue=rs.getObject(colname);
                //logger.log(Level.INFO,String.format("colname: %1$s and type: %2$s",colname,rsmd.getColumnType(i)));
                if(rsmd.getColumnType(i) == 91){
                    if(colValue!=null){
                        java.sql.Date sqlDate = rs.getDate(colname);
                        java.util.Date date = new Date(sqlDate.getTime());
                        colValue = date;
                    }
                }
                if(rsmd.getColumnType(i) == 93){
                    if(colValue!=null){
                        Timestamp timestamp = rs.getTimestamp(colname);
                        Date date = new Date(timestamp.getTime());
                        colValue = date;
                    }
                }
                map.put(colname, colValue);
            }

            return map;
        }
        catch(SQLException sqle) {
            throw new RuntimeException(sqle);
        }
    }

    public static List<Object> mapResultSetToList(ResultSet rs) {
        List<Object> list=new ArrayList<>();
        ResultSetMetaData rsmd=null;
        int colcount=-1;
        int i;

        try {
            rsmd=rs.getMetaData();
            colcount=rsmd.getColumnCount();

            for(i=1;  i<=colcount; i++) {
                list.add(rs.getObject(i));
            }

            return list;
        }
        catch(SQLException sqle) {
            throw new RuntimeException(sqle);
        }
    }

    public List<Map<String, Object>> getResult(String sql) throws SQLException {
        return getStreamForQuery(sql).collect(toList());
    }

    public List<List<Object>> getResultAsList(String sql) throws SQLException {
        return getRSStreamForQuery(sql).map(SQLFront::mapResultSetToList).collect(toList());
    }

    public void processResult(String sql, Consumer<Map<String,Object>> consumer) throws SQLException {
        getStreamForQuery(sql).forEach(consumer);
    }

    public void insert(String tablename, Map<String,Object> recordMap) throws SQLException {
        insert(tablename, recordMap, recordMap.keySet());
    }

    public int insert(String tablename, Map<String,Object> recordMap, Set<String> colnameSet) throws SQLException {
        PreparedStatement ps;
        int idx=0;
        int result;
        String[] colnamesArray=colnameSet.toArray(new String[colnameSet.size()]);

        ps=getPSForInsert(tablename, colnamesArray);

        // set parameters
        for(String cn : colnamesArray) {
            idx++;
            ps.setObject(idx, recordMap.get(cn));
        }

        // execute.
        result=ps.executeUpdate();
        //logger.log(Level.INFO, "insert: executeUpdate returned:{0}", result);

        // clear parameters
        ps.clearParameters();

        return result;
    }

    private PreparedStatement getPSForInsert(String tablename, String[] colnamesArray) throws SQLException {
        StringBuilder sb=new StringBuilder();
        Consumer<String> builder=(s) -> { sb.append(s); sb.append("|"); };
        String key;
        String insertQry;
        PreparedStatement ps;
        boolean first=true;

        // make key
        builder.accept("INSERT");
        builder.accept(tablename);
        for(String cn : colnamesArray) {
            builder.accept(cn);
        }
        key=sb.toString();

        // return if found in cache
        ps=psMap.get(key);
        if(ps!=null) {
            return psMap.get(key);
        }

        // make insert statement
        sb.setLength(0);
        sb.append("INSERT INTO ");
        sb.append(tablename);
        sb.append(" (");
        for(String cn : colnamesArray) {
            if(first) {
                first=false;
                sb.append(cn);
            } else {
                sb.append(", ");
                sb.append(cn);
            }
        }
        sb.append(") VALUES (");
        first=true;
        for(String cn : colnamesArray) {
            if(first) {
                first=false;
                sb.append("?");
            } else {
                sb.append(", ?");
            }
        }
        sb.append(")");
        insertQry=sb.toString();
        logger.log(Level.INFO, "prepared SQLquery:{0}", insertQry);

        // prepare the statement
        ps=con.prepareStatement(insertQry);
        psMap.put(key, ps);

        return ps;
    }

    public int update(String tablename, Map<String,Object> valuesMap, Map<String,Object> constraintMap) throws SQLException {
        StringBuilder sb=new StringBuilder();
        Set<String> condColSet=constraintMap.keySet();
        List<Object> cvList=new ArrayList<>();
        boolean first=true;

        for(String cn : condColSet) {
            if(first) {
                first=false;
            } else {
                sb.append(" AND ");
            }
            sb.append(cn);
            sb.append("=?");

            cvList.add(constraintMap.get(cn));
        }

        return update(tablename, valuesMap, sb.toString(), cvList);
    }

    public int update(String tablename, Map<String,Object> valuesMap, String whereCondition) throws SQLException {
        return update(tablename, valuesMap, whereCondition, null);
    }

    public int update(String tablename, Map<String,Object> valuesMap, String whereCondition, List<Object> constraintValueList) throws SQLException {
        Set<String> colnameSet=valuesMap.keySet();
        String wc=whereCondition;
        PreparedStatement ps;
        int idx=0;
        int result;

        ps=getPSForUpdate(tablename, colnameSet, wc);

        // set parameters
        for(String cn : colnameSet) {
            idx++;
            ps.setObject(idx, valuesMap.get(cn));
        }
        if(whereCondition!=null && constraintValueList!=null) {
            for(Object val : constraintValueList) {
                idx++;
                ps.setObject(idx, val);
            }
        }

        // execute.
        result=ps.executeUpdate();
        logger.log(Level.INFO, "update: executeUpdate returned:{0}", result);

        // clear parameters
        ps.clearParameters();

        return result;
    }

    private PreparedStatement getPSForUpdate(String tablename, Set<String> colnameSet, String whereCondition) throws SQLException {
        StringBuilder sb=new StringBuilder();
        Consumer<String> builder=(s) -> { sb.append(s); sb.append("|"); };
        String key;
        String updateQry;
        PreparedStatement ps;
        boolean first=true;

        // make key
        builder.accept("UPDATE");
        builder.accept(tablename);
        for(String cn : colnameSet) {
            builder.accept(cn);
        }
        builder.accept(whereCondition);
        key=sb.toString();

        // return if found in cache
        ps=psMap.get(key);
        if(ps!=null) {
            return psMap.get(key);
        }

        // make update statement
        sb.setLength(0);
        sb.append("UPDATE ");
        sb.append(tablename);
        sb.append(" SET ");
        for(String cn : colnameSet) {
            if(first) {
                first=false;
            } else {
                sb.append(", ");
            }

            sb.append(cn);
            sb.append("=?");
        }
        if(whereCondition!=null && !whereCondition.trim().isEmpty()) {
            sb.append(" WHERE ");
            sb.append(whereCondition);
        }
        updateQry=sb.toString();

        // prepare the statement
        ps=con.prepareStatement(updateQry);
        psMap.put(key, ps);
        logger.log(Level.INFO, "prepared SQLquery:{0}", updateQry);

        return ps;
    }

    public int delete(String tablename, Map<String,Object> constraintMap) throws SQLException {
        StringBuilder sb=new StringBuilder();
        Set<String> condColSet=constraintMap.keySet();
        List<Object> cvList=new ArrayList<>();
        boolean first=true;

        for(String cn : condColSet) {
            if(first) {
                first=false;
            } else {
                sb.append(" AND ");
            }
            sb.append(cn);
            sb.append("=?");

            cvList.add(constraintMap.get(cn));
        }

        return delete(tablename, sb.toString(), cvList);
    }

    public int delete(String tablename, String whereCondition) throws SQLException {
        return delete(tablename, whereCondition, null);
    }

    public int delete(String tablename, String whereCondition, List<Object> constraintValueList) throws SQLException {
        String wc=whereCondition;
        PreparedStatement ps;
        int idx=0;
        int result;

        ps=getPSForDelete(tablename, wc);

        // set parameters
        if(whereCondition!=null && constraintValueList!=null) {
            for(Object val : constraintValueList) {
                idx++;
                ps.setObject(idx, val);
            }
        }

        // execute.
        result=ps.executeUpdate();
        logger.log(Level.INFO, "delete: executeUpdate returned:{0}", result);

        // clear parameters
        ps.clearParameters();

        return result;
    }

    private PreparedStatement getPSForDelete(String tablename, String whereCondition) throws SQLException {
        StringBuilder sb=new StringBuilder();
        Consumer<String> builder=(s) -> { sb.append(s); sb.append("|"); };
        String key;
        String deleteQuery;
        PreparedStatement ps;

        // make key
        builder.accept("DELETE");
        builder.accept(tablename);
        builder.accept(whereCondition);
        key=sb.toString();

        // return if found in cache
        ps=psMap.get(key);
        if(ps!=null) {
            return psMap.get(key);
        }

        // make update statement
        sb.setLength(0);
        sb.append("DELETE FROM ");
        sb.append(tablename);
        sb.append(" WHERE ");
        sb.append(whereCondition);

        deleteQuery=sb.toString();

        // prepare the statement
        ps=con.prepareStatement(deleteQuery);
        psMap.put(key, ps);
        logger.log(Level.INFO, "prepared SQLquery:{0}", deleteQuery);

        return ps;
    }

    public Connection getConnection() {
        return con;
    }

    public void startTransaction() throws SQLException {
        if(con.getAutoCommit()) {
            con.setAutoCommit(false);
            ctr=0;
        }
    }

    public void commit() throws SQLException {
        con.commit();
    }

    public void rollback() throws SQLException {
        con.rollback();
    }

    public void stopTransactional() throws SQLException {
        if(!con.getAutoCommit()) {
            con.setAutoCommit(true);
        }
    }

    int ctr=0;

    public void incrRecordCounterForBatch(int batchsize) throws SQLException {
        ctr++;
        if(ctr>=batchsize) {
            ctr=0;
            commit();
        }
    }
    
    public Map<String,Object> getTableColumnDetails(String sql){
        Map<String,Object> recMap = null;
        try{
            String colLabel,colType;
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            ResultSetMetaData rsmd =  rs.getMetaData();
            
            recMap = new HashMap<>();
            for (int i = 1; i < rsmd.getColumnCount() + 1; i++) {
                colLabel = rsmd.getColumnLabel(i);
                colType = rsmd.getColumnTypeName(i);
                logger.log(Level.INFO, String.format("colLabel:%1$s, colType:%2$s,",colLabel,colType));
                recMap.put(colLabel,colType);               
            }
        } catch (Exception e) {
            logger.log(Level.INFO, "exception occurred!", e);
        }        
        return recMap;  
    }
    
    public static List<Map<String,Object>> getListOfRecordFromRS(String jndiConName, String sql){
        List<Map<String,Object>> recList = null;
        Map<String,Object> recMap;
       
        try(Connection con = SQLFront.getJNDIConnection(jndiConName)) {
        //try(Connection con = SQLFront.getAlternateConnection(jndiConName)) {
            String colLabel;
            Object colValue;
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            ResultSetMetaData rsmd =  rs.getMetaData();
            
            recList = new ArrayList<>();
            while(rs.next()){
                recMap = new HashMap<>();
                for (int i = 1; i < rsmd.getColumnCount() + 1; i++) {
                    colLabel = rsmd.getColumnLabel(i);
                    if(rsmd.getColumnType(i) == 93){
                        if(rs.getTimestamp(colLabel)!= null){
                            Timestamp timestamp = rs.getTimestamp(colLabel);
                            Date date = new Date(timestamp.getTime());
                            colValue = date;
                            recMap.put(colLabel, colValue);
                        }else{
                            colValue = null;
                            recMap.put(colLabel, colValue);
                        }                        
                    }else{
                        colValue =  rs.getObject(colLabel);
                        recMap.put(colLabel, colValue);                    
                    }                    
                }
                recList.add(recMap);
            }
            con.close();
        } catch (Exception e) {
            logger.log(Level.INFO, "exception occurred!", e);
        }        
        return recList;   
    }
    
    public static <K,V> Map<K,V> seqMapOf(Tuple2<K,V>... tupArray) {
        Map<K,V> map=new LinkedHashMap<K,V>();

        for(Tuple2<K,V> t : tupArray) {
            map.put(t._1, t._2);
        }

        return map;
    }
    
    public static class Tuple2<T1,T2> {
        public T1 _1;
        public T2 _2;

        public Tuple2() {
        }

        public Tuple2(T1 _1, T2 _2) {
            this._1 = _1;
            this._2 = _2;
        }
    }
    public static Tuple2<String,Object> kv(String k, Object v) {
        Tuple2<String,Object> t=new Tuple2();
        t._1=k;
        t._2=v;
        return t;
    }
}
