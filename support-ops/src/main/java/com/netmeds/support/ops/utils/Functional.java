/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.netmeds.support.ops.utils;

import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.IntConsumer;

/**
 *
 * @author prem
 */
public interface Functional {

    @FunctionalInterface
    public static interface CheckedFunction<T,R> {
        public R apply(T t) throws Exception;
    }

    public static <T,R> Function<T,R> uncheckedFunction(CheckedFunction<T,R> cf) {
        return t -> {
            try {
                return cf.apply(t);
            } catch(Exception e) {
                throw new RuntimeException(e);
            }
        };
    }

    @FunctionalInterface
    public static interface CheckedConsumer<T> {
        public void accept(T t) throws Exception;
    }

    public static <T> Consumer<T> uncheckedConsumer(CheckedConsumer<T> cs) {
        return t -> {
            try {
                cs.accept(t);
            } catch(Exception e) {
                throw new RuntimeException(e);
            }
        };
    }

    @FunctionalInterface
    public static interface CheckedBiConsumer<T,U> {
        public void accept(T t, U u) throws Exception;
    }

    public static <T,U> BiConsumer<T,U> uncheckedBiConsumer(CheckedBiConsumer<T,U> cs) {
        return (t,u) -> {
            try {
                cs.accept(t, u);
            } catch(Exception e) {
                throw new RuntimeException(e);
            }
        };
    }

    @FunctionalInterface
    public static interface CheckedSubroutine {
        public void go() throws Exception;
    }

    @FunctionalInterface
    public static interface Subroutine {
        public void go();
    }

    public static Subroutine uncheckedSubroutine(CheckedSubroutine cs) {
        return () ->{
            try { cs.go(); }
            catch(Exception e) {
                throw new RuntimeException(e);
            }
        };
    }

    @FunctionalInterface
    public static interface CheckedIntConsumer {
        public void accept(int i) throws Exception;
    }

    public static IntConsumer uncheckedIntConsumer(CheckedIntConsumer cs) {
        return t -> {
            try {
                cs.accept(t);
            } catch(Exception e) {
                throw new RuntimeException(e);
            }
        };
    }

    @FunctionalInterface
    public static interface TriFunction<P1,P2,P3,R> {
        public R apply(P1 p1, P2 p2, P3 p3);
    }

    public static class Tuple1<T> {
        public T _1;
    }

    public static class Tuple2<T1,T2> {
        public T1 _1;
        public T2 _2;
    }
    
    public static class Tuple5<T1,T2,T3,T4,T5> {
        public T1 _1;
        public T2 _2;
        public T3 _3;
        public T4 _4;
        public T5 _5;

        public Tuple5() {
        }

        public Tuple5(T1 _1, T2 _2, T3 _3, T4 _4, T5 _5) {
            this._1 = _1;
            this._2 = _2;
            this._3 = _3;
            this._4 = _4;
            this._5 = _5;
        }
    }

    public static <T1,T2,T3,T4,T5> Tuple5<T1,T2,T3,T4,T5> tup(T1 v1, T2 v2, T3 v3, T4 v4, T5 v5) {
        Tuple5<T1,T2,T3,T4,T5> t=new Tuple5();
        t._1=v1;
        t._2=v2;
        t._3=v3;
        t._4=v4;
        t._5=v5;
        return t;
    }
}
