/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.netmeds.support.ops.web;

import com.netmeds.support.ops.Environments;
import com.netmeds.support.ops.mstar.service.MStarService;
import com.netmeds.support.ops.utils.ExceptionUtils;
import com.netmeds.support.ops.utils.JsonUtils;
import com.netmeds.support.ops.utils.OrderIdNotFoundException;
import com.netmeds.support.ops.utils.RESTUtils;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonValue;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;


/**
 *
 * @author Admin
 */
@Path("/mstar")
public class MstarREST {
    private static final Logger LOGGER = Logger.getLogger(MstarREST.class.getName());
    
    @GET
    @Path("/get-fynd-req-res")
    public Response getFyndReqRes(@QueryParam("environment") String envName, @QueryParam("order_id") String order_id){
        try{
            List<Map<String,Object>> response = MStarService.getResponseByOrderId(envName, order_id);
            return Response.status(Response.Status.OK).entity(RESTUtils.createSuccessResponseJson(RESTUtils.convertListToJson(response))).build();
        }catch(OrderIdNotFoundException oe){
            LOGGER.log(Level.SEVERE, null, oe);
            Throwable t = ExceptionUtils.getRootCause(oe);
            ExceptionUtils.logExceptionStackTrace(t);
            String msg = ExceptionUtils.getExceptionMessage(t,"API get-fynd-req-res failed!");
            return Response.status(Response.Status.BAD_REQUEST).entity(RESTUtils.createFailedResponseJson(msg)).build();
        }catch(Exception e){
            LOGGER.log(Level.SEVERE, null, e);
            Throwable t = ExceptionUtils.getRootCause(e);
            ExceptionUtils.logExceptionStackTrace(t);
            String msg = ExceptionUtils.getExceptionMessage(t,"API get-fynd-req-res failed!");
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(RESTUtils.createFailedResponseJson(msg)).build();
        }
    }
    
    @GET
    @Path("/get-environments")
    public Response getEnvironments(){
        try{
            JsonArrayBuilder jsonArrB = Json.createArrayBuilder();
            JsonObjectBuilder jsonObjB;
            for (Environments env : Environments.values()) {
                jsonObjB = Json.createObjectBuilder();
                jsonObjB.add("environment_name", env.getDisplayName());
                jsonObjB.add("tetrapod_url", env.getTetrapodURL());
                jsonArrB.add(jsonObjB.build());
            }
            return Response.status(Response.Status.OK).entity(RESTUtils.createSuccessResponseJson(jsonArrB.build())).build();
        }catch(Exception e){
            LOGGER.log(Level.SEVERE, null, e);
            Throwable t = ExceptionUtils.getRootCause(e);
            ExceptionUtils.logExceptionStackTrace(t);
            String msg = ExceptionUtils.getExceptionMessage(t,"API get 'get-environments' failed!");
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(RESTUtils.createFailedResponseJson(msg)).build();
        } 
    }
    
    @GET
    @Path("/get-customer")
    public Response getCustomer(@QueryParam("environment") String envName,@QueryParam("mobile_no") String mobileNo,
            @QueryParam("email") String email,@QueryParam("customer_id") String custId){
        try{
            List<Map<String,Object>> customerList = MStarService.getCustomerList(envName, mobileNo, email, custId);
            return Response.status(Response.Status.OK).entity(RESTUtils.createSuccessResponseJson(RESTUtils.convertListToJson(customerList))).build();
        }catch(Exception e){
            LOGGER.log(Level.SEVERE, null, e);
            Throwable t = ExceptionUtils.getRootCause(e);
            ExceptionUtils.logExceptionStackTrace(t);
            String msg = ExceptionUtils.getExceptionMessage(t,"API get 'mstar customer' failed!");
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(RESTUtils.createFailedResponseJson(msg)).build();
        } 
    }
    
    @GET
    @Path("/get-cart")
    public Response getCart(@QueryParam("environment") String envName,@QueryParam("order_id") String orderId,
            @QueryParam("cart_id") String cartId,@QueryParam("customer_id") String custId,@QueryParam("search_by_order_id_history") boolean searchByOrderIdHistory){
        try{
            List<Map<String,Object>> cartList = MStarService.getCartList(envName, orderId, cartId, custId, searchByOrderIdHistory);
            return Response.status(Response.Status.OK).entity(RESTUtils.createSuccessResponseJson(RESTUtils.convertListToJson(cartList))).build();
        }catch(Exception e){
            LOGGER.log(Level.SEVERE, null, e);
            Throwable t = ExceptionUtils.getRootCause(e);
            ExceptionUtils.logExceptionStackTrace(t);
            String msg = ExceptionUtils.getExceptionMessage(t,"API get 'mstar cart' failed!");
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(RESTUtils.createFailedResponseJson(msg)).build();
        } 
    }
    
    @GET
    @Path("/get-sessions")
    public Response getSessions(@QueryParam("environment") String envName,@QueryParam("customer_id") String custId){
        try{
            List<Map<String,Object>> cartList = MStarService.getSessions(envName, Integer.parseInt(custId));
            return Response.status(Response.Status.OK).entity(RESTUtils.createSuccessResponseJson(RESTUtils.convertListToJson(cartList))).build();
        }catch(Exception e){
            LOGGER.log(Level.SEVERE, null, e);
            Throwable t = ExceptionUtils.getRootCause(e);
            ExceptionUtils.logExceptionStackTrace(t);
            String msg = ExceptionUtils.getExceptionMessage(t,"API get 'mstar sessions' failed!");
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(RESTUtils.createFailedResponseJson(msg)).build();
        } 
    }
    
    @GET
    @Path("/get-addresses")
    public Response getAddresses(@QueryParam("environment") String envName,@QueryParam("customer_id") String custId){
        try{
            List<Map<String,Object>> cartList = MStarService.getAddresses(envName, Integer.parseInt(custId));
            return Response.status(Response.Status.OK).entity(RESTUtils.createSuccessResponseJson(RESTUtils.convertListToJson(cartList))).build();
        }catch(Exception e){
            LOGGER.log(Level.SEVERE, null, e);
            Throwable t = ExceptionUtils.getRootCause(e);
            ExceptionUtils.logExceptionStackTrace(t);
            String msg = ExceptionUtils.getExceptionMessage(t,"API get 'mstar addresses' failed!");
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(RESTUtils.createFailedResponseJson(msg)).build();
        } 
    }
    
    @GET
    @Path("/get-lines-in-cart")
    public Response getLinesInCart(@QueryParam("environment") String envName,@QueryParam("cart_id") String cartId){
        try{
            List<Map<String,Object>> cartList = MStarService.getCartItems(envName, Integer.parseInt(cartId));
            return Response.status(Response.Status.OK).entity(RESTUtils.createSuccessResponseJson(RESTUtils.convertListToJson(cartList))).build();
        }catch(Exception e){
            LOGGER.log(Level.SEVERE, null, e);
            Throwable t = ExceptionUtils.getRootCause(e);
            ExceptionUtils.logExceptionStackTrace(t);
            String msg = ExceptionUtils.getExceptionMessage(t,"API get 'mstar lines in cart' failed!");
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(RESTUtils.createFailedResponseJson(msg)).build();
        } 
    }
    
    @GET
    @Path("/get-shipping-and-billing-address")
    public Response getShippingAndBillingAddress(@QueryParam("environment") String envName,@QueryParam("cart_id") String cartId){
        try{
            List<Map<String,Object>> addressLi = MStarService.getShippingAndBillingAddress(envName, Integer.parseInt(cartId));
            return Response.status(Response.Status.OK).entity(RESTUtils.createSuccessResponseJson(RESTUtils.convertListToJson(addressLi))).build();
        }catch(Exception e){
            LOGGER.log(Level.SEVERE, null, e);
            Throwable t = ExceptionUtils.getRootCause(e);
            ExceptionUtils.logExceptionStackTrace(t);
            String msg = ExceptionUtils.getExceptionMessage(t,"API get 'shipping/billing address' failed!");
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(RESTUtils.createFailedResponseJson(msg)).build();
        } 
    }
    
    
    
    @GET
    @Path("/get-cart-address-backup")
    public Response getCartAddressBackup(@QueryParam("environment") String envName,@QueryParam("cart_id") String cartId){
        try{
            List<Map<String,Object>> addressLi = MStarService.getCartAddressBackup(envName, Integer.parseInt(cartId));
            return Response.status(Response.Status.OK).entity(RESTUtils.createSuccessResponseJson(RESTUtils.convertListToJson(addressLi))).build();
        }catch(Exception e){
            LOGGER.log(Level.SEVERE, null, e);
            Throwable t = ExceptionUtils.getRootCause(e);
            ExceptionUtils.logExceptionStackTrace(t);
            String msg = ExceptionUtils.getExceptionMessage(t,"API get 'get-cart-address-backup' failed!");
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(RESTUtils.createFailedResponseJson(msg)).build();
        } 
    }
    
    @GET
    @Path("/get-cart-payment-info")
    public Response getCartPaymentInfo(@QueryParam("environment") String envName,@QueryParam("cart_id") String cartId){
        try{
            List<Map<String,Object>> addressLi = MStarService.getCartPaymentInfo(envName, Integer.parseInt(cartId));
            return Response.status(Response.Status.OK).entity(RESTUtils.createSuccessResponseJson(RESTUtils.convertListToJson(addressLi))).build();
        }catch(Exception e){
            LOGGER.log(Level.SEVERE, null, e);
            Throwable t = ExceptionUtils.getRootCause(e);
            ExceptionUtils.logExceptionStackTrace(t);
            String msg = ExceptionUtils.getExceptionMessage(t,"API get 'get-cart-payment-info' failed!");
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(RESTUtils.createFailedResponseJson(msg)).build();
        } 
    }
    
    @GET
    @Path("/get-order-id-history")
    public Response getOrderIdHistory(@QueryParam("environment") String envName,@QueryParam("cart_id") String cartId,@QueryParam("order_id") String orderId){
        try{
            if(cartId==null && orderId==null)return Response.status(Response.Status.BAD_REQUEST).entity("invalid cart id or order id!").build();
            List<Map<String,Object>> orderIdHisLi = MStarService.getOrderIdHistory(envName,cartId,orderId);
            return Response.status(Response.Status.OK).entity(RESTUtils.createSuccessResponseJson(RESTUtils.convertListToJson(orderIdHisLi))).build();
        }catch(Exception e){
            LOGGER.log(Level.SEVERE, null, e);
            Throwable t = ExceptionUtils.getRootCause(e);
            ExceptionUtils.logExceptionStackTrace(t);
            String msg = ExceptionUtils.getExceptionMessage(t,"API get 'get-order-id-history' failed!");
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(RESTUtils.createFailedResponseJson(msg)).build();
        } 
    }
    
    @GET
    @Path("/get-payment-history")
    public Response getPaymentHistory(@QueryParam("environment") String envName,@QueryParam("cart_id") String cartId){
        try{
            List<Map<String,Object>> orderIdHisLi = MStarService.getPaymentHistory(envName, Integer.parseInt(cartId));
            return Response.status(Response.Status.OK).entity(RESTUtils.createSuccessResponseJson(RESTUtils.convertListToJson(orderIdHisLi))).build();
        }catch(Exception e){
            LOGGER.log(Level.SEVERE, null, e);
            Throwable t = ExceptionUtils.getRootCause(e);
            ExceptionUtils.logExceptionStackTrace(t);
            String msg = ExceptionUtils.getExceptionMessage(t,"API get 'get-payment-history' failed!");
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(RESTUtils.createFailedResponseJson(msg)).build();
        } 
    }
    
    @GET
    @Path("/get-juspay-webhook")
    public Response getJuspayWebhook(@QueryParam("environment") String envName,@QueryParam("cart_id") String cartId){
        try{
            List<Map<String,Object>> orderIdHisLi = MStarService.getJuspayWebhookList(envName, Integer.parseInt(cartId));
            return Response.status(Response.Status.OK).entity(RESTUtils.createSuccessResponseJson(RESTUtils.convertListToJson(orderIdHisLi))).build();
        }catch(Exception e){
            LOGGER.log(Level.SEVERE, null, e);
            Throwable t = ExceptionUtils.getRootCause(e);
            ExceptionUtils.logExceptionStackTrace(t);
            String msg = ExceptionUtils.getExceptionMessage(t,"API get 'get-juspay-webhook' failed!");
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(RESTUtils.createFailedResponseJson(msg)).build();
        } 
    }
    
    @GET
    @Path("/get-myjio-webhook")
    public Response getMyJioWebhook(@QueryParam("environment") String envName,@QueryParam("cart_id") String cartId){
        try{
            List<Map<String,Object>> orderIdHisLi = MStarService.getMyJioWebhookList(envName, Integer.parseInt(cartId));
            return Response.status(Response.Status.OK).entity(RESTUtils.createSuccessResponseJson(RESTUtils.convertListToJson(orderIdHisLi))).build();
        }catch(Exception e){
            LOGGER.log(Level.SEVERE, null, e);
            Throwable t = ExceptionUtils.getRootCause(e);
            ExceptionUtils.logExceptionStackTrace(t);
            String msg = ExceptionUtils.getExceptionMessage(t,"API get 'get-myjio-webhook' failed!");
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(RESTUtils.createFailedResponseJson(msg)).build();
        } 
    }
    
    @POST
    @Path("/customer-block")
    public Response customerBlock(@QueryParam("environment") String envName,JsonObject jsonObj){
        try{
            int custId = (int)JsonUtils.convertJsonValueToObject(jsonObj, "customer_id", Arrays.asList(JsonValue.ValueType.NUMBER));
            String reason = (String)JsonUtils.convertJsonValueToObject(jsonObj, "block_reason", Arrays.asList(JsonValue.ValueType.STRING));
            
            MStarService.customerBlock(envName, custId, reason);
            return Response.status(Response.Status.OK).entity(RESTUtils.createSuccessResponseJson("given customer has been blocked successfully!")).build();
        }catch(Exception e){
            LOGGER.log(Level.SEVERE, null, e);
            Throwable t = ExceptionUtils.getRootCause(e);
            ExceptionUtils.logExceptionStackTrace(t);
            String msg = ExceptionUtils.getExceptionMessage(t,"API get 'customer-block' failed!");
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(RESTUtils.createFailedResponseJson(msg)).build();
        } 
    }
    
    private static Map<String,String> getHeaders(HttpServletRequest request){
        Map<String,String> result = new HashMap();
        Enumeration headerNames = request.getHeaderNames();
        while(headerNames.hasMoreElements()){
            String key = (String)headerNames.nextElement();
            String value = request.getHeader(key);
            result.put(key,value);
        }
        return result;
    }
    
    /*
    @GET
    @Path("/get/smart-recon-report")
    //@Produces({MediaType.APPLICATION_OCTET_STREAM, MediaType.TEXT_PLAIN, MediaType.APPLICATION_JSON})
    public Response getSmartReconReport(@Context HttpServletRequest request,@Context HttpServletResponse response,@QueryParam("start_time") String startTimeStr,
            @QueryParam("end_time") String endTimeStr){
        try {
            String clientIp = request.getRemoteAddr();
            LOGGER.log(Level.INFO, String.format("report requested from client ip: %1$s", clientIp));
            if(startTimeStr==null || endTimeStr == null)return Response.status(Response.Status.BAD_REQUEST).entity("missing query string start_time/end_time in request!").build();
            LocalDateTime startTime, endTime;
            try {
                startTime = DateUtils.parseLocalDateTime(startTimeStr);
                endTime = DateUtils.parseLocalDateTime(endTimeStr);
            } catch (Exception e) {
                return Response.status(Response.Status.BAD_REQUEST).entity("invalid start_time/end_time format! required format:yyyy-mm-dd HH:mm:ss").build();
            }
            if(endTime.isBefore(startTime))
                return Response.status(Response.Status.BAD_REQUEST).entity("end_time should be grater than start_time!").build();
            
            Map<String,Object> validateReqRec = MStarService.validateSmartReconRequest(clientIp,startTime,endTime);
            if((boolean)validateReqRec.get("status")){
               
                //setting the content disposition
                response.setHeader("Content-Disposition", "attachment; filename=" + "SMART_Reconciliation_"+System.currentTimeMillis()+".csv");
                //marking content type as csv
                response.setContentType("text/csv");
                //retrieving output stream writer from the response
                OutputStreamWriter osw = new OutputStreamWriter(response.getOutputStream(), "UTF-8");

                //Initializing Apache Commons Csv format

                //Create the CSVFormat object with "\n" as a record delimiter
                CSVFormat csvFileFormat = CSVFormat.DEFAULT.withDelimiter(',').withRecordSeparator("\n");

                //Initializing Apache Commons Csv printer

                CSVPrinter csvFilePrinter = new CSVPrinter(osw, csvFileFormat);

                List<Map<String,Object>> reportList = HomeRSMartOrdersrecon.getReconciliationRecList("MS_ORDER_CREATE", startTimeStr, endTimeStr);
                if(reportList.size()>0){
                    //printing header to the csv file
                    csvFilePrinter.printRecord(String.format("recon for time window: '%1$s' to '%2$s'", startTimeStr,endTimeStr));
                    csvFilePrinter.printRecord(String.format("report generated at:%1$s", LocalDateTime.now().format(DateTimeFormatter.ISO_DATE_TIME)));
                    csvFilePrinter.printRecord(reportList.get(0).keySet());
                    
                    for (int i = 0; i < reportList.size(); i++) {
                        Map<String, Object> reportRec = reportList.get(i);
                        List<String> row = new ArrayList();
                        for (String key : reportRec.keySet()) {
                            row.add(reportRec.get(key)+"");
                        }
                        csvFilePrinter.printRecord(row);
                    }
                }
                csvFilePrinter.flush();
                csvFilePrinter.close();

                osw.flush();
                osw.close();
                
                return Response.ok().build();
            }else{
                String message = validateReqRec.get("message")+"";
                return Response.status(Response.Status.BAD_REQUEST).entity(message).build();
            }            
        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, null, e);
            Throwable t = ExceptionUtils.getRootCause(e);
            ExceptionUtils.logExceptionStackTrace(t);
            String msg = ExceptionUtils.getExceptionMessage(t,"API get '/get/smart-recon-report' failed!");
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(RESTUtils.createFailedResponseJson(msg)).build();
        }
    }
    
    @POST
    @Path("/smart_order_analysis")
    public Response getSmartOrderAnalysis(JsonObject jsonObj){
        try {
            JsonArray orderIdsJosnArr = JsonUtils.getJsonArray(jsonObj, "order_ids");
            List<String> orderIdLi = new LinkedList();
            for (int i = 0; i < orderIdsJosnArr.size(); i++) {
                String orderId = orderIdsJosnArr.getString(i);
                orderIdLi.add(orderId);
            }
            List<Map<String,Object>> orderAnalysisRecLi = HomeRSMartOrdersrecon.getSmartOrderAnalysisRecordList(orderIdLi);
            return Response.status(Response.Status.OK).entity(RESTUtils.createSuccessResponseJson(RESTUtils.convertListToJson(orderAnalysisRecLi))).build();
        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, null, e);
            Throwable t = ExceptionUtils.getRootCause(e);
            ExceptionUtils.logExceptionStackTrace(t);
            String msg = ExceptionUtils.getExceptionMessage(t,"API '/smart_order_analysis' failed!");
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(RESTUtils.createFailedResponseJson(msg)).build();
        }
    }
    */

}
