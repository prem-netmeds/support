/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.netmeds.support.ops.utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author prem
 */
public class SQLUtils {

    private static final Logger logger=Logger.getLogger(SQLUtils.class.getName());

    private static boolean v2=false;
    private static Connection con=null;

    public static class ResultBlock {
        public ResultBlock(Connection con, Statement stmt, ResultSet rs) {
            this.con = con;
            this.stmt = stmt;
            this.rs = rs;
        }

        Connection con;
        Statement stmt;
        public ResultSet rs;
    }

    public static List<Map<String, Object>> getResult(String sql) throws SQLException {
        SQLUtils.ResultBlock rb;
        List<Map<String,Object>> resultList=new ArrayList<>();
        Map<String, Object> result;
        ResultSetMetaData rsmd=null;
        int colcount=-1;
        int i;
        String colname;

        rb=executeQuery(sql);
        while(rb.rs.next()) {
            result=new HashMap<>();

            if(rsmd==null) {
                rsmd=rb.rs.getMetaData();
                colcount=rsmd.getColumnCount();
            }

            for(i=1;  i<=colcount; i++) {
                colname=rsmd.getColumnLabel(i);
                result.put(colname, rb.rs.getObject(colname));
            }
            resultList.add(result);
        }

        closeResult(rb);

        logger.log(Level.INFO, "------ total {0} record(s) found!", resultList.size());

        return resultList;
    }

    public static void processResult(String sql, Consumer<Map<String,Object>> consumer) throws SQLException {
        SQLUtils.ResultBlock rb;
        //List<Map<String,Object>> resultList=new ArrayList<>();
        Map<String, Object> result;
        ResultSetMetaData rsmd=null;
        int colcount=-1;
        int i;
        String colname;
        int ctr=0;

        rb=executeQuery(sql);
        while(rb.rs.next()) {
            result=new HashMap<>();

            if(rsmd==null) {
                rsmd=rb.rs.getMetaData();
                colcount=rsmd.getColumnCount();
            }

            for(i=1;  i<=colcount; i++) {
                colname=rsmd.getColumnLabel(i);
                result.put(colname, rb.rs.getObject(colname));
            }
            //resultList.add(result);
            consumer.accept(result);
            ctr++;
        }

        closeResult(rb);

        logger.log(Level.INFO, "------ total {0} record(s) found!", ctr);

        //return resultList;
    }

    public static ResultBlock executeQuery(String sql) throws SQLException {
        Connection con=null;
        Statement stmt;

        logger.log(Level.INFO, "sql:{0}", sql);

        try {
            con=getConnection();
            stmt=con.createStatement();
            stmt.executeQuery(sql);

            return new ResultBlock(con, stmt, stmt.getResultSet());
        }
        finally {
            //if(con!=null) {
            //    con.close();
            //}
        }
    }

    public static final boolean FLIPSWITCH_USE_RDS=true;

    public static final String USE230_PROPNAME="dbcon.use230";
    public static final String USEKKT_PROPNAME="dbcon.usekkt";

    public static void setupToUse230() {
        System.setProperty(USE230_PROPNAME, "y");
    }

    public static void setupToUseKKT() {
        System.setProperty(USEKKT_PROPNAME, "y");
    }

    private static String getdbname() {
        String use;

        use=System.getProperty(USE230_PROPNAME, "n");
        if(use.equalsIgnoreCase("y") || use.equalsIgnoreCase("true")) {
            return "230";
        }
        else {
            use=System.getProperty(USEKKT_PROPNAME, "n");
            if(use.equalsIgnoreCase("y") || use.equalsIgnoreCase("true")) {
                return "kkt";
            }
        }

        return "default";
    }

    private static Connection getConnection() {
        if(v2 && con!=null) return con;

        try { Class.forName("com.mysql.jdbc.Driver"); }
        catch(ClassNotFoundException cnfe) {
            logger.log(Level.SEVERE, "mysql drivers not found!");
            return null;
        }
        try {
            String url="jdbc:mysql://nms-stock-master.cwi6ft8tahgz.us-east-1.rds.amazonaws.com:3306/netmeds_bi?zeroDateTimeBehavior=convertToNull";
            String userid="bi_user";
            String passwd="bi_pass";
            String use;

            use=getdbname();
            switch(use) {
                case "230":
                        if(!FLIPSWITCH_USE_RDS) {
                        url="jdbc:mysql://192.168.2.230:3306/netmeds_bi?zeroDateTimeBehavior=convertToNull";
                        userid="wp";
                        passwd="wp";
                        }
                        break;
                case "kkt":
                        url="jdbc:mysql://nms-stock-master.cwi6ft8tahgz.us-east-1.rds.amazonaws.com:3306/nms_stock_master?zeroDateTimeBehavior=convertToNull";
                        userid="nms_stock";
                        passwd="passwd_stock";
                        break;
            }

            con=DriverManager.getConnection(url, userid, passwd);
            return con;
            //return DriverManager.getConnection("jdbc:mysql://localhost:3306/netmeds_bi?zeroDateTimeBehavior=convertToNull", "root", "root");
        }
        catch(SQLException sqle) {
            logger.log(Level.SEVERE, "unable to get connection!", sqle);
            return null;
        }
    }

    public static void closeResult(ResultBlock rb) throws SQLException {
        rb.rs.close();
        rb.stmt.close();
        if(!v2) {
            rb.con.close();
            con=null;
        }
    }

    public static void closeConnection() throws SQLException {
        con.close();
        con=null;
    }

    public static void setV2() {
        v2=true;
    }
}
