/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var ctxpath="/support-ops";
var indexPage="/index.html";
var homePage="/home.html";
var basePath=ctxpath+'/rest';

var targetObj = {"baseUrl":"https://tp2.netmeds.com"};

function clearSessionStorage(){
	sessionStorage.removeItem("user_id");
	sessionStorage.removeItem("session_id");
	sessionStorage.removeItem("user_name");
	sessionStorage.removeItem("roles");
	sessionStorage.removeItem("is_admin");
}

$("#logoutLink").click(function(e) {
        sessionStorage.removeItem("user_id");
        sessionStorage.removeItem("session_id");
        window.location.href=ctxpath+indexPage;
        /*
	$.ajax({
		url: basePath+'/v2/session/logout',
		type: 'GET',
		headers:{session_id:sessionStorage.getItem('session_id')},
		success:function(data){
			clearSessionStorage();
			window.location.href=ctxpath+indexPage;
		},
		error:function(xhr,staus,errorThrown){
			window.location.href=ctxpath+indexPage;		
		}
	});
        */
});


function sessionExists() {
    return (("user_id" in sessionStorage) && ("session_id" in sessionStorage));
}

function ensureNoSession(callback) {
    if(sessionExists()) {
        window.location.href=ctxpath+homePage;
    } else {
        callback();
    }
}

function ensureSession(callback) {
    if(!sessionExists()) {
        window.location.href=ctxpath+indexPage;
    } else {
        callback();
    }
}

/*
function sessionExists() {
	var validSessionState = false;
	$.ajax({
		url: basePath+'/v2/session/validate/sessionid',
		type:'GET',
		headers:{session_id:sessionStorage.getItem('session_id')},
		success:function(data){
			validSessionState = true;
		},
		error:function(xhr,staus,errorThrown){
			validSessionState = false;		
		},
		complete:function(){
			alert('complete '+validSessionState);
			return validSessionState;
		}
	});	
}

function ensureNoSession(callback) {	
    var validSessionState = false;
	$.ajax({
		url: basePath+'/v2/session/validate/sessionid',
		type:'GET',
		headers:{session_id:sessionStorage.getItem('session_id')},
		success:function(data){
			validSessionState = true;
		},
		error:function(xhr,staus,errorThrown){
			validSessionState = false;		
		},
		complete:function(){
			if(validSessionState) {
				window.location.href=ctxpath+homePage;
			} else {
				callback();
			}
		}
	});
}

function ensureSession(callback) {
    var validSessionState = false;
	$.ajax({
		url: basePath+'/v2/session/validate/sessionid',
		type:'GET',
		headers:{session_id:sessionStorage.getItem('session_id')},
		success:function(data){
			validSessionState = true;
		},
		error:function(xhr,staus,errorThrown){
			validSessionState = false;		
		},
		complete:function(){
			if(!validSessionState) {
				window.location.href=ctxpath+indexPage;
			} else {
				callback();
			}
		}
	});
	
}
*/

function showAlert(type, message) {
    var html="";

    html+="<div class='alert alert-"+type+" alert-dismissable' role='alert'>";
    html+="    <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>";
    html+=message;
    html+="</div>";
    $("#alertHolder").html(html);
}

function showSuccessAlert(message) {
    showAlert("success", message);
}

function showFailureAlert(message) {
    showAlert("danger", message);
}

function showWarningAlert(message) {
    showAlert("warning", message);
}

function showInfoAlert(message) {
    showAlert("info", message);
}


function showajaxCallFailedModal(titleHtml,msgText) {
	var title=titleHtml;
	var msg=msgText;
	var $confirm = $("#commonModal");			
	$confirm.modal('show');		
	$("#lblTitlecommonModal").html(title);
	$("#lblMsgcommonModal").text(msg);
	$("#btnNOcommonModal").hide();
	$("#btnOKcommonModal").removeClass("btn btn-success").addClass("btn btn-info");
	$("#btnOKcommonModal").removeClass("btn btn-danger").addClass("btn btn-info");	
	$("#btnOKcommonModal").text("OK");			
	$('#activityResonDiv').hide();
	
	$("#btnOKcommonModal").off('click').click(function () {
		$confirm.modal("hide");
	});			
}

function showModalSecondary(titleType,title,messageType,message) {            
	var $confirm = $("#modalSec");			
	$confirm.modal('show');	
	
	if(titleType=='text'){
		$("#modalSecTitle").text(title);
	}
	if(titleType=='html'){
		$("#modalSecTitle").html(title);
	}
	if(messageType=='text'){
		$("#modalSecMessage").text(message);
	}
	if(messageType=='html'){
		$("#modalSecMessage").html(message);
	}			
	$("#modalSecOk").off('click').click(function () {
		$confirm.modal("hide");
	});			
}

function showModalSecondaryAdd(titleType,title,messageType,message) {            
	var $confirm = $("#modalSecAdd");			
	$confirm.modal('show');	
	
	if(titleType=='text'){
		$("#modalSecAddTitle").text(title);
	}
	if(titleType=='html'){
		$("#modalSecAddTitle").html(title);
	}
	if(messageType=='text'){
		$("#modalSecAddMessage").text(message);
	}
	if(messageType=='html'){
		$("#modalSecAddMessage").html(message);
	}			
	$("#modalSecAddOk").off('click').click(function () {
		$confirm.modal("hide");
	});			
}

function showErrorDetails(state){
	if(state == 'show'){
		$('#showHideErrorDet').attr('onclick',"showErrorDetails('hide')");
		$('#showHideErrorDet').attr('title','Hide Error Log');
		$('#showHideErrorDet').attr('class','mdi mdi-eye-off icon-sm text-primary cursor-pointer');
		$('#showHideErrorDet').html('<small> Hide Error Log</small>');
		$('#errorDetails').removeClass('d-none');
	}
	if(state == 'hide'){
		$('#showHideErrorDet').attr('onclick',"showErrorDetails('show')");
		$('#showHideErrorDet').attr('title','Show Error Log');
		$('#showHideErrorDet').attr('class','mdi mdi-eye icon-sm text-primary cursor-pointer');
		$('#showHideErrorDet').html('<small> Show Error Log</small>');
		$('#errorDetails').addClass('d-none');
	}
}

function prepareModalMessage(modalTitleText,modalMessage,iconClass,iconColorClass){
	var modalTitleHtml,modalBodyHtml="";
	modalTitleHtml = "<span>"+modalTitleText+"</span>";
	modalBodyHtml+="<div class=\"d-flex flex-grow-1 align-items-left justify-content-left p-1 item\">";
	modalBodyHtml+="<i class=\'"+iconClass+" "+iconColorClass+" mr-3 icon-lg'\></i>";
	modalBodyHtml+="<div class=\"d-flex flex-column justify-content-around\">";                           
	modalBodyHtml+="<div class=\"mr-2 mb-0\">"+modalMessage+"</div>";
	modalBodyHtml+="</div>";
	modalBodyHtml+="</div>";
	showMessageModal(modalTitleHtml,modalBodyHtml);
}

function showMessageModal(modalTitleHtml,modalBodyHtml){
	$('#myModal').removeClass('d-none');
	$('#myModal').modal('show');
	$('#modalTitle').html(modalTitleHtml);
	$('#modalBody').html(modalBodyHtml);
	$('modalClse').off('click',function(){
		$('#myModal').addClass('d-none');
		$('#myModal').modal('hide');
	});
}

function handleAjaxError(xhr){
	var statusCode, statusText, responseText, responseJson = null,errorReason,errorReasonJsonObj,errorObj = {};
	statusCode = xhr.status;
	statusText = xhr.statusText;
	responseText = xhr.responseText;
	errorReason = null;
	errorReasonJsonObj = null;
	try{
		responseJson = jQuery.parseJSON(responseText);
		errorReasonJsonObj = jQuery.parseJSON(responseJson.reason);
	}catch(err){}
	var errorMsg = null;
	if(responseJson != null && 'reason' in responseJson){
		errorReason = responseJson.reason;
	}	
	if(statusCode != 401){
		errorObj['status_code'] = statusCode;
		errorObj['status_text'] = statusText;
		if(errorReasonJsonObj != null){
			if('additional_message' in errorReasonJsonObj){
				errorObj['message'] = errorReasonJsonObj['additional_message'];
			}
			if('root_cause' in errorReasonJsonObj){
				errorObj['root_cause'] = errorReasonJsonObj['root_cause'];
			}
			if('caused_by' in errorReasonJsonObj){
				errorObj['caused_by'] = errorReasonJsonObj['caused_by'];
			}
			if('stack_trace' in errorReasonJsonObj){
				errorObj['stack_trace'] = errorReasonJsonObj['stack_trace'];
			}
		}else{
			if(errorReason != null){
				errorObj['message'] = errorReason;
			}else{
				errorObj['message'] = 'System Error';
			}
		}		
		var modalTitleHtml,modalBodyHtml = "";
		modalTitleHtml = "<span>Error</span>";
		modalBodyHtml+="<div class=\"d-flex flex-grow-1 align-items-left justify-content-left p-1 item\">";
		modalBodyHtml+="<i class=\"mdi mdi-alert-circle-outline text-danger mr-3 icon-lg\"></i>";
		modalBodyHtml+="<div class=\"d-flex flex-column justify-content-around\">";                           
		modalBodyHtml+="<div class=\"mr-2 mb-0\">"+errorObj['message']+"</div>";
		modalBodyHtml+="<div>";
		if('root_cause' in errorObj || 'caused_by' in errorObj || 'stack_trace' in errorObj){
			modalBodyHtml+="<span><i class=\"mdi mdi-eye icon-sm text-primary cursor-pointer\" title=\"Show Error Log\" onclick=\"showErrorDetails('show')\" id=\"showHideErrorDet\"><small> Show Error Log</small></i></span>";
		}
		modalBodyHtml+="</div>";
		modalBodyHtml+="</div>";
		modalBodyHtml+="</div>";	
		
		modalBodyHtml+="<div id=\"errorDetails\" class=\"d-none\">";		
		if('root_cause' in errorObj){
			modalBodyHtml+="<p><b>Root Cause: </b>"+errorObj['root_cause']+"<p>";
		}
		if('caused_by' in errorObj){
			modalBodyHtml+="<p><b>Caused By: </b>"+errorObj['caused_by']+"<p>";
		}
		if('stack_trace' in errorObj){
			modalBodyHtml+="<div>";
			modalBodyHtml+="<p><b>Stack Trace:</b></p>";
			modalBodyHtml+="<ol>";
			for(var i=0;i<errorObj['stack_trace'].length;i++){
				modalBodyHtml+="<li>"+errorObj['stack_trace'][i]+"</li>";				
			}
			modalBodyHtml+="</ol>";
			modalBodyHtml+="</div>";
		}
		modalBodyHtml+="</div>";		
		showMessageModal(modalTitleHtml,modalBodyHtml);
	}else{
		if(errorReason != null){
			errorMsg = "<p>"+errorReason+"</p>";
		}else{
			errorMsg = "<p>Unauthorized</p>";			
		}
		var href = document.location.href;
		var lastPathSegment = href.substr(href.lastIndexOf('/') + 1);
		showMessageModal("Error",errorMsg);	
		if(lastPathSegment != 'index.html'){
			setTimeout(function(){clearSessionStorage();ensureSession(function(){});},3000);
		}		
	}	
}


