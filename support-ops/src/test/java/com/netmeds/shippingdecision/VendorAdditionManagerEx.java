/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.netmeds.shippingdecision;

import com.netmeds.support.ops.utils.SQLFront;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Admin
 */
public class VendorAdditionManagerEx {
    public static void printSellerMapCount(){
        SQLFront sdSF = null;
        try {
            SQLFront.alternateConnectionFactory();
            sdSF=new SQLFront("com.mysql.jdbc.Driver", "jdbc:mysql://v2-prod-mysql-no1.cip33pywvytw.ap-south-1.rds.amazonaws.com:3306/shipping_decision?zeroDateTimeBehavior=convertToNull&rewriteBatchedStatements=true", "sd_user", "sd_pass");
            String sql = "SELECT fc_code FROM shipping_decision.sdcc_fc_master WHERE fc_type='SELLER';";
            List<Map<String,Object>> recList1 = sdSF.getResult(sql);
            System.out.println("fc_code|shipper|count|DIRECT/REF|to_fc");
            for (int i = 0; i < recList1.size(); i++) {
                Map<String, Object> rec1 = recList1.get(i);
                String sellerCode = rec1.get("fc_code")+"";
                sql = "SELECT fc_code, shipper, count(*) as count\n" +
                        " FROM sdcc_fc_tat WHERE fc_code='"+sellerCode+"'\n" +
                        " group by fc_code,shipper\n" +
                        " ;";
                List<Map<String,Object>> recList2 = sdSF.getResult(sql);
                for (int j = 0; j < recList2.size(); j++) {
                    Map<String, Object> rec2 = recList2.get(j);
                    System.out.println(rec2.get("fc_code")+"|"+rec2.get("shipper")+"|"+rec2.get("count")+"|DIRECT");
                }
                sql = "SELECT * FROM sdcc_tat_reference WHERE to_fc='"+sellerCode+"';";
                List<Map<String,Object>> recList3 = sdSF.getResult(sql);
                for (int j = 0; j < recList3.size(); j++) {
                    Map<String, Object> rec3 = recList3.get(j);
                    sql = "SELECT fc_code, shipper, count(*) as count FROM sdcc_fc_tat \n" +
                    " WHERE fc_code='"+rec3.get("from_fc")+"' AND shipper='"+rec3.get("from_shipper")+"'\n" +
                    " group by fc_code,shipper\n" +
                    " ;";
                    List<Map<String,Object>> recList4 = sdSF.getResult(sql);
                    for (int k = 0; k < recList4.size(); k++) {
                        Map<String, Object> rec4 = recList4.get(k);
                        System.out.println(sellerCode+"|"+rec4.get("shipper")+"|"+rec4.get("count")+"|REF|"+rec3.get("from_fc"));
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }finally{
            if(sdSF!=null)sdSF.close();
        }
    }
    public static void main(String[] args) {
        printSellerMapCount();
    }
}
