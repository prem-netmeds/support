/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.netmeds.support.tetrapod;

import com.netmeds.support.ops.utils.JsonUtils;
import com.netmeds.support.ops.utils.SQLFront;
import java.io.PrintWriter;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.UnaryOperator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonString;
import javax.json.JsonValue;


/**
 *
 * @author prem
 */
public class OrderCreateRecon extends AbstractRecon {

    private static final Logger LOGGER=Logger.getLogger(OrderCreateRecon.class.getName());

    private static void writeHeader(PrintWriter out, LocalDateTime now, LocalDateTime startTime, LocalDateTime endTime) {
        writeHeaderGeneric(out, now, startTime, endTime, joinAsCSV(
            "frontend_order_id",
            "item_count",
            "shipping_to_pin",
            "store_code",
            "store_format",
            "payment_type",
            "payment_amount",
            "txn_id",
            "rrn",
            "confirmation_time",
            "attempts",
            "http_status",
            "backend_order_id",
            "response_status",
            "response_status_message",
            "event_id",
            "action_id",
            "response"
        ));
    }

    private static void writeBody(PrintWriter out, ActionLogResult alr) {
        List<Map<String,Object>> recLi = alr.recList;
        BiFunction<JsonObject,String,String> getPaymentMethodsField=(reqJobj, fn)->{
            JsonValue jv;
            JsonArray ja;
            JsonObject jo;

            jv=reqJobj.get("payment_methods");
            if(jv.getValueType()!=JsonValue.ValueType.ARRAY) {
                return "ERR-pm-isnt-array";
            }

            ja=((JsonArray)jv);
            if(ja.size()<=0) {
                return "ERR-array-empty";
            }

            jv=ja.get(0);
            if(jv.getValueType()!=JsonValue.ValueType.OBJECT) {
                return "ERR-ele-not-object";
            }

            jo=((JsonObject)jv);
            if(!jo.containsKey(fn)) {
                return "ERR-key-na:"+fn;
            }
            jv=jo.get(fn);
            if(jv.getValueType()==JsonValue.ValueType.NULL) {
                return "ERR-val-null:"+fn;
            }
            if(jv.getValueType()!=JsonValue.ValueType.STRING) {
                return "ERR-not-str:"+jv.toString();
            }

            return ((JsonString)jv).getString();
        };
        UnaryOperator<String> clean=str->{
            String rsp=str;
            rsp=rsp.replaceAll("\r", "");
            rsp=rsp.replaceAll("\n", "<<CR>>");
            return rsp;
        };

            for (int i = 0; i < recLi.size(); i++) {
                Map<String, Object> actionLog = recLi.get(i);
                //actionLogIdSet2.add((Integer)actionLog.get("id"));
                String requestStr = (actionLog.get("request_msg")!=null)?(String)actionLog.get("request_msg"):null;
                String responseStr = (actionLog.get("response_msg")!=null)?(String)actionLog.get("response_msg"):null;

                JsonObject requestJson=null,responseJson=null;
                try {
                    requestJson  = (requestStr!=null && !requestStr.trim().isEmpty())?JsonUtils.convertStringToJsonObject(requestStr):null;
                    responseJson  = (responseStr!=null && !responseStr.trim().isEmpty())?JsonUtils.convertStringToJsonObject(responseStr):null;
                } catch (Exception e) {
                    //e.printStackTrace();
                }

                Map<String,Object> reconciliationRec = new LinkedHashMap();
                reconciliationRec.put("frontend_order_id",null);
                reconciliationRec.put("item_count",null);
                reconciliationRec.put("shipping_to_pin",null);
                reconciliationRec.put("store_code",null);
                reconciliationRec.put("store_format",null);
                reconciliationRec.put("payment_type",null);
                reconciliationRec.put("payment_amount",null);
                reconciliationRec.put("txn_id",null);
                reconciliationRec.put("rrn",null);
                reconciliationRec.put("confirmation_time",null);
                reconciliationRec.put("attempts",alr.attemptCountMap.get((Integer)actionLog.get("event_queue_id")));
                reconciliationRec.put("http_status",actionLog.get("http_status"));
                reconciliationRec.put("backend_order_id",null);
                reconciliationRec.put("response_status",null);
                reconciliationRec.put("response_status_message",null);
                reconciliationRec.put("event_id",actionLog.get("event_queue_id"));
                reconciliationRec.put("action_id",actionLog.get("action_queue_id"));
                reconciliationRec.put("response",clean.apply(responseStr));

                String pincode="";
                JsonObject shippingJobj=requestJson.getJsonObject("shipping_address");
                if(shippingJobj.containsKey("pin")) {
                    pincode=shippingJobj.getString("pin");
                }
                if(shippingJobj.containsKey("pincode")) {
                    pincode=shippingJobj.getString("pincode");
                }

                if(requestJson!=null){
                    reconciliationRec.put("frontend_order_id",requestJson.getString("temp_order_id"));
                    reconciliationRec.put("item_count",requestJson.getJsonArray("products").size());
                    reconciliationRec.put("shipping_to_pin",pincode);
                    reconciliationRec.put("store_code",requestJson.getString("fulfillment_center_id"));
                    reconciliationRec.put("store_format",requestJson.getString("fulfillment_type"));
                    reconciliationRec.put("payment_type",requestJson.getString("payment_method"));
                    reconciliationRec.put("payment_amount",JsonUtils.convertJsonValueToObject(requestJson,"final_order_amount"));
                    reconciliationRec.put("txn_id",(requestJson.getJsonArray("payment_methods").size()>0)?
                            requestJson.getJsonArray("payment_methods").getJsonObject(0).getString("transaction_ref_number"):"NA");
                    //reconciliationRec.put("rrn",(requestJson.getJsonArray("payment_methods").size()>0)?
                    //        requestJson.getJsonArray("payment_methods").getJsonObject(0).getString("order_app_code"):"NA");
                    reconciliationRec.put("rrn", getPaymentMethodsField.apply(requestJson, "order_app_code"));
                    reconciliationRec.put("confirmation_time",requestJson.getString("order_created_date"));
                }
                if(responseJson!=null){
                    try { reconciliationRec.put("backend_order_id",responseJson.getString("order_id")); }
                    catch(ClassCastException cce) {
                        JsonValue jv=responseJson.get("order_id");
                        LOGGER.log(Level.SEVERE, "cce... event-id:"+actionLog.get("event_queue_id")+", order-id:"+requestJson.getString("temp_order_id")+", value-type:"+jv.getValueType()+".");
                        reconciliationRec.put("backend_order_id", "CCEx");
                    }
                    catch(NullPointerException npe) {
                        LOGGER.log(Level.SEVERE, "npe... event-id:"+actionLog.get("event_queue_id")+", order-id:"+requestJson.getString("temp_order_id"));
                        reconciliationRec.put("backend_order_id", "NPEx");
                    }
                    try { reconciliationRec.put("response_status",responseJson.getString("status")); }
                    catch(Exception e) {
                        reconciliationRec.put("response_status", "Exception");
                    }
                    try { reconciliationRec.put("response_status_message",responseJson.getString("status_Message").replaceAll("\\n", "")); }
                    catch(Exception e) {
                        reconciliationRec.put("response_status_message", "Exception");
                    }
                }
                //reconciliationRecLi.add(reconciliationRec);

                // write out
                List<Object> valList=new ArrayList();
                reconciliationRec.forEach((k,v)->{
                    valList.add(v);
                });

                out.println(joinAsCSV(valList.toArray(new Object[valList.size()])));
            }
    }

    public static void main(String[] args) throws Exception {
        boolean FOR_DATE=true;
        LocalDate forDate=LocalDate.parse("2020-08-13");
        LocalDateTime startTime=LocalDateTime.parse("2020-08-11T10:26:25");
        LocalDateTime endTime=LocalDateTime.parse("2020-08-12T00:00:00");
        /*
        //old database credentials
        SQLFront tpdSF=new SQLFront("com.mysql.jdbc.Driver", "jdbc:mysql://prod-smart-mstar.cluster-ro-cip33pywvytw.ap-south-1.rds.amazonaws.com:3306/smart_tetrapod?zeroDateTimeBehavior=convertToNull&rewriteBatchedStatements=true", "muthu", "Muthu_pwd20");
        SQLFront tpdLogSF=new SQLFront("com.mysql.jdbc.Driver", "jdbc:mysql://prod-logs-cluster.cluster-ro-cip33pywvytw.ap-south-1.rds.amazonaws.com:3306/smart_tetrapodlog?zeroDateTimeBehavior=convertToNull&rewriteBatchedStatements=true", "muthu", "Muthu_pwd19");
        */
        
        SQLFront tpdSF=new SQLFront("com.mysql.jdbc.Driver", "jdbc:mysql://p-jiomart-mstar.cluster-ro-cip33pywvytw.ap-south-1.rds.amazonaws.com:3306/p_jiomart_tetrapod?zeroDateTimeBehavior=convertToNull&rewriteBatchedStatements=true", "muthu", "70a683e5ae7898e717dec7d7d57eff75");
        SQLFront tpdLogSF=new SQLFront("com.mysql.jdbc.Driver", "jdbc:mysql://p-jiomart-logs.cluster-ro-cip33pywvytw.ap-south-1.rds.amazonaws.com:3306/p_jiomart_tetrapod_log?zeroDateTimeBehavior=convertToNull&rewriteBatchedStatements=true", "muthu", "70a683e5ae7898e717dec7d7d57eff75");
        

        String mcname="MS_ORDER_CREATE";

        if(FOR_DATE) {
            startTime=forDate.atStartOfDay();
            endTime=forDate.plusDays(1).atStartOfDay();
        }

        LocalDateTime now=LocalDateTime.now();

        PrintWriter out;
        out=getOutputStreamDefault(mcname, now);
        //out=new PrintWriter(System.out);

        BiConsumer<LocalDateTime,LocalDateTime> headerWriter=(stldt,edldt)->writeHeader(out, now, stldt, edldt);
        Consumer<AbstractRecon.ActionLogResult> bodyWriter=alr->writeBody(out, alr);

        getReconData(mcname, startTime, endTime, tpdSF, tpdLogSF, headerWriter, bodyWriter);

        out.close();
    }
}
