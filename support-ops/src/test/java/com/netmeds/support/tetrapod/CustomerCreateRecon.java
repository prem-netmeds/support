/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.netmeds.support.tetrapod;

import com.netmeds.support.ops.utils.SQLFront;
import java.io.PrintWriter;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

/**
 *
 * @author prem
 */
public class CustomerCreateRecon extends AbstractRecon {
    public static void main(String[] args) throws Exception {
        boolean FOR_DATE=false;
        LocalDate forDate=LocalDate.parse("2020-07-24");
        LocalDateTime startTime=LocalDateTime.parse("2020-07-24T08:15:00");
        LocalDateTime endTime=LocalDateTime.parse("2020-07-24T09:00:00");
        SQLFront tpdSF=new SQLFront("com.mysql.jdbc.Driver", "jdbc:mysql://prod-smart-mstar.cluster-ro-cip33pywvytw.ap-south-1.rds.amazonaws.com:3306/smart_tetrapod?zeroDateTimeBehavior=convertToNull&rewriteBatchedStatements=true", "muthu", "Muthu_pwd20");
        SQLFront tpdLogSF=new SQLFront("com.mysql.jdbc.Driver", "jdbc:mysql://prod-logs-cluster.cluster-ro-cip33pywvytw.ap-south-1.rds.amazonaws.com:3306/smart_tetrapodlog?zeroDateTimeBehavior=convertToNull&rewriteBatchedStatements=true", "muthu", "Muthu_pwd19");
        String mcname="MS_CUSTOMER_CREATE";

        if(FOR_DATE) {
            startTime=forDate.atStartOfDay();
            endTime=forDate.plusDays(1).atStartOfDay();
        }

        LocalDateTime now=LocalDateTime.now();

        PrintWriter out;
        out=getOutputStreamDefault(mcname, now);
        //out=new PrintWriter(System.out);

        BiConsumer<LocalDateTime,LocalDateTime> headerWriter=(stldt,edldt)->writeHeaderDefault(out, now, stldt, edldt);
        Consumer<ActionLogResult> bodyWriter=alr->writeBodyDefault(out, alr);

        getReconData(mcname, startTime, endTime, tpdSF, tpdLogSF, headerWriter, bodyWriter);

        out.close();
    }
}
