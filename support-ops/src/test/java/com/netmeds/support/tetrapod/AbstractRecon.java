/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.netmeds.support.tetrapod;

import com.netmeds.support.ops.utils.JsonUtils;
import com.netmeds.support.ops.utils.SQLFront;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.UnaryOperator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.json.JsonObject;

/**
 *
 * @author prem
 */
public class AbstractRecon {

    private static final Logger LOGGER=Logger.getLogger(AbstractRecon.class.getName());

    private static class BooleanTuple {
        boolean first;
    }

    private static class IntTuple {
        int ctr=0;
    }

    protected static class ActionLogResult {
        final int eventCount;
        final List<Map<String,Object>> recList;
        final Map<Integer,Integer> attemptCountMap;

        public ActionLogResult(int eventCount, List<Map<String, Object>> recList, Map<Integer, Integer> attemptCountMap) {
            this.eventCount = eventCount;
            this.recList = recList;
            this.attemptCountMap = attemptCountMap;
        }
    }

    protected static String joinAsCSV(Object... fvs) {
        StringBuilder sb=new StringBuilder();
        boolean first=true;

        for(Object fo : fvs) {
            String str;

            if(fo==null) {
                str="-null-";
            } else {
                str=""+fo;
            }

            if(first) {
                sb.append(str);
                first=false;
            } else {
                sb.append("|");
                sb.append(str);
            }
        }

        return sb.toString();
    }

    protected static ActionLogResult getLatestActionLogs(String mcname, String startTimeStr, String endTimeStr, Optional<String> limitClauseOpt, SQLFront tpSF, SQLFront tpLogSF) throws SQLException {
        LocalDateTime startTime, endTime;

        startTime=LocalDateTime.parse(startTimeStr);
        endTime=LocalDateTime.parse(endTimeStr);

        return getLatestActionLogs(mcname, startTime, endTime, limitClauseOpt, tpSF, tpLogSF);
    }

    protected static long getEventCount(String mcname, LocalDateTime startTime, LocalDateTime endTime, SQLFront tpSF) throws SQLException {
        String sql;
        Map<String,Object> rec;

        sql=String.format("SELECT count(*) as count FROM tpd_event_queue WHERE mcname='%1$s' AND created_time>='%2$tF %2$tT' AND created_time<'%3$tF %3$tT'", mcname, startTime, endTime);
        rec=tpSF.getResult(sql).get(0);

        return (Long)rec.get("count");
    }

    protected static ActionLogResult getLatestActionLogs(String mcname, LocalDateTime startTime, LocalDateTime endTime, Optional<String> limitClauseOpt, SQLFront tpSF, SQLFront tpLogSF) throws SQLException {
        String sql;
        BooleanTuple bt=new BooleanTuple();
        IntTuple it=new IntTuple();
        int eventCount;

        sql=String.format("SELECT id FROM tpd_event_queue WHERE mcname='%1$s' AND created_time>='%2$tF %2$tT' AND created_time<'%3$tF %3$tT'", mcname, startTime, endTime);
        if(limitClauseOpt.isPresent()) {
            sql=sql+" "+limitClauseOpt.get();
        }
        LOGGER.log(Level.INFO, "sql1:{0}", sql);
        StringBuilder eventIds = new StringBuilder();
        it.ctr=0;
        bt.first=true;
        tpSF.getStreamForQuery(sql).forEach(rec->{
            it.ctr++;
            if(bt.first) {
                eventIds.append(rec.get("id"));
                bt.first=false;
            } else {
                eventIds.append(",");
                eventIds.append(rec.get("id"));
            }
        });
        LOGGER.log(Level.INFO, "found totally {0} events", it.ctr);
        eventCount=it.ctr;

        sql="SELECT\n" +
            "    event_queue_id,\n" +
            "    max(id) action_log_id,\n" +
            "    count(*) no_of_log_records\n" +
            "FROM tpd_action_log \n" +
            "WHERE \n" +
            "    event_queue_id in ("+eventIds.toString()+") \n" +
            "    AND \n" +
            "    process_log<>'unable to grab action!' \n" +
            "GROUP BY event_queue_id\n" +
            ";";
        eventIds.setLength(0);
        StringBuilder actionIds=eventIds;
        Map<Integer,Integer> attemptCountMap = new HashMap();
        it.ctr=0;
        bt.first=true;
        tpLogSF.getResult(sql).forEach(rec->{
            it.ctr++;
            if((it.ctr%5000)==0) {
                System.out.println("at rec:"+it.ctr);
            }
            Integer eventQueueId=(Integer)rec.get("event_queue_id");
            Integer actionLogId=(Integer)rec.get("action_log_id");
            Integer recCount=((Long)rec.get("no_of_log_records")).intValue();
            attemptCountMap.put(eventQueueId, recCount);
            if(bt.first) {
                actionIds.append(actionLogId);
                bt.first=false;
            } else {
                actionIds.append(",");
                actionIds.append(actionLogId);
            }
        });
        LOGGER.log(Level.INFO, "found totally {0} actions", it.ctr);

        sql="SELECT * FROM tpd_action_log WHERE id IN ("+actionIds.toString()+") AND process_log<>'unable to grab action!';";
        return new ActionLogResult(eventCount, tpLogSF.getResult(sql), attemptCountMap);
    }

    protected static void getReconData(String mcname, LocalDateTime startTime, LocalDateTime endTime, SQLFront tpSF, SQLFront tpLogSF, BiConsumer<LocalDateTime,LocalDateTime> headerWriter, Consumer<ActionLogResult> bodyrWriter) throws SQLException {
        final int PAGE_SIZE=10000;
        int pageNo=0;
        int reccount=PAGE_SIZE;
        ActionLogResult alr;
        String limitClause;

        LOGGER.log(Level.INFO, "total events:{0}", getEventCount(mcname, startTime, endTime, tpSF));

        headerWriter.accept(startTime, endTime);

        while(reccount>=PAGE_SIZE) {
            limitClause="LIMIT "+(PAGE_SIZE*pageNo)+","+PAGE_SIZE;
            alr=getLatestActionLogs(mcname, startTime, endTime, Optional.of(limitClause), tpSF, tpLogSF);
            bodyrWriter.accept(alr);
            reccount=alr.eventCount;
            pageNo++;
        }
    }

    protected static void writeHeaderGeneric(PrintWriter out, LocalDateTime generationTimeLDT, LocalDateTime startTime, LocalDateTime endTime, String fieldNamesLine) {
        out.println(String.format("recon for time window: '%1$tF %1$tT' to '%2$tF %2$tT'", startTime, endTime));
        out.println(String.format("report generated at '%1$tF %1$tT'", generationTimeLDT));
        out.println(fieldNamesLine);
    }

    protected static void writeHeaderDefault(PrintWriter out, LocalDateTime generationTimeLDT, LocalDateTime startTime, LocalDateTime endTime) {
        out.println(String.format("recon for time window: '%1$tF %1$tT' to '%2$tF %2$tT'", startTime, endTime));
        out.println(String.format("report generated at '%1$tF %1$tT'", generationTimeLDT));

        // event-id, action-id, no-of-attempts, result, created-time, http-status, duration, status, resp
        out.println(joinAsCSV(
            "event-id",
            "action-id",
            "attempt-count",
            "result",
            "created-time",
            "http-status",
            "duration",
            "status",
            "resp"
        ));
    }

    protected static void writeBodyDefault(PrintWriter out, ActionLogResult alr) {
        List<Map<String,Object>> recList=alr.recList;
        Map<Integer,Integer> attemptCountMap=alr.attemptCountMap;
        UnaryOperator<String> clean=str->{
            String rsp;
            rsp=(str.length()>100)?str.substring(0, 100):str;
            rsp=rsp.replaceAll("\r", "");
            rsp=rsp.replaceAll("\n", "<CR>");
            return rsp;
        };

        // event-id, action-id, no-of-attempts, result, created-time, http-status, duration, status, resp
        recList.forEach(rec->{
            Integer eventId=(Integer)rec.get("event_queue_id");
            Integer actionId=(Integer)rec.get("action_queue_id");
            Integer attemptCount=attemptCountMap.get(eventId);
            String result=(String)rec.get("result");
            Date createdTime=(Date)rec.get("created_time");
            String httpStatus=(String)rec.get("http_status");
            Integer duration=(Integer)rec.get("duration");
            String respStr=(String)rec.get("response_msg");

            // parse resp
            String status="";
            try {
                JsonObject jobj=JsonUtils.convertStringToJsonObject(respStr);
                status=jobj.getString("ErrorMsg");
            } catch(Exception e) {
                status="parse-failure";
            }

            out.println(joinAsCSV(
                eventId,
                actionId,
                attemptCount,
                result,
                String.format("%1$tF %1$tT", createdTime),
                httpStatus,
                duration,
                clean.apply(status),
                clean.apply(respStr)
            ));
        });
    }

    protected static PrintWriter getOutputStreamDefault(String mcname, LocalDateTime generationTimeLDT) throws IOException {
        String filename=String.format("%1$s/Desktop/RECON-%2$s-%3$s.csv", System.getProperty("user.home"), mcname, String.format("%1$tY%1$tm%1$td-%1$tH%1$tM%1$tS", generationTimeLDT));
        return new PrintWriter(new FileWriter(filename));
    }
}
