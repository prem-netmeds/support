/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.netmeds.support.tetrapod;

import com.netmeds.support.ops.utils.CommonUtils;
import com.netmeds.support.ops.utils.ExceptionUtils;
import com.netmeds.support.ops.utils.Functional.Tuple5;
import static com.netmeds.support.ops.utils.Functional.tup;
import com.netmeds.support.ops.utils.JsonUtils;
import com.netmeds.support.ops.utils.RESTUtils;
import com.netmeds.support.ops.utils.SQLFront;
import static com.netmeds.support.ops.utils.SQLFront.kv;
import static com.netmeds.support.ops.utils.SQLFront.seqMapOf;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import static java.util.Comparator.comparing;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.Consumer;
import java.util.function.Function;
import static java.util.stream.Collectors.toList;
import java.util.stream.Stream;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonValue;
import javax.json.JsonValue.ValueType;

/**
 *
 * @author Admin
 */
public class MyJioWebhookEx {
    private static final class MyjioMop {
        private static final String BILLDESK="48";
        private static final String JIOMONEY_WALLET="192";
        private static final String PAYTM="189";
        private static final String MOBIKWIK="190";
        private static final String PHONEPE="191";
        private static final String YESBANK="187";
        private static final String SODEXO="184";
        private static final String COD="20";
        private static final String STORE_CREDIT="119";
        private static final String EMPLOYEE_LIMIT="183";
        private static final String LOYALTY_POINTS="5";
    }
    
    public static class MyjioWebhookMessage {
        public static class PaymentDetail {
            private final int sortId;
            private final String instRefNo;
            private final BigDecimal amount;
            private final String mopCode;
            private final String mopName;
            private final String mopDesc;
            private final Date instTime;

            public PaymentDetail(int sortId, String instRefNo, BigDecimal amount, String mopCode, String mopName, String mopDesc, Date instTime) {
                this.sortId = sortId;
                this.instRefNo = instRefNo;
                this.amount = amount;
                this.mopCode = mopCode;
                this.mopName = mopName;
                this.mopDesc = mopDesc;
                this.instTime = instTime;
            }

            public int getSortId() { return sortId; }
            public String getInstRefNo() { return instRefNo; }
            public BigDecimal getAmount() { return amount; }
            public String getMopCode() { return mopCode; }
            public String getMopName() { return mopName; }
            public String getMopDesc() { return mopDesc; }
            public Date getInstTime() { return instTime; }

            public boolean isCod() {
                return getMopCode().equals(MyjioMop.COD);
            }

            public boolean isPrepaid() {
                String mopcode=getMopCode();

                if(mopcode.equals(MyjioMop.COD))            return false;
                if(mopcode.equals(MyjioMop.STORE_CREDIT))   return false;
                if(mopcode.equals(MyjioMop.EMPLOYEE_LIMIT)) return false;
                if(mopcode.equals(MyjioMop.LOYALTY_POINTS)) return false;

                return true;
            }

            public boolean isNop() {
                String mopcode=getMopCode();

                if(mopcode.equals(MyjioMop.STORE_CREDIT))   return true;
                if(mopcode.equals(MyjioMop.EMPLOYEE_LIMIT)) return true;
                if(mopcode.equals(MyjioMop.LOYALTY_POINTS)) return true;

                return false;
            }

            public boolean isSodexo() {
                return getMopCode().equals(MyjioMop.SODEXO);
            }

            public boolean isStoreCredit() {
                return getMopCode().equals(MyjioMop.STORE_CREDIT);
            }

            public boolean isEmployeeLimit() {
                return getMopCode().equals(MyjioMop.EMPLOYEE_LIMIT);
            }

            public boolean isLoyaltyPoints() {
                return getMopCode().equals(MyjioMop.LOYALTY_POINTS);
            }

            public JsonObject toJson() {
                Map<String,Object> map=seqMapOf(
                    kv("sort_id", getSortId()),
                    kv("inst_refno", getInstRefNo()),
                    kv("amount", getAmount()),
                    kv("mop_code", getMopCode()),
                    kv("mop_name", getMopName()),
                    kv("mop_desc", getMopDesc()),
                    kv("inst_time", getInstTime())
                );

                return RESTUtils.convertMapToJson(map);
            }

            @Override
            public String toString() {
                return toJson().toString();
            }

            public static PaymentDetail parse(JsonObject jobj) {
                JsonObject pdJobj=jobj;
                Integer sortId=pdJobj.getInt("sortId");
                String instRefNo=pdJobj.getString("instrumentReference");
                //BigDecimal amount=pdJobj.getJsonNumber("amount").bigDecimalValue();
                BigDecimal amount=new BigDecimal(pdJobj.getString("amount"));
                String mopCode=pdJobj.getString("mop");
                String mopName=pdJobj.getString("modeOfPayment");
                String mopDesc=pdJobj.getString("modeOfPaymentDesc");
                String instTimeStr=pdJobj.getString("instrumentDate");

                Date instTime=Timestamp.valueOf(LocalDateTime.parse(instTimeStr));

                return new PaymentDetail(sortId, instRefNo, amount, mopCode, mopName, mopDesc, instTime);
            }
        }

        private final String txnRefNo;
        private final String txnType;
        private final String linkId;
        private final BigDecimal totalAmount;
        private final Date txnTime;
        private final boolean success;
        private final String channelId;
        private final List<PaymentDetail> paymentDetailList;

        public MyjioWebhookMessage(String txnRefNo, String txnType, String linkId, BigDecimal totalAmount, Date txnTime, boolean success, String channelId, List<PaymentDetail> paymentDetailList) {
            this.txnRefNo = txnRefNo;
            this.txnType = txnType;
            this.linkId = linkId;
            this.totalAmount = totalAmount;
            this.txnTime = txnTime;
            this.success = success;
            this.channelId = channelId;
            this.paymentDetailList = paymentDetailList;
        }

        public String getTxnRefNo() { return txnRefNo; }
        public String getTxnType() { return txnType; }
        public String getLinkId() { return linkId; }
        public BigDecimal getTotalAmount() { return totalAmount; }
        public Date getTxnTime() { return txnTime; }
        public boolean isSuccess() { return success; }
        public String getChannelId() { return channelId; }
        public List<PaymentDetail> getPaymentDetailList() { return paymentDetailList; }

        @Override
        public String toString() {
            Map<String,Object> map=seqMapOf(
                kv("txn_refno", getTxnRefNo()),
                kv("txn_type", getTxnType()),
                kv("link_id", getLinkId()),
                kv("total_amount", getTotalAmount()),
                kv("txn_time", getTxnTime()),
                kv("success", isSuccess()),
                kv("channel_id", getChannelId()),
                kv("payment_details", getPaymentDetailList().stream().map(PaymentDetail::toJson).collect(toList()))
            );

            return RESTUtils.convertMapToJson(map).toString();
        }

        public PaymentDetail identifyPrimaryPayment() {
            List<PaymentDetail> pdList=getPaymentDetailList();
            Optional<PaymentDetail> pdOpt;

            pdOpt=pdList.stream().
                    filter(pd->{
                        if(pd.getMopCode().equals(MyjioMop.STORE_CREDIT)) return false;
                        if(pd.getMopCode().equals(MyjioMop.LOYALTY_POINTS)) return false;
                        if(pd.getMopCode().equals(MyjioMop.EMPLOYEE_LIMIT)) return false;
                        if(pd.getMopCode().equals(MyjioMop.COD)) return false;
                        return true;
                    }).
                    findFirst();

            if(pdOpt.isPresent()) {
                return pdOpt.get();
            }

            pdOpt=pdList.stream().
                    filter(pd->{
                        if(pd.getMopCode().equals(MyjioMop.STORE_CREDIT)) return false;
                        if(pd.getMopCode().equals(MyjioMop.LOYALTY_POINTS)) return false;
                        if(pd.getMopCode().equals(MyjioMop.EMPLOYEE_LIMIT)) return false;
                        return true;
                    }).
                    findFirst();

            if(pdOpt.isPresent()) {
                return pdOpt.get();
            }

            return pdList.get(0);
        }

        public Optional<PaymentDetail> getSodexoPayment() {
            List<PaymentDetail> pdList=getPaymentDetailList();
            return pdList.stream().filter(PaymentDetail::isSodexo).findFirst();
        }

        public Optional<BigDecimal> getPrepaidAmount() {
            List<PaymentDetail> pdList=getPaymentDetailList();
            return pdList.stream().filter(PaymentDetail::isPrepaid).map(PaymentDetail::getAmount).reduce(BigDecimal::add);
        }

        public Optional<BigDecimal> getCodAmount() {
            List<PaymentDetail> pdList=getPaymentDetailList();
            return pdList.stream().filter(PaymentDetail::isCod).map(PaymentDetail::getAmount).reduce(BigDecimal::add);
        }

        public boolean isPrepaid() {
            boolean prepaid = getPaymentDetailList().stream().anyMatch(PaymentDetail::isPrepaid);

            if(prepaid) return true;

            boolean allWallets=getPaymentDetailList().stream().allMatch(PaymentDetail::isNop);

            return allWallets;
        }

        public String computeChecksum(String checksumAlgo, String secretKey) {
            StringBuilder sb=new StringBuilder();
            Consumer<String> appender=str->{
                sb.append("|");
                sb.append(str);
            };

            sb.append(isSuccess());
            appender.accept(getTxnRefNo());
            appender.accept(getTotalAmount().toPlainString());


            List<PaymentDetail> pdList=new ArrayList(getPaymentDetailList());
            pdList.sort(comparing(PaymentDetail::getSortId));
            pdList.forEach(pd->{
                appender.accept(pd.getInstRefNo());
                appender.accept(pd.getAmount().toPlainString());
                appender.accept(pd.getMopCode());
            });

            String checksumFor=sb.toString();
            //System.out.println("checksum-for:"+checksumFor);

            String checksum=CommonUtils.encodeToHex(CommonUtils.computeChecksum(checksumFor, checksumAlgo, secretKey), 64).toUpperCase();

            return checksum;
        }

        public static MyjioWebhookMessage parse(JsonObject jobj) {
            Function<String,String> getNullableString=an->{
                if(jobj.containsKey(an)) {
                    JsonValue jv=jobj.get(an);
                    if(jv.getValueType()==JsonValue.ValueType.STRING) {
                        return jobj.getString(an);
                    } else {
                        return jv.toString();
                    }
                } else {
                    return "";
                }
            };
            String txnRefNo=jobj.getString("transactionRefNumber");
            String txnType=jobj.getString("transactionType");
            String linkId=getNullableString.apply("linkId");
          //BigDecimal totalAmount=jobj.getJsonNumber("totalAmount").bigDecimalValue();
            BigDecimal totalAmount=new BigDecimal(jobj.getString("totalAmount"));
            String txnTimeStr=jobj.getString("transactionDateTime");
            boolean success=jobj.getBoolean("success");
            String channelId=jobj.getString("channelId");
            List<PaymentDetail> pdList=new ArrayList();

            JsonArray paymentDetailsJsar=jobj.getJsonArray("paymentDetail");


            Date txnTime=Timestamp.valueOf(LocalDateTime.parse(txnTimeStr));

            paymentDetailsJsar.forEach(jv->{
                /*
                JsonObject pdJobj=(JsonObject)jv;
                Integer sortId=pdJobj.getInt("sortId");
                String instRefNo=pdJobj.getString("instrumentReference");
                BigDecimal amount=pdJobj.getJsonNumber("amount").bigDecimalValue();
                String mopCode=pdJobj.getString("mop");
                String mopName=pdJobj.getString("modeOfPayment");
                String mopDesc=pdJobj.getString("modeOfPaymentDesc");
                String instTimeStr=pdJobj.getString("instrumentDate");

                Date instTime=Timestamp.valueOf(LocalDateTime.parse(instTimeStr));

                pdList.add(new PaymentDetail(sortId, instRefNo, amount, mopCode, mopName, mopDesc, instTime));
                */
                pdList.add(PaymentDetail.parse((JsonObject)jv));
            });

            return new MyjioWebhookMessage(txnRefNo, txnType, linkId, totalAmount, txnTime, success, channelId, pdList);
        }
    }
    
    public static JsonObject getOrderCreateJson(String orderId){
	SQLFront smartPrd = null;
	SQLFront smartTetrapod = null;
	SQLFront smartTetrapodLog = null;
	JsonObject orderCreateJson = null;
	try {
            smartPrd = SQLFront.jiomartMStarPrd();
            smartTetrapod = SQLFront.jiomartTetrapodPrd();
            smartTetrapodLog = SQLFront.jiomartTetrapodLogPrd();			

            String sql;
            try {
                sql = "select tp_event_id from mst_cart where order_id = '"+orderId+"'";
                int tpEventId = 0;
                List<Map<String,Object>> cartRecList = smartPrd.getResult(sql);
                if(cartRecList.isEmpty()){
                    sql = "select tp_event_id from mst_cart where id in (select cart_id from mst_order_id_history where order_id = '"+orderId+"');";
                    cartRecList = smartPrd.getResult(sql);
                }
                if(cartRecList.size()>0){
                        Map<String,Object> cartRec = cartRecList.get(0);
                        if(cartRec.get("tp_event_id") != null){
                            tpEventId = (int)cartRec.get("tp_event_id");
                            sql = "select id from tpd_action_queue where event_queue_id = "+tpEventId+";";
                            List<Map<String,Object>> actionRecList = smartTetrapod.getResult(sql);
                            if(actionRecList.size()>0){
                                Map<String,Object> actionRec = actionRecList.get(0);
                                int tpActionId = (int)actionRec.get("id");
                                sql = "select * from tpd_action_log where action_queue_id = "+tpActionId+" order by created_time desc;";
                                List<Map<String,Object>> actionLogRecList = smartTetrapodLog.getResult(sql);
                                if(actionLogRecList.size()>0){
                                        Map<String,Object> actionLogRec = actionLogRecList.get(0);

                                        orderCreateJson = JsonUtils.convertStringToJsonObject(actionLogRec.get("request_msg")+"");
                                }else{
                                    throw new RuntimeException("invalid/no records for the 'action_queue_id' from table 'tpd_action_log'");
                                }
                            }else{
                                throw new RuntimeException("invalid/no records for the 'event_queue_id' from table 'tpd_action_queue'");
                            }
                        }else{
                            throw new RuntimeException("tp_event_id is null");
                        }
                }else{
                    throw new RuntimeException("invalid/no records for the 'order_id' from table 'mst_cart' and 'order_id_history'");
                }
            } catch (Exception e) {
                Throwable t = ExceptionUtils.getRootCause(e);
                throw new RuntimeException(ExceptionUtils.getExceptionMessage(t,"exception: unable to get log!"));
            }
	} catch (Exception e) {
            Throwable t = ExceptionUtils.getRootCause(e);
            ExceptionUtils.logExceptionStackTrace(t);
            String msg = ExceptionUtils.getExceptionMessage(t,"get order create json failed!");
            throw new RuntimeException(msg);
	}finally{
            if(smartPrd!=null)smartPrd.close();
            if(smartTetrapod!=null)smartTetrapod.close();
            if(smartTetrapodLog!=null)smartTetrapodLog.close();
	}
	return orderCreateJson;
    }
    
    public static List<MyjioWebhookMessage> getMyJioWebhookMessages(String orderId){
        List<MyjioWebhookMessage> webhookMsgLi = null;
        SQLFront sf = null;
        try {
            webhookMsgLi = new ArrayList();
            sf = SQLFront.jiomartMStarPrd();
            String sql = "SELECT * FROM mst_myjio_webhook_log where order_id = '"+orderId+"';";
            List<Map<String,Object>> recLi = sf.getResult(sql);
            for (int i = 0; i < recLi.size(); i++) {
                Map<String, Object> rec = recLi.get(i);
                String reqContent = rec.get("request_content")+"";
                JsonObject jobj=JsonUtils.convertStringToJsonObject(reqContent);

                MyjioWebhookMessage msg=MyjioWebhookMessage.parse(jobj);
                webhookMsgLi.add(msg);
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }finally{
            if(sf!=null)sf.close();
        }
        return webhookMsgLi;
    }
    public static Date getPaymentConclusionTime(String orderId){
        Date paymentConTime = null;
        SQLFront sf = null;
        try {
            sf = SQLFront.jiomartMStarPrd();
            String sql = "SELECT * FROM mst_cart where order_id = '"+orderId+"';";
            List<Map<String,Object>> recLi = sf.getResult(sql);
            for (int i = 0; i < recLi.size(); i++) {
                Map<String, Object> rec = recLi.get(i);
                paymentConTime = (Date)rec.get("payment_conclusion_time");
                
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }finally{
            if(sf!=null)sf.close();
        }
        return paymentConTime;
    }
    public static void myjioWebhookPaymentProcess(List<String> orderIdLi){
        try {
            for (int i = 0; i < orderIdLi.size(); i++) {
                String orderId = orderIdLi.get(i);
                List<MyjioWebhookMessage> webhookMsgLi = getMyJioWebhookMessages(orderId);
                String outCome;
                if(webhookMsgLi.isEmpty()){
                    outCome = "NO-WEBHOOK";
                }else if(webhookMsgLi.size()>1){
                    outCome = "MULTIPLE-WEBHOOK";
                }else{
                    JsonObject orderCreateJson = getOrderCreateJson(orderId);
                    writeFile(orderCreateJson.toString(), String.format("%1$s%2$sDesktop%2$sTemp%2$sMyJio%2$sSource%2$s%3$s.json", System.getProperty("user.home"),File.separator,orderId+"-request"));
                    Function<Date,String> formatTSToMM_DD_YYYY_HH_MM_SS=ld->String.format("%1$td-%1$tm-%1$tY %1$tT", ld);
                    
                    Function<Boolean,List<Map<String,Object>>> makePaymentMethodsArray=cod->{
                        List<Map<String,Object>> pmList=new ArrayList();
                        String txnDate=formatTSToMM_DD_YYYY_HH_MM_SS.apply(getPaymentConclusionTime(orderId));
                        Consumer<Tuple5<String,String,BigDecimal,String,String>> add=t5->{
                                String pmtId=t5._1;
                                String pmtDesc=t5._2;
                                BigDecimal pmtAmt=t5._3;
                                String txnId=t5._4;
                                String approvalCode=t5._5;

                                Map<String,Object> map=seqMapOf(
                                        kv("payment_id", pmtId),
                                        kv("payment_desc", pmtDesc),
                                        kv("payment_amt", pmtAmt),
                                        kv("payment_cart", null),
                                        kv("order_inv_num", null),
                                        kv("order_app_code", approvalCode),
                                        kv("bdcustomer_id", null),
                                        kv("transaction_ref_number", txnId),
                                        kv("txn_date", txnDate)
                                );
                                pmList.add(map);
                        };

                        MyjioWebhookMessage myjioWHMessageOpt=webhookMsgLi.get(0);

                        myjioWHMessageOpt.getPaymentDetailList().forEach(pd->{
                                add.accept(tup(pd.getMopCode(), pd.getMopDesc(), pd.getAmount(), myjioWHMessageOpt.getTxnRefNo(), pd.getInstRefNo()));
                        });
                        return pmList;
                    };
                    List<Map<String,Object>> paymentMethodLi = makePaymentMethodsArray.apply(Boolean.TRUE);
                    JsonArray paymentMethodJsonArr = RESTUtils.convertListToJson(paymentMethodLi);
                    orderCreateJson = updatePaymentMethod(orderCreateJson, paymentMethodJsonArr);
                    
                    writeFile(orderCreateJson.toString(), String.format("%1$s%2$sDesktop%2$sTemp%2$sMyJio%2$sOutput%2$s%3$s.json", System.getProperty("user.home"),File.separator,orderId+"-request"));
                    outCome = "DONE";
                }
                System.out.println(orderId+","+outCome);
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    
    }
    
    public static JsonObject updatePaymentMethod(JsonObject parent, JsonArray paymentMethodJsonArr) {
        JsonObjectBuilder jsonBuilder = Json.createObjectBuilder();
        parent.entrySet().
                forEach(e -> {
                    if("payment_methods".equals(e.getKey())){
                        jsonBuilder.add(e.getKey(), paymentMethodJsonArr);
                    }else{
                        jsonBuilder.add(e.getKey(), e.getValue());
                    }
                });
        return jsonBuilder.build();
    }
    
    public static void writeFile(String sourceStr,String fileName){
        try {
            BufferedWriter bw = new BufferedWriter(new FileWriter(new File(fileName)));
            bw.write(sourceStr);
            bw.close();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }    
    }
    
    public static void getRedeemFailedRecords(String fromDateTimeStr, String toDateTimeStr){
        SQLFront sf = null;
        try {
            sf = SQLFront.jiomartTetrapodPrd();
            String sql = "SELECT \n" +
                            "	* \n" +
                            "FROM p_jiomart_tetrapod.tpd_event_queue \n" +
                            "WHERE \n" +
                            "	mcname = 'MS_ORDER_CREATE'\n" +
                            "	AND created_time >= '"+fromDateTimeStr+"' AND created_time < '"+toDateTimeStr+"'\n" +
                            ";";
            System.out.println("sql: "+sql);
            Stream<ResultSet> eventRecStm = sf.getRSStreamForQuery(sql);
            Map<String,Map<String,Object>> redeemFailedRec = new HashMap();
            Set<String> paymentDescSet = new LinkedHashSet();
            eventRecStm.forEach(rs->{
                try {
                    String dataStr = rs.getString("data");
                    JsonObject dataJsonObj = JsonUtils.convertStringToJsonObject(dataStr);
                    JsonArray  paymentMethods = dataJsonObj.getJsonArray("payment_methods");
                    String orderId = dataJsonObj.getString("temp_order_id");
                    String orderCreatedTime = dataJsonObj.getString("order_created_date");
                    for (int j = 0; j < paymentMethods.size(); j++) {
                        JsonObject paymentMethod = (JsonObject)paymentMethods.get(j);

                        String paymentDesc = paymentMethod.getString("payment_desc");
                        String orderAppCode = (paymentMethod.get("order_app_code").getValueType().equals(ValueType.NULL))?null:paymentMethod.getString("order_app_code");
                        Object paymentAmount = paymentMethod.get("payment_amt");
                        String txnDate = paymentMethod.getString("txn_date");
                        if(orderAppCode!=null && (orderAppCode.equals("REDEEMFAILED")||orderAppCode.equals("REDEEM-FAILED"))){
                            paymentDescSet.add(paymentDesc);
                            Map<String,Object> redeemFailedRecVal;
                            if(redeemFailedRec.containsKey(orderId)){
                                redeemFailedRecVal = redeemFailedRec.get(orderId);
                            }else{
                                redeemFailedRecVal = new HashMap();
                                redeemFailedRecVal.put("order_created_date",orderCreatedTime);
                            }
                            Map<String,Object> rec = new HashMap();
                            rec.put("payment_desc", paymentDesc);
                            rec.put("order_app_code", orderAppCode);
                            rec.put("payment_amt", paymentAmount);
                            rec.put("txn_date", txnDate);

                            redeemFailedRecVal.put(paymentDesc+"",rec);
                            redeemFailedRec.put(orderId, redeemFailedRecVal);
                        }
                    }
                } catch (SQLException e) {
                    throw new RuntimeException(e);
                }                
            });
            
            StringBuilder sb = new StringBuilder();
            sb.append("order_id,order_created_date");
            for (String paymentDescKey : paymentDescSet) {
                sb.append(String.format(",%1$s_redeem_failed", paymentDescKey));
                sb.append(String.format(",%1$s_payment_amt", paymentDescKey));
                sb.append(String.format(",%1$s_txn_date", paymentDescKey));
            }
            System.out.println(sb);
            for (String key : redeemFailedRec.keySet()) {
                Map<String,Object> redeemFailedRecVal = redeemFailedRec.get(key);
                sb = new StringBuilder();
                sb.append(String.format("%1$s,%2$s", key,redeemFailedRecVal.get("order_created_date")));
                for (String paymentDescKey : paymentDescSet) {
                    if(redeemFailedRecVal.containsKey(paymentDescKey)){
                        sb.append(String.format(",%1$s", "Y"));
                        Map<String,Object> rec = (Map<String,Object>)redeemFailedRecVal.get(paymentDescKey);
                        sb.append(String.format(",%1$s", rec.get("payment_amt")));
                        sb.append(String.format(",%1$s", rec.get("txn_date")));
                    }else{
                        sb.append(",");
                        sb.append(",");
                        sb.append(",");
                    }
                }
                System.out.println(sb);
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }finally{
            if(sf!=null)sf.close();
        }
    
    }
    
    public static void main(String[] args) {
//        List<String> orderIdLi = Arrays.asList("16038154899105064A");
//        myjioWebhookPaymentProcess(orderIdLi);
        getRedeemFailedRecords("2020-10-28 00:00:00", "2020-10-28 02:00:00");
    }


}
